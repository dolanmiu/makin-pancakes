﻿using System;
using System.Xml.Linq;
using Windows.Storage;

namespace _3D_Game_Recompile.PlayerData
{
    /// <Summary>
    /// XML Writer Class: Reads the XML saved data.
    /// </Summary>
    public static class XMLWriter
    {
        /// <Summary>
        /// public static XDocument GenerateBlankPlayerXML(): 
        ///     Generates a blank XML save data file.
        /// </Summary>
        public static XDocument GenerateBlankPlayerXML()
        {
            XDocument doc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement("SaveData",
                    new XElement("Statistics",
                        new XElement("CubesSaved", ""),
                        new XElement("GamesPlayed", "")),
                    new XElement("Stars",""),
                    new XElement("Items",""),
                    new XElement("Abilities",
                        new XElement("Skill",""))));

            return doc;
        }

        /// <Summary>
        /// public static XDocument GeneratePlayerXML(): 
        ///     Generates a XML save data file from the player stats class.
        /// </Summary>
        public static XDocument GeneratePlayerXML()
        {
            XElement rootNode = new XElement("SaveData");
            XElement starNode = new XElement("Stars");
            XElement itemNode = new XElement("Items");
            XElement cubeNode = new XElement("CubesSaved", new XAttribute("total", PlayerStats.TotalBlocksDestroyed));
            XElement abilitiesNode = new XElement("Abilities");

            XDocument doc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
            /*               new XElement("SaveData",
                   new XElement("Statistics",
                       new XElement("CubesSaved", PlayerStats.TotalBlocksDestroyed),
                       new XElement("GamesPlayed", PlayerStats.TotalGamesPlayed))));*/

            for (int i = 0; i < PlayerStats.Stars.Count; i++)
            {
                starNode.Add(new XElement("Star", PlayerStats.Stars[i].ToString()));
            }

            for (int i = 0; i < PlayerStats.Items.Count; i++)
            {
                itemNode.Add(new XElement("Item", new XAttribute("id", PlayerStats.Items[i].ID)));
            }

            doc.Add(rootNode);
            rootNode.Add(starNode);
            rootNode.Add(itemNode);
            rootNode.Add(cubeNode);
            rootNode.Add(abilitiesNode);

            return doc;
        }

        /// <Summary>
        /// private static XDocument GenerateLevelXML(Level level): 
        ///     Generates a XML file representation of a Level.
        /// </Summary>
        private static XDocument GenerateLevelXML(Level level)
        {
            XDocument doc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));
            XElement levelNode = new XElement("Level",
                new XAttribute("width", level.Width.ToString()),
                new XAttribute("height", level.Height.ToString()));

            for (int i = 0; i < level.Waves.Count; i++)
            {
                levelNode.Add(new XElement("Wave", level.Waves[i].WaveString));
            }
            doc.Add(levelNode);
            return doc;
        }

        /// <Summary>
        /// public static async void WritePlayerData(string filename): 
        ///     Asynchronously writes the player data onto a hard disk.
        /// </Summary>
        public static async void WritePlayerData(string filename)
        {
            XDocument doc = GeneratePlayerXML();

            StorageFolder myGames = await Global.GameDirectory.CreateFolderAsync("My Games", CreationCollisionOption.OpenIfExists);
            StorageFolder storageFolder = await myGames.CreateFolderAsync("Makin Pancakes", CreationCollisionOption.OpenIfExists);

            StorageFile playerData = await storageFolder.CreateFileAsync(filename + ".xml", CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(playerData, doc.ToString());
        }

        /// <Summary>
        /// public static async void WriteBlankPlayerData(string filename): 
        ///     Asynchronously writes a blank save file if a current one doesn't exist.
        /// </Summary>
        public static async void WriteBlankPlayerData(string filename)
        {
            XDocument doc = GenerateBlankPlayerXML();

            StorageFolder myGames = await Global.GameDirectory.CreateFolderAsync("My Games", CreationCollisionOption.OpenIfExists);
            StorageFolder storageFolder = await myGames.CreateFolderAsync("Makin Pancakes", CreationCollisionOption.OpenIfExists);

            StorageFile playerData = await storageFolder.CreateFileAsync(filename + ".xml", CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(playerData, doc.ToString());
        }

        /// <Summary>
        /// public static async void WriteLevelData(String levelName, Level level): 
        ///     Asynchronously writes a level created by the user.
        /// </Summary>
        public static async void WriteLevelData(String levelName, Level level)
        {
            XDocument doc = GenerateLevelXML(level);

            StorageFolder myGames = await Global.GameDirectory.CreateFolderAsync("My Games", CreationCollisionOption.OpenIfExists);
            StorageFolder storageFolder = await myGames.CreateFolderAsync("Makin Pancakes", CreationCollisionOption.OpenIfExists);
            StorageFolder levelFolder = await storageFolder.CreateFolderAsync("Custom Levels", CreationCollisionOption.OpenIfExists);

            StorageFile levelData = await levelFolder.CreateFileAsync(levelName + ".xml", CreationCollisionOption.GenerateUniqueName);
            await FileIO.WriteTextAsync(levelData, doc.ToString());
        }
    }
}
