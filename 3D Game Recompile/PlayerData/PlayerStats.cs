﻿using _3D_Game_Recompile.PlayerData;
using System.Collections.Generic;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Player Stats Class: Stores all the statistics of the player.
    /// </Summary>
    public class PlayerStats
    {
        private static int goodBlocksDestroyed = 0;
        private static int addNextWaveRows = 1;

        private static int totalBlocksDestroyed;
        private static int totalGamesPlayed;
        private static int lifeLine = 3;

        private static List<int> stars = new List<int>();
        private static List<Item> items = new List<Item>();

        /// <Summary>
        /// public static int GetStars(int levelID): 
        ///     Gets the amount of stars the player has for a certain level ID.
        /// </Summary>
        public static int GetStars(int levelID)
        {
            if (levelID > stars.Count - 1)
            {
                return 0;
            }
            else
            {
                return stars[levelID];
            }
        }

        /// <Summary>
        /// public static void AddItem(): 
        ///     Adds an item to the players arsenal.
        /// </Summary>
        public static bool AddItem(Item item)
        {
            if (!ContainsItem(item) && totalBlocksDestroyed > item.Price)
            {
                items.Add(item);
                totalBlocksDestroyed -= item.Price;
                XMLWriter.WritePlayerData("playerData");
                return true;
            }
            return false;
        }

        /// <Summary>
        /// public static void AddItem(): 
        ///     Adds an item to the players arsenal.
        /// </Summary>
        public static void AddItem(int itemID)
        {
            Item item = ItemList.CreateItem(itemID);
            if (!ContainsItem(item))
            {
                items.Add(item);
                XMLWriter.WritePlayerData("playerData");
            }
        }

        /// <Summary>
        /// public static void ContainsItem(Item item):
        ///     Helper function to check between two items
        /// </Summary>
        public static bool ContainsItem(Item item)
        {
            foreach (Item tempItem in items)
            {
                if (tempItem.ToString() == item.ToString()) return true;
            }
            return false;
        }

        /// <Summary>
        /// public static bool ContainsItem(Item item):
        ///     Helper function to check between two items
        /// </Summary>
        public static bool ContainsItem(string itemName)
        {
            foreach (Item tempItem in items)
            {
                if (tempItem.ToString() == itemName) return true;
            }
            return false;
        }

        /// <Summary>
        /// public static int GetStars(Level level): 
        ///     Gets the amount of stars the player has for a certain level.
        /// </Summary>
        public static int GetStars(Level level)
        {
            int levelID = level.ID;
            if (levelID > stars.Count - 1)
            {
                return 0;
            }
            else
            {
                return stars[levelID];
            }
        }

        /// <Summary>
        /// public static void SetStars(Level level, int stars): 
        ///     Sets the amount of stars a level has.
        /// </Summary>
        public static void SetStars(Level level, int stars)
        {
            CheckStarLength();
            int levelID = level.ID;
            Stars[levelID] = stars;
        }

        /// <Summary>
        /// private static void CheckStarLength(): 
        ///     Checks to see if the amount of levels match up to the amount of stars. If not it will make some.
        /// </Summary>
        private static void CheckStarLength()
        {
            if (Stars.Count < AllLevels.Levels.Count)
            {        
                for (int i = Stars.Count; i < AllLevels.Levels.Count; i++)
                {
                    Stars.Add(0);
                }
            }
        }

        /// <Summary>
        /// public static void ResetLevelStats(): 
        ///     Resets stats of a level when entering to the next level.
        /// </Summary>
        public static void ResetLevelStats()
        {
            goodBlocksDestroyed = 0;
            addNextWaveRows = 1;
            lifeLine = 3;
        }

        #region Properties

        public static List<int> Stars
        {
            get 
            { return stars; }
            set { stars = value; }
        }

        public static int GoodBlocks
        {
            get { return goodBlocksDestroyed; }
            set { goodBlocksDestroyed = value; }
        }

        public static int LifeLine
        {
            get { return lifeLine; }
            set { lifeLine = value; }
        }

        public static int TotalGamesPlayed
        {
            set { totalGamesPlayed = value; }
            get { return totalGamesPlayed; }
        }

        public static int TotalBlocksDestroyed
        {
            set { totalBlocksDestroyed = value; }
            get { return totalBlocksDestroyed; }
        }

        public static int AddNextRows
        {
            get { return addNextWaveRows; }
            set { addNextWaveRows = value; }
        }

        public static List<Item> Items
        {
            get { return items; }
        }
        #endregion
    }
}
