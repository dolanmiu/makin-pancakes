﻿using _3D_Game_Recompile.PlayerData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Data.Xml.Dom;
using Windows.Storage;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// XML Reader Class: Reads the XML saved data.
    /// </Summary>
    public static class XMLReader
    {

        /// <Summary>
        /// public static async void ReadCustomLevels(): 
        ///     Asynchronously reads the custom levels the player created.
        /// </Summary>
        public static async void ReadCustomLevels()
        {
            StorageFolder myGames = await Global.GameDirectory.CreateFolderAsync("My Games", CreationCollisionOption.OpenIfExists);
            StorageFolder storageFolder = await myGames.CreateFolderAsync("Makin Pancakes", CreationCollisionOption.OpenIfExists);
            StorageFolder levelFolder = await storageFolder.CreateFolderAsync("Custom Levels", CreationCollisionOption.OpenIfExists);

            IReadOnlyList<StorageFile> files = await levelFolder.GetFilesAsync();

            for (int i = 0; i < files.Count; i++)
            {
                StorageFile file = files[i];
                //var stream = await file.OpenAsync(FileAccessMode.Read);
                XmlDocument xmlDoc = await XmlDocument.LoadFromFileAsync(file);
                XDocument xDoc = XDocument.Parse(xmlDoc.GetXml());
                IEnumerable<XElement> levels = from row in xDoc.Descendants("Level") select row;
                IEnumerable<XElement> waves = from row in xDoc.Descendants("Wave") select row;

                Level level = new Level();
                IEnumerable<XAttribute> attList = from att in levels.DescendantsAndSelf().Attributes() select att;
                foreach (XAttribute xAtt in attList)
                {
                    if (xAtt.Name == "width")
                    {
                        level.Width = int.Parse(xAtt.Value);
                    }
                    if (xAtt.Name == "height")
                    {
                        level.Height = int.Parse(xAtt.Value);
                    }
                }

                foreach (XElement waveNode in waves)
                {
                    level.AddWave(waveNode.Value);
                }
                level.ID = -1;                                  //negative levels indicate custom levels
                level.Name = file.DisplayName;
                AllLevels.CustomLevels.Add(level);
            }
        }

        /// <Summary>
        /// static async Task<bool> DoesFileExistAsync(string fileName): 
        ///     Asynchronously checks to see if a file exists.
        /// </Summary>
        static async Task<bool> DoesFileExistAsync(string fileName)
        {
            try
            {
                StorageFolder myGames = await Global.GameDirectory.CreateFolderAsync("My Games", CreationCollisionOption.OpenIfExists);
                StorageFolder storageFolder = await myGames.CreateFolderAsync("Makin Pancakes", CreationCollisionOption.OpenIfExists);
                await storageFolder.GetFileAsync(fileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <Summary>
        /// static async Task<bool> DoesFileExistAsync(string fileName): 
        ///     Asynchronously reads the Player Save data.
        /// </Summary>
        public static async void ReadPlayerData()
        {
            //XDocument xDoc = XDocument.Load("XML/PlayerSave.xml");
            StorageFolder myGames = await Global.GameDirectory.CreateFolderAsync("My Games", CreationCollisionOption.OpenIfExists);
            StorageFolder storageFolder = await myGames.CreateFolderAsync("Makin Pancakes", CreationCollisionOption.OpenIfExists);
            StorageFile playerSave;
            bool exists = await DoesFileExistAsync("PlayerData.xml");
            if (exists)
            {
                playerSave = await storageFolder.GetFileAsync("PlayerData.xml");
            }
            else
            {
                XMLWriter.WriteBlankPlayerData("PlayerData");
                await Task.Delay(TimeSpan.FromSeconds(1));
                playerSave = await storageFolder.GetFileAsync("PlayerData.xml");
            }
            XmlDocument xmlDoc = await XmlDocument.LoadFromFileAsync(playerSave);
            XDocument xDoc = XDocument.Parse(xmlDoc.GetXml());

            //IEnumerable<XElement> playerStats = from row in xDoc.Descendants("Statistics") select row;
            IEnumerable<XElement> stars = from row in xDoc.Descendants("Stars") select row;
            IEnumerable<XElement> items = from row in xDoc.Descendants("Items") select row;
            IEnumerable<XElement> cubes = from row in xDoc.Descendants("CubesSaved") select row;
            IEnumerable<XElement> abilities = from row in xDoc.Descendants("Abilities") select row;
            /*foreach (XElement statNode in playerStats)
            {
                IEnumerable<XElement> cubeList = from cubes in statNode.Descendants("CubesSaved") select cubes;
                foreach (XElement x in cubeList)
                {
                    Debug.WriteLine(x.Value);
                    PlayerStats.TotalBlocksDestroyed = int.Parse(x.Value);
                }

                IEnumerable<XElement> gamePlayedList = from games in statNode.Descendants("GamesPlayed") select games;
                foreach (XElement x in gamePlayedList)
                {
                    PlayerStats.TotalGamesPlayed = int.Parse(x.Value);
                }
            }*/

            foreach (XElement cubeNode in cubes)
            {
                IEnumerable<XAttribute> cubeAttList = from cube in cubeNode.Attributes() select cube;
                foreach (XAttribute att in cubeAttList)
                {
                    PlayerStats.TotalBlocksDestroyed = int.Parse(att.Value);
                }
            }

            foreach (XElement highScoreNode in stars)
            {
                IEnumerable<XElement> starList = from star in highScoreNode.Descendants("Star") select star;
                foreach (XElement x in starList)
                {
                    PlayerStats.Stars.Add(int.Parse(x.Value));
                }
            }

            foreach (XElement itemNode in items)
            {
                IEnumerable<XElement> itemList = from item in itemNode.Descendants("Item") select item;
                foreach (XElement item in itemList)
                {
                    IEnumerable<XAttribute> attList = from att in item.Attributes() select att;
                    foreach (XAttribute att in attList)
                    {
                        Debug.WriteLine("Item Attribute: " + att);
                        PlayerStats.AddItem(int.Parse(att.Value));
                        PlayerStats.AddItem(int.Parse(att.Value));

                    }
                }
            }

            foreach (XElement abilityNode in abilities)
            {
                IEnumerable<XElement> abilityList = from ability in abilityNode.Descendants("Skill") select ability;
                foreach (XElement ability in abilityList)
                {
                    IEnumerable<XAttribute> skillAttList = from skillAtt in ability.Attributes() select skillAtt;
                    foreach (XAttribute skillAtt in skillAttList)
                    {
                        Debug.WriteLine(skillAtt);
                        //create abilities
                    }
                }
            }
        }

        /// <Summary>
        /// public static void ReadLevels():
        ///     Reads the levels created by me.
        /// </Summary>
        public static void ReadLevels()
        {
            AllLevels.Levels = new List<Level>();
            XDocument xDoc = XDocument.Load("XML/Level.xml");

            IEnumerable<XElement> levelList = from row in xDoc.Descendants("Level") select row;

            foreach (XElement levelNode in levelList)
            {
                Debug.WriteLine(levelNode.Name);
                Level currentLevel = AllLevels.CreateLevel();

                IEnumerable<XAttribute> attList = from att in levelNode.DescendantsAndSelf().Attributes() select att;

                foreach (XAttribute xAtt in attList)
                {
                    if (xAtt.Name == "width")
                    {
                        Debug.WriteLine("actual width " + xAtt);
                        currentLevel.Width = int.Parse(xAtt.Value);
                    }
                    if (xAtt.Name == "height")
                    {
                        Debug.WriteLine("actual height " + xAtt);
                        currentLevel.Height = int.Parse(xAtt.Value);
                    }
                }

                IEnumerable<XElement> waveList = from waves in levelNode.Descendants("Wave") select waves;

                foreach (XElement wave in waveList)
                {
                    Debug.WriteLine(wave.Value);
                    currentLevel.AddWave(wave.Value);
                }
            }
        }
    }
}
