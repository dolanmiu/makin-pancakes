﻿using Microsoft.Xna.Framework;
using System;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Timer Component Class: Every timer in the game uses this class
    /// </Summary>
    public class TimerComponent
    {
        private double time = 0;
        private double interval;
        private Boolean isTicking;

        /// <Summary>
        /// public TimerComponent(double interval):
        ///     Timer Component constructor.
        /// </Summary>
        public TimerComponent(double interval)
        {
            this.interval = interval;
        }

        /// <Summary>
        /// public void Update(GameTime gameTime):
        ///     Updates the Timer per frame.
        /// </Summary>
        public void Update(GameTime gameTime) 
        {
            time += gameTime.ElapsedGameTime.TotalSeconds;
            Check();
        }

        /// <Summary>
        /// private Boolean Check():
        ///     Checks the state of the Timer.
        /// </Summary>
        private Boolean Check()
        {
            if (time > interval)
            {
                time = 0.0f;
                isTicking = true;
                return true;
            }
            else
            {
                isTicking = false;
                return false;
            }
        }

        /// <Summary>
        /// public void Reset():
        ///     Resets the Timer to the start.
        /// </Summary>
        public void Reset()
        {
            time = 0.0f;
        }

        /// <Summary>
        /// public void Reset():
        ///     Resets the Timer to a certain time.
        /// </Summary>
        public void Reset(float time1)
        {
            time = time1;
        }

        #region Properties
        public double Interval
        {
            set { interval = value; }
        }

        public Boolean Ticking
        {
            get { return isTicking; }
        }
        #endregion
    }
}
