﻿
namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Item List Class: Contains all the items obtainable in the game.
    /// </Summary>
    public static class ItemList
    {
        /// <Summary>
        /// public static Item CreateItem(int index):
        ///     Create the items for the game. Perhaps put this in a seperate class later.
        /// </Summary>
        public static Item CreateItem(int index)
        {
            Item item = null;
            switch (index)
            {
                case 0:
                    item = new Item("Fortunate Heart", 30, "Particles/Heart Trail");
                    item.ID = 0;
                    break;
                case 1:
                    break;
            }
            return item;
        }
    }
}
