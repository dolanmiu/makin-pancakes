﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile.Redundant_Classes
{
    class CubeWave
    {

        private List<Cube> cubes;
        private ThirdPersonCamera camera;
        private int tileWidth, tileHeight;
        private TimerComponent cubeTimer;
        private int cubeColumns;
        private Boolean cleared;
        Random randomNumber = new Random();

        public CubeWave(ThirdPersonCamera camera, int tileWidth, int tileHeight, int cubeColumns)
        {
            this.cubeColumns = cubeColumns;
            this.camera = camera;
            this.tileHeight = tileHeight;
            this.tileWidth = tileWidth;
            cubes = new List<Cube>();
            cubeTimer = new TimerComponent(4);
        }

        #region Unused Code
        /*public void CreateSetCubes(ContentManager Content, int[,] cubeLevel)
        {
            for (int i = 0; i < cubeLevel.GetLength(0); i++)
            {
                for (int j = 0; j < cubeLevel.GetLength(1); j++)
                {
                    cubeSelection(Content, cubeLevel[i,j], i, j);
                    
                }
            }
        }

        private Cube cubeSelection(ContentManager Content, int number, int x, int y)
        {
            switch (number)
            {
                case 0:
                    Cube cube = new Cube();
                    cube.Position = new Vector3(x * tileWidth, 0.5f, y * tileHeight);
                    cube.Camera = camera;
                    cube.SetAnimation();
                    cube.Load(Content);
                    cubes.Add(cube);
                    return cube;
                    //Debug.WriteLine("Cube 0 created");
                case 1:
                    //Debug.WriteLine("Cube 1 created");
                    break;
                case 2:
                    //Debug.WriteLine("Cube 2 created");
                    break;
            }
            return null;
        }

        public void CreateSetCubes(ContentManager Content, int size, int[] cubeArray)
        {
            cleared = false;
            Cube[,] placedCubes = new Cube[cubeColumns, size];
            Random randomPosX = new Random();
            Random randomPosY = new Random();
            Boolean placedDown = false;
            for (int i = 0; i < cubeArray.Length; i++) //cube type
            {
                
                for (int j = 0; j < cubeArray[i]; j++) //each cube for a given type.
                {
                    placedDown = false;
                    while (!placedDown)
                    {
                        int posX = randomPosX.Next(cubeColumns);
                        int posY = randomPosY.Next(size);
                        if (placedCubes[posX, posY] == null)
                        {
                            Cube cube = cubeSelection(Content, i, posX, posY);
                            placedCubes[posX, posY] = cube;
                            placedDown = true;
                        }
                    }
                }
            }
        }

        public void CreateRandomCubes(ContentManager Content, int size, int randomTo) 
        {
            cleared = false;
            for (int i = 0; i < cubeColumns; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    cubeSelection(Content, WeightedAverage(new int[] { 1, 2, 3 }), i, j);
                }
            }
        }

        /*private Cube SelectCube()
        {

        }
        

        private int WeightedAverage(int[] weightedRandomness)
        {
            int totalRandom = 0;
            int counter = 0;
            for (int i = 0; i < weightedRandomness.Length; i++)
            {
                totalRandom += weightedRandomness[i];
            }
            int result = randomNumber.Next(totalRandom);
            Debug.WriteLine(result);
            for (int i = 0; i < weightedRandomness.Length; i++)
            {
                if (result >= counter && result < counter+weightedRandomness[i]) return i;
                counter += weightedRandomness[i];
            }
            return -1;
        }*/
        #endregion

        public void Update(GameTime gameTime)
        {
            for (int i = 0; i < cubes.Count; i++)
            {
                //cubes[i].Update(gameTime, cubeTimer);
            }
            //ResetAnimation(gameTime);

            if (cubes.Count == 0)
            {
                cleared = true;
            }
            else
            {
                cleared = false;
            }
        }

        private void ResetAnimation(GameTime gameTime)
        {
            cubeTimer.Update(gameTime);
            if (cubeTimer.Ticking)
            {
                for (int i = 0; i < cubes.Count; i++)
                    cubes[i].ShiftDown();
            }
        }

        public void Draw(Matrix viewMatrix)
        {
            for (int i = 0; i < cubes.Count; i++)
            {
                cubes[i].Draw(viewMatrix);
            }
        }

        #region Properties
        public List<Cube> Cubes
        {
            get { return cubes; }
        }

        public Boolean Cleared
        {
            get { return cleared; }
        }

        public TimerComponent Timer
        {
            get { return cubeTimer; }
            set { cubeTimer = value; }
        }
        #endregion
    }
}
