﻿using _3D_Game_Recompile.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Text;

namespace _3D_Game_Recompile.Redundant_Classes
{
    /// <Summary>
    /// Shop Screen Class: A screen showing the shop
    /// </Summary>
    class HighScoreScreen1 : Screen
    {
        private SpriteFont font;
        private Button backButton;

        public HighScoreScreen1(Game1 game) : base(game)
        {
            font = Global.Content.Load<SpriteFont>("Fonts/HighScoreFont");
            backButton = new Button(new Rectangle((int)Global.ScreenSize.X / 2, (int)Global.ScreenSize.Y - 200, 100, 50), "Back", menuFont, Alignment.Center);
        }

        public void Update()
        {
            base.BaseUpdate();
            backButton.Update();
            if (keyboardState.IsKeyDown(Keys.Back) && lastKeyboardState.IsKeyUp(Keys.Back))
            {
                game.StartScreen();
            }
            lastKeyboardState = keyboardState;

            if (backButton.Clicked()) game.StartScreen();
        }

        public void Draw(Color[] skyColours)
        {
            base.BaseDraw("Shop", skyColours);
            StringBuilder buffer = new StringBuilder();
            buffer.AppendLine("High Scores!");
            buffer.AppendLine();
            Global.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            Global.SpriteBatch.DrawString(font, buffer.ToString(), new Vector2((int)Global.ScreenSize.X / 2 - 300, 300), Color.Black);
            backButton.Draw();
            Global.SpriteBatch.End();
        }
    }
}
