using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using _3D_Game_Recompile.Screens;
using Microsoft.Xna.Framework.Media;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Game1 Class: The main Game class
    /// </Summary>
    public class Game1 : Game
    {
        public GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private static GameState gameState;
        private StartScreen startScreen;
        private LevelSelectionScreen levelScreen;
        private InGameScreen inGameScreen;
        private ShopScreen ShopScreen;
        private HowToPlayScreen howToPlayScreen;
        private OptionsScreen optionsScreen;
        private CreditsScreen creditsScreen;
        private LevelDesignerScreen levelDesignerScreen;
        private Color skyColourDay, skyColourEvening, skyColourNight;
        private Color[] skyColours;
        private float dayTimeValue = 0;

        /// <Summary>
        /// public Game1():
        ///     Game constructor.
        /// </Summary>
        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <Summary>
        /// protected override void Initialize():
        ///     Initialize the game.
        /// </Summary>
        protected override void Initialize()
        {
            base.Initialize();
            XMLReader.ReadPlayerData();
            XMLReader.ReadCustomLevels();

            StartScreen();
            gameState = GameState.StartScreen;
            SoundPlayer.PlaySong("menuMusic");

            skyColourDay = new Color(197, 237, 246);
            skyColourEvening = new Color(221, 88, 88);
            skyColourNight = new Color(24, 35, 70);
            skyColours = new Color[3];

            ShaderLoader.ReadShader();
        }

        /// <Summary>
        /// protected override void LoadContent():
        ///     Loads all the assets of the game.
        /// </Summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            Global.ScreenSize = new Vector2(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            Global.GraphicsDevice = GraphicsDevice;
            Global.Content = Content;
            Global.SpriteBatch = Spritebatch;

            SoundPlayer.LoadMusic();
        }

        /// <Summary>
        /// protected override void UnloadContent():
        ///     Unloads all the assets not needed in the game.
        /// </Summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <Summary>
        /// protected override void Update(GameTime gameTime):
        ///     Updates the game per frame.
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// </Summary>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            switch (gameState)
            {
                case GameState.StartScreen:
                    if (startScreen != null)
                        startScreen.Update();
                    dayTimeValue += 0.001f;
                    skyColours = CalculateDayColour(dayTimeValue);
                    break;
                case GameState.LevelSelect:
                    if (levelScreen != null)
                        levelScreen.Update();
                    dayTimeValue += 0.001f;
                    skyColours = CalculateDayColour(dayTimeValue);
                    break;
                case GameState.InGame:
                    if (inGameScreen != null)
                        inGameScreen.Update(gameTime);
                    dayTimeValue += 0.001f;
                    skyColours = CalculateDayColour(dayTimeValue);
                    break;
                case GameState.Shop:
                    if (ShopScreen != null)
                        ShopScreen.Update();
                    dayTimeValue += 0.001f;
                    skyColours = CalculateDayColour(dayTimeValue);
                    break;
                case GameState.HowToPlay:
                    if (howToPlayScreen != null)
                        howToPlayScreen.Update();
                    dayTimeValue += 0.001f;
                    skyColours = CalculateDayColour(dayTimeValue);
                    break;
                case GameState.Settings:
                    if (optionsScreen != null)
                        optionsScreen.Update();
                    dayTimeValue += 0.001f;
                    skyColours = CalculateDayColour(dayTimeValue);
                    break;
                case GameState.LevelDesigner:
                    if (levelDesignerScreen != null)
                        levelDesignerScreen.Update(gameTime);
                    break;
                case GameState.Credits:
                    if (creditsScreen != null)
                        creditsScreen.Update();
                    dayTimeValue += 0.001f;
                    skyColours = CalculateDayColour(dayTimeValue);
                    break;
            }
            base.Update(gameTime);
        }

        private Color[] CalculateDayColour(float theTime)
        {
            Color[] skyColours = new Color[3];
            if (dayTimeValue < 1)
            {
                skyColours[0] = Color.Lerp(skyColourDay, skyColourEvening, dayTimeValue);
                skyColours[1] = Color.Lerp(Color.Transparent, Color.Transparent, dayTimeValue);
                skyColours[2] = Color.Lerp(Color.White, Color.White, dayTimeValue);
                return skyColours;
            }
            else if (dayTimeValue >= 1 && dayTimeValue < 2)
            {
                skyColours[0] = Color.Lerp(skyColourEvening, skyColourNight, dayTimeValue - 1);
                skyColours[1] = Color.Lerp(Color.Transparent, Color.White, dayTimeValue - 1);
                skyColours[2] = Color.Lerp(Color.White, Color.Transparent, dayTimeValue - 1);
                return skyColours;
            }
            else if (dayTimeValue >= 2 && dayTimeValue < 3)
            {
                skyColours[0] = Color.Lerp(skyColourNight, skyColourDay, dayTimeValue - 2);
                skyColours[1] = Color.Lerp(Color.White, Color.Transparent, dayTimeValue - 2);
                skyColours[2] = Color.Lerp(Color.Transparent, Color.White, dayTimeValue - 2);
                return skyColours;
            }
            else if (dayTimeValue >= 3)
            {
                dayTimeValue = 0;
            }
            return skyColours;
        }

        /// <Summary>
        /// protected override void Draw(GameTime gameTime):
        ///     Draws the game per frame.
        /// </Summary>
        protected override void Draw(GameTime gameTime)
        {
            switch (gameState)
            {
                case GameState.StartScreen:
                    if (startScreen != null)
                        startScreen.Draw(skyColours);
                    break;
                case GameState.LevelSelect:
                    if (levelScreen != null)
                        levelScreen.Draw(skyColours);
                    break;
                case GameState.InGame:
                    if (inGameScreen != null)
                    {
                        GraphicsDevice.Clear(Color.CornflowerBlue);
                        GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                        //DepthStencilState depthBufferState = new DepthStencilState();
                        //depthBufferState.DepthBufferEnable = true;
                        //GraphicsDevice.DepthStencilState = depthBufferState;
                        inGameScreen.Draw();
                    }
                    break;
                case GameState.Shop:
                    if (ShopScreen != null)
                        ShopScreen.Draw(skyColours);
                    break;
                case GameState.HowToPlay:
                    if (howToPlayScreen != null)
                        howToPlayScreen.Draw(skyColours);
                    break;
                case GameState.LevelDesigner:
                    if (levelDesignerScreen != null)
                        levelDesignerScreen.Draw();
                    break;
                case GameState.Settings:
                    if (optionsScreen != null)
                        optionsScreen.Draw(skyColours);
                    break;
                case GameState.Credits:
                    if (creditsScreen != null)
                        creditsScreen.Draw(skyColours);
                    break;
            }
            base.Draw(gameTime);
        }

        /// <Summary>
        /// public void ShowOptions():
        ///     Shows the Options screen.
        /// </Summary>
        public void ShowOptions()
        {
            NullifyScreens();
            optionsScreen = new OptionsScreen(this);
            gameState = GameState.Settings;
        }

        /// <Summary>
        /// public void ShowCredits()
        ///     Shows the Credits screen.
        /// </Summary>
        public void ShowCredits()
        {
            NullifyScreens();
            creditsScreen = new CreditsScreen(this);
            gameState = GameState.Credits;
        }

        /// <Summary>
        /// public void ShowLevelDesigner()
        ///     Shows the Level Designer screen.
        /// </Summary>
        public void ShowLevelDesigner()
        {
            NullifyScreens();
            levelDesignerScreen = new LevelDesignerScreen(this);
            gameState = GameState.LevelDesigner;
        }

        /// <Summary>
        /// public void StartGame(Level level)
        ///     Starts the game.
        /// </Summary>
        public void StartGame(Level level)
        {
            NullifyScreens();
            inGameScreen = new InGameScreen(this, level);
            gameState = GameState.InGame;
        }

        /// <Summary>
        /// public void ShowLevelDesigner()
        ///     Shows the Level Designer screen.
        /// </Summary>
        public void ShopMenu()
        {
            NullifyScreens();
            ShopScreen = new ShopScreen(this);
            gameState = GameState.Shop;
        }

        /// <Summary>
        /// public void HowToPlayScreen()
        ///     Shows the Tutorial screen.
        /// </Summary>
        public void HowToPlayScreen()
        {
            NullifyScreens();
            howToPlayScreen = new HowToPlayScreen(this);
            gameState = GameState.HowToPlay;
        }

        /// <Summary>
        /// public void StartScreen()
        ///     Shows the Start screen.
        /// </Summary>
        public void StartScreen()
        {
            NullifyScreens();
            startScreen = new StartScreen(this);
            gameState = GameState.StartScreen;
        }

        /// <Summary>
        /// public void LevelSelect()
        ///     Shows the Level Select screen.
        /// </Summary>
        public void LevelSelect()
        {
            NullifyScreens();
            levelScreen = new LevelSelectionScreen(this);
            gameState = GameState.LevelSelect;
        }

        /// <Summary>
        /// public void NullifyScreens()
        ///     Kills all screens before a new one is set in place to save memory.
        /// </Summary>
        public void NullifyScreens()
        {
            inGameScreen = null;
            startScreen = null;
            ShopScreen = null;
            howToPlayScreen = null;
            creditsScreen = null;
            optionsScreen = null;
            
        }

        #region Properties
        public GameState GameState
        {
            get { return gameState; }
        }

        public SpriteBatch Spritebatch
        {
            get { return _spriteBatch; }
        }
        #endregion
    }
}
