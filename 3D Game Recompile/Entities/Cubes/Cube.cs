﻿using _3D_Game_Recompile.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Diagnostics;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Cube Class: The famous cube in gameplay seen everywhere
    /// </Summary>
    public abstract class Cube : BaseEntity
    {
        public Vector3 animationAdd;
        public Matrix totalTranslation;
        private ObjectAnimation cubeAnim;
        private ObjectAnimation boundingBoxAnim;
        private bool fallingDown;
        private bool sunk;
        private string contentString = "defaultCube";
        private Vector3 animationPosition;
        private float rollTime = 1.5f;
        private SoundEffect hitGroundSound;
        private bool soundPlayed;

        /// <Summary>
        /// public Cube()
        ///     The cube constructor
        /// </Summary>
        public Cube()
        {
            animationAdd = new Vector3(0, 0, 0);
            fallingDown = false;
            sunk = false;
        }

        /// <Summary>
        /// public void SetAnimation()
        ///     Assign an animation to the cube
        /// </Summary>
        public void SetAnimation()
        {
            cubeAnim = new ObjectAnimation(Position, new Vector3(Position.X, Position.Y, Position.Z + 2), Vector3.Zero, new Vector3(MathHelper.PiOver2, 0, 0), TimeSpan.FromSeconds(rollTime), false, false);
            boundingBoxAnim = new ObjectAnimation(Position, new Vector3(Position.X, Position.Y, Position.Z + 2), Vector3.Zero, Vector3.Zero, TimeSpan.FromSeconds(rollTime), false, true);
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera)
        ///     Load the assets for the cube
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            base.Load(contentString, camera);
            //soundfile = TitleContainer.OpenStream(@"Content\Sounds\blockGroundSound");
            hitGroundSound = Global.Content.Load<SoundEffect>("Sounds/blockGroundSound");
        }

        /// <Summary>
        /// public void Update(GameTime gameTime, TimerComponent cubeTimer)
        ///     Updates the cube at every frame
        /// </Summary>
        public void Update(Wave wave, GameTime gameTime, TimerComponent cubeTimer)
        {
            if (!wave.Snared)
            {
                base.BaseUpdate();
                cubeAnim.Update(gameTime.ElapsedGameTime);
                boundingBoxAnim.Update(gameTime.ElapsedGameTime);
                animationPosition = boundingBoxAnim.Position;
                if (!fallingDown && cubeTimer.Ticking)
                {
                    ResetAnimation(cubeTimer);
                }

                Vector3 pointToRotateAround = transformMatrix.Translation + transformMatrix.Down + transformMatrix.Backward;
                transformMatrix.Translation -= pointToRotateAround;
                transformMatrix *= Matrix.CreateFromAxisAngle(new Vector3(1, 0, 0), cubeAnim.Rotation.X);
                transformMatrix.Translation += pointToRotateAround;
            }
            else
            {
                if (cubeTimer.Ticking)
                {
                    wave.Snared = false;
                }
            }

            if (cubeAnim.Amt == 1)
            {
                if (soundPlayed == false)
                {
                    SoundPlayer.Play(hitGroundSound, 0.2f, -0.6f, (float)Global.GetRandomNumber(-1, 1));
                    soundPlayed = true;
                }
            }
            else
            {
                soundPlayed = false;
            }
        }

        /// <Summary>
        /// public void ResetAnimation(TimerComponent cubeTimer)
        ///     Resets the rolling animation of the cube
        /// </Summary>
        public void ResetAnimation(TimerComponent cubeTimer)
        {
            ShiftDown();
            MoveCube();
        }


        /// <Summary>
        /// private void MoveCube()
        ///     Resets the moving animation of the cube, hence move the cube
        /// </Summary>
        private void MoveCube()
        {
            cubeAnim.reset(Position, new Vector3(Position.X, Position.Y, Position.Z + 2));
            boundingBoxAnim.reset(Position, new Vector3(Position.X, Position.Y, Position.Z + 2));
        }

        /// <Summary>
        /// public void MoveFast(TimerComponent cubeTimer)
        ///     Quickly moves the cube to finish level
        /// </Summary>
        public void MoveFast(TimerComponent cubeTimer)
        {
            cubeAnim.Duration = TimeSpan.FromSeconds(0.2f);
            boundingBoxAnim.Duration = TimeSpan.FromSeconds(0.2);
            cubeTimer.Interval = 0.5;
        }

        /// <Summary>
        /// public void ShiftDown()
        ///     Shifts the real position of the cube
        /// </Summary>
        public void ShiftDown()
        {
            Position = boundingBoxAnim.Position; //this part weird - next position - fixed - now previous position
        }

        /// <Summary>
        /// public void FallDown()
        ///     Cause cube to fall
        /// </Summary>
        public void FallDown()
        {
            fallingDown = true;
            Position -= new Vector3(0, 0.3f, 0);
        }

        /// <Summary>
        /// public abstract void DetonationAction(Tile quad, PlayScreen screen);
        ///     Extra actions needed when a cube is detonated.
        /// </Summary>
        public abstract void DetonationAction(Tile quad, PlayScreen screen);

        #region Properties
        public string ModelName
        {
            get { return contentString; }
            set { contentString = value; }
        }

        public ObjectAnimation Animation
        {
            get { return cubeAnim; }
        }

        public Vector3 AnimationPosition
        {
            get { return animationPosition; }
        }

        public Vector3 AnimationPositionBottomRight
        {
            get
            {
                Vector3 tempVector = animationPosition - new Vector3(1, 0, 1);
                return tempVector;
            }
        }

        public Vector3 AnimationPositionTopLeft
        {
            get
            {
                Vector3 tempVector = animationPosition - new Vector3(-1, 1, -1);
                return tempVector;
            }
        }

        public bool Sunk
        {
            get { return sunk; }
            set { sunk = value; }
        }

        public float Interval
        {
            get { return cubeAnim.Duration.Seconds; }
            set
            {
                cubeAnim.Duration = TimeSpan.FromSeconds(value);
                boundingBoxAnim.Duration = TimeSpan.FromSeconds(value);
            }
        }
        #endregion
    }
}
