﻿using _3D_Game_Recompile.Screens;

namespace _3D_Game_Recompile.Entities
{
    /// <Summary>
    /// DefaultCube Class: The standard white cube
    /// </Summary>
    class DefaultCube : Cube
    {
        /// <Summary>
        /// public DefaultCube()
        ///     Cube constructor
        /// </Summary>
        public DefaultCube()
        {
            ModelName = "defaultCube";
        }

        /// <Summary>
        /// public override void DetonationAction(Tile quad, PlayScreen screen)
        ///     What happens when a tile is detonated
        /// </Summary>
        public override void DetonationAction(Tile quad, PlayScreen screen)
        {
            PlayerStats.GoodBlocks++;
        }
    }
}
