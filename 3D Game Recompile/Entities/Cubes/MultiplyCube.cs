﻿using _3D_Game_Recompile.Screens;

namespace _3D_Game_Recompile.Entities
{
    /// <Summary>
    /// DefaultCube Class: The standard white cube
    /// </Summary>
    class MultiplyCube : Cube
    {
        /// <Summary>
        /// public DefaultCube()
        ///     Cube constructor
        /// </Summary>
        public MultiplyCube()
        {
            ModelName = "multiplyCube";
        }

        /// <Summary>
        /// public override void DetonationAction(Tile quad, PlayScreen screen)
        ///     What happens when a tile is detonated
        /// </Summary>
        public override void DetonationAction(Tile quad, PlayScreen screen)
        {
            screen.PlayGrid.MarkMultiplytile(quad);
            screen.PlayGrid.CurrentWave.Snared = true;
        }
    }
}
