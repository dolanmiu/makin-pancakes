﻿using _3D_Game_Recompile.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile.Entities
{
    /// <Summary>
    /// DefaultCube Class: The forbidden black cube
    /// </Summary>
    class ForbiddenCube : Cube
    {
        /// <Summary>
        /// public ForbiddenCube()
        ///     Cube constructor
        /// </Summary>
        public ForbiddenCube()
        {
            ModelName = "darkCube";
        }

        /// <Summary>
        /// public override void DetonationAction(Tile quad, PlayScreen screen)
        ///     What happens when a tile is detonated
        /// </Summary>
        public override void DetonationAction(Tile quad, PlayScreen screen)
        {
            //PlayerStats.BadBlocks++;
            /*if (PlayerStats.AddNextRows == 1)
            {
                PlayerStats.AddNextRows -= 2;
            }
            else
            {
                PlayerStats.AddNextRows--;
            }*/
            PlayerStats.AddNextRows = 0;
            screen.Extend(-1);
        }
    }
}
