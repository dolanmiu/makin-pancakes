﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _3D_Game_Recompile.Entities
{
    class Grass : BaseEntity
    {
        /// <Summary>
        /// public Grass(Vector3 spawnPos)
        ///     Grass constructor.
        /// </Summary>
        public Grass(Vector3 spawnPos)
        {
            Position = spawnPos;
            ScaleMatrix = Matrix.CreateScale((float)Global.GetRandomNumber(0.1, 0.4));
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera)
        ///     Load the fence's assets.
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            base.Load("sceneGrass", camera);
        }

        /// <Summary>
        /// public void Update()
        ///     Updates the fence per frame.
        /// </Summary>
        public void Update()
        {
            base.BaseUpdate();
        }

        /// <Summary>
        /// new public void Draw(Matrix viewMatrix)
        ///     Draws the fence per frame.
        /// </Summary>
        new public void Draw(Matrix viewMatrix)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();

                    effect.World = world * transforms[mesh.ParentBone.Index] * scaleMatrix * RotationMatrix * transformMatrix; //ISROT
                    effect.View = Matrix.Identity;
                    effect.Projection = camera.ViewProjectionMatrix;
                }
                mesh.Draw();
            }
        }
    }
}
