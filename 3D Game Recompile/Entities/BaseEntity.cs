﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// BaseEntity Class: This is the core class for representing objects in my game with a 3D model.
    /// </Summary>
    public abstract class BaseEntity
    {
        //private Vector3 modelRotation;
        protected Vector3 modelPosition;
        protected Matrix world;
        protected Matrix view;
        protected Matrix projection;
        protected Matrix scaleMatrix;
        protected Matrix transformMatrix;
        private Matrix rotationMatrix;
        protected Model model;
        protected ThirdPersonCamera camera;
        protected enum types { NORMAL, PLAYER };
        protected types kind;

        /// <Summary>
        /// protected BaseEntity(): 
        ///     Is the constructor for the Documentation class.
        /// </Summary>
        protected BaseEntity()
        {
            world = Matrix.Identity;
            view = Matrix.Identity;

            transformMatrix = Matrix.Identity;
            rotationMatrix = Matrix.Identity;
            scaleMatrix = Matrix.Identity;
        }

        /// <Summary>
        /// protected void Load(String name, ThirdPersonCamera camera): 
        ///     Loads the object.
        /// </Summary>
        protected void Load(String name, ThirdPersonCamera camera)
        {
            model = Global.Content.Load<Model>(name);
            this.camera = camera;
        }

        /// <Summary>
        /// protected void BaseUpdate(): 
        ///     All objects which call this in their own Update method.
        /// </Summary>
        protected void BaseUpdate()
        {
            transformMatrix = Matrix.CreateTranslation(modelPosition);
        }

        /// <Summary>
        /// public virtual void Draw(Matrix viewMatrix): 
        ///     All objects which call this in their own Draw method. 
        /// </Summary>
        public virtual void Draw(Matrix viewMatrix)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = world * scaleMatrix * rotationMatrix * transformMatrix; //ISROT
                    //effect.World = world * Matrix.CreateRotationX(modelRotation) * Matrix.CreateTranslation(modelPosition);
                    //effect.DirectionalLight0.DiffuseColor = Color.Yellow.ToVector3();
                    effect.View = Matrix.Identity;
                    effect.Projection = camera.ViewProjectionMatrix;
                }
                mesh.Draw();
            }
        }

        #region Properties
        public Matrix Projection
        {
            set { projection = value; }
            get { return projection; }
        }

        public Matrix ScaleMatrix
        {
            get { return scaleMatrix; }
            set { scaleMatrix = value; }
        }

        public Matrix TransformMatrix
        {
            get { return transformMatrix; }
            set { transformMatrix = value; }
        }

        public Matrix RotationMatrix
        {
            get { return rotationMatrix; }
            set { rotationMatrix = value; }
        }

        public Model Model
        {
            get { return model; }
            set { model = value; }
        }

        public Vector3 Position
        {
            get { return modelPosition; }
            set { modelPosition = value; }
        }

        public Matrix World
        {
            get { return world; }
        }

        public Matrix View
        {
            get { return view; }
        }
        #endregion
    }
}
