﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace _3D_Game_Recompile.Core_Game_System
{

    /// <Summary>
    /// Quad Class: A class for all 2 dimensional 3D objects
    /// </Summary>
    public abstract class Quad
    {
        protected VertexPositionNormalTexture[] Vertices;
        protected short[] Indexes;
        protected float Width, Height;
        protected Vector3 origin, normal, up, left;
        protected Vector3 upperLeft, upperRight, lowerLeft, lowerRight;
        protected BasicEffect quadEffect;
        protected ThirdPersonCamera camera;
        protected Matrix worldMatrix;

        /// <Summary>
        /// public Quad(Vector3 origin, Vector3 normal, Vector3 up, float width, float height)
        ///     Constructor for the Quad class
        /// </Summary>
        public Quad(Vector3 origin, Vector3 normal, Vector3 up, float width, float height)
        {
            this.Vertices = new VertexPositionNormalTexture[4];
            this.Indexes = new short[6];
            this.normal = normal;
            this.origin = origin;
            this.up = up;
            this.Height = height;
            this.Width = width;
            this.worldMatrix = Matrix.Identity;
            SetLocation();
        }

        /// <Summary>
        /// public void SetLocation()
        ///     Sets the location of the quad
        /// </Summary>
        public void SetLocation()
        {
            // Calculate the quad corners
            left = Vector3.Cross(normal, up);
            Vector3 uppercenter = (up * Height / 2) + origin;
            upperLeft = uppercenter + (left * Width / 2);
            upperRight = uppercenter - (left * Width / 2);
            lowerLeft = upperLeft - (up * Height);
            lowerRight = upperRight - (up * Height);

            FillVertices();
        }

        /// <Summary>
        /// private void FillVertices()
        ///     Fills the verticies with a texture
        /// </Summary>
        private void FillVertices()
        {
            // Fill in texture coordinates to display full texture
            // on quad
            Vector2 textureUpperLeft = new Vector2(0.0f, 0.0f);
            Vector2 textureUpperRight = new Vector2(1.0f, 0.0f);
            Vector2 textureLowerLeft = new Vector2(0.0f, 1.0f);
            Vector2 textureLowerRight = new Vector2(1.0f, 1.0f);

            // Provide a normal for each vertex
            for (int i = 0; i < Vertices.Length; i++)
            {
                Vertices[i].Normal = normal;
            }

            Vertices[0].Position = lowerLeft;
            Vertices[0].TextureCoordinate = textureLowerLeft;
            Vertices[1].Position = upperLeft;
            Vertices[1].TextureCoordinate = textureUpperLeft;
            Vertices[2].Position = lowerRight;
            Vertices[2].TextureCoordinate = textureLowerRight;
            Vertices[3].Position = upperRight;
            Vertices[3].TextureCoordinate = textureUpperRight;

            // Set the index buffer for each vertex, using
            // clockwise winding
            Indexes[0] = 0;
            Indexes[1] = 1;
            Indexes[2] = 2;
            Indexes[3] = 2;
            Indexes[4] = 1;
            Indexes[5] = 3;
        }

        /// <Summary>
        /// protected void BaseLoad(ThirdPersonCamera camera)
        ///     All objects which inherit Quad must use this in their Load() function
        /// </Summary>
        protected void BaseLoad(ThirdPersonCamera camera)
        {
            this.camera = camera;
            quadEffect = new BasicEffect(Global.GraphicsDevice);
            quadEffect.TextureEnabled = true;
            //quadEffect.EnableDefaultLighting();
            quadEffect.World = worldMatrix;
            

            VertexDeclaration vertexDeclaration = new VertexDeclaration(new VertexElement[]
            {
                new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
                new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
                new VertexElement(24, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0)
            });
        }

        /// <Summary>
        /// protected void BaseDraw(Matrix viewMatrix)
        ///     All objects which inherit Quad must use this in their Draw() function
        /// </Summary>
        protected void BaseDraw(Matrix viewMatrix)
        {
            //quadEffect.View = viewMatrix;
            quadEffect.Projection = camera.ViewProjectionMatrix;
            foreach (EffectPass pass in quadEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                Global.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(PrimitiveType.TriangleList, Vertices, 0, 4, Indexes, 0, 2);
            }
            quadEffect.End();
        }

        #region Properties
        public Texture2D Texture
        {
            set { quadEffect.Texture = value; }
        }

        public Vector3 Origin
        {
            get { return origin; }
        }

        public Vector3 UpperLeft
        {
            get { return upperLeft; }
        }

        public Vector3 UpperRight
        {
            get { return upperRight; }
        }

        public Vector3 LowerLeft
        {
            get { return lowerLeft; }
        }

        public Vector3 LowerRight
        {
            get { return lowerRight; }
        }

        public Vector3 Position
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
                SetLocation();
            }
        }

        public Vector3 Up //increasing this expands
        {
            get { return up; }
            set
            {
                up = value;
                SetLocation();
                FillVertices();
            }
        }

        public Vector3 Normal //increasing this expands
        {
            get { return normal; }
            set
            {
                normal = value;
                SetLocation();
                FillVertices();
            }
        }
        #endregion

    }
}
