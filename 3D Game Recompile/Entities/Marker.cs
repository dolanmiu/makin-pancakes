﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Diagnostics;

namespace _3D_Game_Recompile.Entities
{
    /// <Summary>
    /// Marker Class: The icon which comes up on a tile when it is marked, fused of multiply selected.
    /// </Summary> 
    public class Marker : BaseEntity
    {
        private ObjectAnimation anim;
        private Model normal, green, red;

        /// <Summary>
        /// public Marker()
        ///     Marker constructor.
        /// </Summary>
        public Marker()
        {
            anim = new ObjectAnimation(Position, Position, Vector3.Zero, new Vector3(0, MathHelper.Pi * 2, 0), TimeSpan.FromSeconds(5), true, false);
            scaleMatrix = Matrix.CreateScale(0.09f);
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera)
        ///     Loads the marker assets.
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            base.Load("selectionCursor", camera);
            normal = Global.Content.Load<Model>("selectionCursor");
            green = Global.Content.Load<Model>("selectionCursorGreen");
            red = Global.Content.Load<Model>("selectionCursorRed");
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Updates the marker per frame.
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            base.BaseUpdate();
            anim.Update(gameTime.ElapsedGameTime);
            RotationMatrix = Matrix.CreateRotationY(anim.Rotation.Y);
        }

        /// <Summary>
        /// public void SetFused()
        ///     Sets the marker into a red fused one.
        /// </Summary>
        public void SetFused()
        {
            model = red;
        }

        /// <Summary>
        /// public void SetMultiply()
        ///     Sets the marker into a green multiply one.
        /// </Summary>
        public void SetMultiply()
        {
            model = green;
        }

        /// <Summary>
        /// public void SetSelected()
        ///     Sets the marker into a white selection one.
        /// </Summary>
        public void SetSelected()
        {
            model = normal;
        }
    }
}
