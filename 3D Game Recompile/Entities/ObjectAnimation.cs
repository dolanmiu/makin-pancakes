﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Object Animation Class: An object which stores the translational and rotational animation for an object.
    /// </Summary> 
    public class ObjectAnimation
    {
        private TimeSpan elapsedTime = TimeSpan.FromSeconds(0);
        private Vector3 startPosition, endPosition, startRotation, endRotation;
        private TimeSpan duration;
        private bool loop;
        private bool lerpPos;
        public bool finishedAnimating;
        private float amt;

        /// <Summary>
        /// public ObjectAnimation(Vector3 StartPosition, Vector3 EndPosition, Vector3 StartRotation, Vector3 EndRotation, TimeSpan Duration, bool Loop, bool lerpPos)
        ///     Object Animation constructor.
        /// </Summary>
        public ObjectAnimation(Vector3 StartPosition, Vector3 EndPosition, Vector3 StartRotation, Vector3 EndRotation, TimeSpan Duration, bool Loop, bool lerpPos)
        {
            this.startPosition = StartPosition;
            this.endPosition = EndPosition;
            this.startRotation = StartRotation;
            this.endRotation = EndRotation;
            this.duration = Duration;
            this.loop = Loop;
            this.lerpPos = lerpPos;
            Position = startPosition;
            Rotation = startRotation;
        }

        /// <Summary>
        /// public void Update(TimeSpan Elapsed)
        ///     Updates the animation per frame.
        /// </Summary>
        public void Update(TimeSpan Elapsed)
        {
            this.elapsedTime += Elapsed;                                                            // Update the time
            amt = (float)elapsedTime.TotalSeconds / (float)duration.TotalSeconds;             // Determine how far along the duration value we are (0 to 1)
            if (loop)
            {
                while (amt > 1)
                {                                                                                   // Wrap the time if we are looping
                    amt -= 1;
                }
            }
            else                                                                                    // Clamp to the end value if we are not
            {
                amt = MathHelper.Clamp(amt, 0, 1);                                                  // Update the current position and rotation
            }

            if (lerpPos)
            {
                Position = Vector3.Lerp(startPosition, endPosition, amt);
            }
            Rotation = Vector3.Lerp(startRotation, endRotation, amt);
        }

        /// <Summary>
        /// public void Process(BaseEntity entity)
        ///     Sets the position of the object into the animation position.
        /// </Summary>
        public void sProcess(BaseEntity entity) //not used? delete, dumb method
        {
            entity.Position = Position;
        }

        /// <Summary>
        /// public void reset(Vector3 startPos, Vector3 endPos)
        ///     Resets the animation to the beginging.
        /// </Summary>
        public void reset(Vector3 startPos, Vector3 endPos)
        {
            startPosition = startPos;
            endPosition = endPos;
            elapsedTime = TimeSpan.FromSeconds(0);
            Position = endPosition;
        }

        #region Properties
        public Vector3 Position { get; private set; }
        public Vector3 Rotation { get; private set; }
        public Vector3 StartPosition
        {
            get { return startPosition; }
            set { startPosition = value; }
        }

        public Vector3 EndPosition
        {
            get { return endPosition; }
            set { endPosition = value; }
        }

        public TimeSpan Duration
        {
            get { return duration; }
            set { duration = value; }
        }

        public TimeSpan ElapsedTime
        {
            get { return elapsedTime; }
        }

        public float Amt
        {
            get { return amt; }
        }
        #endregion
    }
}
