﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile.Entities
{
    /// <Summary>
    /// Fence Class: Fence object.
    /// </Summary> 
    class Fence : BaseEntity
    {
        /// <Summary>
        /// public Fence(Vector3 spawnPos)
        ///     Fence constructor.
        /// </Summary>
        public Fence(Vector3 spawnPos)
        {
            Position = spawnPos;
            ScaleMatrix = Matrix.CreateScale(0.1f);
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera)
        ///     Load the fence's assets.
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            base.Load("fence", camera);
        }

        /// <Summary>
        /// public void Update()
        ///     Updates the fence per frame.
        /// </Summary>
        public void Update()
        {
            base.BaseUpdate();
        }

        /// <Summary>
        /// new public void Draw(Matrix viewMatrix)
        ///     Draws the fence per frame.
        /// </Summary>
        new public void Draw(Matrix viewMatrix)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();

                    effect.World = world * transforms[mesh.ParentBone.Index] * scaleMatrix * RotationMatrix * transformMatrix; //ISROT
                    effect.View = Matrix.Identity;
                    effect.Projection = camera.ViewProjectionMatrix;
                }
                mesh.Draw();
            }
        }
    }
}
