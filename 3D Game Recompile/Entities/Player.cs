﻿using _3D_Game_Recompile.Entities.Particles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Diagnostics;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Player Class: The hero of the game.
    /// </Summary> 
    public class Player : BaseEntity
    {
        private float velocity;
        private Vector3 direction;
        private Vector3 modelVelocity;
        //Matrix projection1 = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45), 800f / 480f, 0.1f, 100f);
        private MovementTrail trail;
        private float currentRotation;
        private SplashParticles splashParticles;
        private bool sunk = false;

        /// <Summary>
        /// public Player()
        ///     Player constructor.
        /// </Summary>
        public Player()
        {
            base.world = Matrix.CreateTranslation(new Vector3(0, 0, 0));
            //view = Matrix.CreateLookAt(new Vector3(0, 0, 10), new Vector3(0, 0, 0), Vector3.UnitY); //What does this like.. do?
            base.projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45), 800f / 480f, 0.1f, 100f);
            base.modelPosition = new Vector3(0, 0, 8);
            currentRotation = (float)Math.PI;
            RotationMatrix = Matrix.CreateRotationY(currentRotation);
            modelVelocity = Vector3.Zero;
            trail = new MovementTrail();
            splashParticles = new SplashParticles();
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera)
        ///     Load the assets for the player.
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            base.Load("foxWalk", camera);
            trail.Load(camera);
            splashParticles.Load(camera);
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Updates the player per frame.
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            base.BaseUpdate();
            trail.Update(gameTime);
            splashParticles.Update(gameTime);
        }

        /// <Summary>
        /// public void Move(Vector3 direction)
        ///     Move the player in a certain direction.
        /// </Summary>
        public void Move(Vector3 direction)
        {
            this.direction = direction;
            modelPosition += direction * velocity;
            velocity += 0.01f;
            if (velocity >= 0.1f) velocity = 0.1f;
            trail.CreateTrail(5, Position, direction);

            currentRotation = LerpRotation(direction, currentRotation);
            RotationMatrix = Matrix.CreateRotationY(currentRotation);
        }

        /// <Summary>
        /// public float LerpRotation(Vector3 direction)
        ///     Face the fox to correct orientation;
        /// </Summary>
        public float LerpRotation(Vector3 direction, float currentRotation)
        {
            float destinationRotation = 0;
            if (direction != Vector3.Zero)
            {
                float dotProduct = Vector3.Dot(new Vector3(0, 0, 1), direction);
                double directionMag = Math.Pow(direction.X, 2) + Math.Pow(direction.Y, 2) + Math.Pow(direction.Z, 2);
                double cosTheta = dotProduct / directionMag;
                destinationRotation = (float)Math.Acos(cosTheta);
                if (direction.X == -1)
                {
                    destinationRotation = -destinationRotation;
                }
                Debug.WriteLine(destinationRotation);
                if (currentRotation <= -(float)Math.PI) currentRotation = -currentRotation;
                if (currentRotation >= (float)Math.PI) currentRotation = -currentRotation;

                if (direction == Vector3.Forward)
                {
                    if (currentRotation > 0)
                    {
                        if (currentRotation > Math.PI) return currentRotation;
                        currentRotation += 0.1f;
                    }
                    else
                    {
                        if (currentRotation < -Math.PI) return currentRotation;
                        currentRotation -= 0.1f;
                    }
                }

                    if (currentRotation > destinationRotation)
                    {
                        currentRotation -= 0.1f;
                    }
                    else
                    {
                        currentRotation += 0.1f;
                    }
            }
            return destinationRotation;
        }

        /// <Summary>
        /// public void SlowDown()
        ///     Slow the player down.
        /// </Summary>
        public void SlowDown()
        {
            velocity *= 0.5f;
            modelPosition += direction * velocity;
        }

        /// <Summary>
        /// public override void Draw(Matrix viewMatrix)
        ///     Draws the marker per frame.
        /// </Summary>
        public override void Draw(Matrix viewMatrix)
        {
            trail.Draw(viewMatrix);
            splashParticles.Draw(viewMatrix);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = world * RotationMatrix * Matrix.CreateTranslation(modelPosition);
                    effect.View = viewMatrix;
                    effect.Projection = projection;
                }
                mesh.Draw();
            }
        }

        /// <Summary>
        /// public void FallDown()
        ///     Make the player falldown into the water when off the stage.
        /// </Summary>
        public void FallDown()
        {
            Position -= new Vector3(0, 0.3f, 0);
            if (Position.Y < -2 && !sunk)
            {
                splashParticles.CreateSplash(new Vector3(Position.X, Position.Y, Position.Z + 2));
                sunk = true;
            }
        }

        #region Properties
        public float Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }
        #endregion
    }
}
