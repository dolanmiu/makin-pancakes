﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _3D_Game_Recompile.Entities.Particles
{
    /// <Summary>
    /// ExplosionMachine Class: The explosive effect when blowing up a cube
    /// </Summary>
    class ExplosionMachine : ParticleMachine
    {
        /// <Summary>
        /// public ExplosionMachine()
        ///     Explosion constructor
        /// </Summary>
        public ExplosionMachine()
        {
        }

        /// <Summary>
        /// public void CreateExplosion(int amount, Vector3 spawnPos)
        ///     Creates an explosion at a certain point and amount of particles of the explosion
        /// </Summary>
        public void CreateExplosion(int amount, Vector3 spawnPos)
        {
            Add2DPointSourceDynamicParticlesCylinder(amount, new Vector2(7, 7), BillBoardType.Spherical, spawnPos, new Vector2(-0.2f, 0.2f), new Vector2(0, 0), 0.1, 0.3, 0.9f, Vector3.Zero);
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera, Texture2D texture2D):
        ///     loads assets for the particle machine.
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            this.camera = camera;
            texture2D = Global.Content.Load<Texture2D>("Particles/Explosions/Explosion1");
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Udpates the explosion per frame
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            base.BaseUpdate(gameTime);
            ReduceAlpha(0.02f);
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the explosion per frame
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            Global.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            Global.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            base.BaseDraw(viewMatrix);
            Global.GraphicsDevice.BlendState = BlendState.Opaque;
            Global.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
        }
    }
}
