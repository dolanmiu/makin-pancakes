﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _3D_Game_Recompile.Entities.Particles
{
    /// <Summary>
    /// Movement Trail Class: Leaves a trail of particles for a given moving object
    /// </Summary> 
    class MovementTrail : ParticleMachine
    {
        private int count;

        /// <Summary>
        /// public MovementTrail()
        ///     Movement trail constructor
        /// </Summary>
        public MovementTrail()
        {
            count = 0;
            interval = 0.5;
        }

        /// <Summary>
        /// new public void Load(ThirdPersonCamera camera)
        ///     Loads the particle system
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            this.camera = camera;
            if (PlayerStats.ContainsItem("Fortunate Heart"))
            {
                texture2D = Global.Content.Load<Texture2D>("Particles/Heart Trail");
            }
            else
            {
                texture2D = Global.Content.Load<Texture2D>("Particles/Explosions/Explosion1");
            }
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Updates the movement trail per frame
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            base.BaseUpdate(gameTime);
            ReduceAlpha(0.04f);
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the movement trail per frame
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            Global.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            Global.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            base.BaseDraw(viewMatrix);
            Global.GraphicsDevice.BlendState = BlendState.Opaque;
            Global.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
        }

        /// <Summary>
        /// public void CreateTrail(int interval, Vector3 spawnPos, Vector3 direction)
        ///     Creates a trail given the position, direction to shoot in and an interval to puff out a particle
        /// </Summary>
        public void CreateTrail(int interval, Vector3 spawnPos, Vector3 direction)
        {
            count++;
            if (count >= interval)
            {
                Add2DPointSourceDynamicParticlesCylinder(1, new Vector2(3, 3), BillBoardType.Spherical, spawnPos, new Vector2(-0.1f,0.1f), new Vector2(-0.1f, 0.1f), 0.1, 0.3, 0.7f, Vector3.Zero);
                count = 0;
            }
        }
    }
}
