﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace _3D_Game_Recompile.Entities.Particles
{
    /// <Summary>
    /// Splash Particles Class: The splashes you see when a cube hits the water.
    /// </Summary> 
    class SplashParticles : ParticleMachine
    {
        SoundEffect splashSound1, splashSound2, splashSound3;
        //SoundEffectInstance splashSounds;
        AudioEmitter emitter = new AudioEmitter();
        AudioListener listener = new AudioListener();
        /// <Summary>
        /// public SplashParticles()
        ///     Splash particles constructor
        /// </Summary>
        public SplashParticles()
        {
            splashSound1 = Global.Content.Load<SoundEffect>("Sounds/splash1");
            splashSound2 = Global.Content.Load<SoundEffect>("Sounds/splash2");
            splashSound3 = Global.Content.Load<SoundEffect>("Sounds/splash3");
        }

        /// <Summary>
        /// public void CreateSplash(Vector3 spawnPos)
        ///     Creates a splash effect.
        /// </Summary>
        public void CreateSplash(Vector3 spawnPos)
        {
            Add2DPointSourceDynamicParticlesCylinder(10, new Vector2(3, 3), BillBoardType.Spherical, spawnPos, new Vector2(-0.04f, 0.04f), new Vector2(0, -0.05f), 0.1, 0.3, 1, Vector3.Up * 0.1f);
            switch (Global.GetRandomInt(0,3))
            {
                case 0:
                    SoundPlayer.Play(splashSound1);
                    //splashSounds = splashSound1.CreateInstance();
                    break;
                case 1:
                    SoundPlayer.Play(splashSound2);
                    //splashSounds = splashSound2.CreateInstance();
                    break;
                case 2:
                    SoundPlayer.Play(splashSound3);
                    //splashSounds = splashSound3.CreateInstance();
                    break;
            }
            //emitter.Position = spawnPos;
            //listener.Position = camera.Position;
            //splashSounds.Apply3D(listener, emitter);

            //splashSounds.Play();
            //splashSounds.State = SoundState.Stopped;
        }

        /// <Summary>
        /// new public void Load(ThirdPersonCamera camera)
        ///     Loads the particle system
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            this.camera = camera;
            texture2D = Global.Content.Load<Texture2D>("Particles/Water Particle");
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Updates the particle every frame.
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            base.BaseUpdate(gameTime);
            ReduceAlpha(0.02f);
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the particle every frame.
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            Global.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            Global.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            base.BaseDraw(viewMatrix);
            Global.GraphicsDevice.BlendState = BlendState.Opaque;
            Global.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
        }
    }
}
