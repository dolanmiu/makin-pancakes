﻿using _3D_Game_Recompile.Core_Game_System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile.Entities.Particles
{
    public enum BillBoardType { Spherical, Cylindrical, None }
    /// <Summary>
    /// Billboard Class: The sprites that always face you class
    /// </Summary>
    public class Billboard : Quad
    {
        private BillBoardType type;
        /// <Summary>
        /// public Billboard(float width, float height, BillBoardType type)
        ///     Billboard constructor
        /// </Summary>
        public Billboard(float width, float height, BillBoardType type)
            : base(Vector3.Zero, Vector3.Zero, Vector3.Zero, width, height)
        {
            this.type = type;
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera, string texturePath)
        ///     Load assets for billboard
        /// </Summary>
        public void Load(ThirdPersonCamera camera, string texturePath)
        {
            base.BaseLoad(camera);
            Texture2D texture = Global.Content.Load<Texture2D>(texturePath);
            quadEffect.Texture = texture;
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Updates the billboard per frame
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            FaceCamera(camera.Position);
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the billboard per frame
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            base.BaseDraw(viewMatrix);
        }

        /// <Summary>
        /// protected void FaceCamera(Vector3 cameraPos)
        ///     Faces the billboard towards the camera given the position of the camera
        /// </Summary>
        protected void FaceCamera(Vector3 cameraPos)
        {
            Vector3 tempNewPos;
            Vector3 newPos;
            switch (type)
            {
                case BillBoardType.Spherical:
                    tempNewPos = camera.Position - Origin;
                    newPos = Vector3.Normalize(tempNewPos);

                    float angleFromY = Vector3.Dot(newPos, Vector3.Up);
                    float vectorLengths = tempNewPos.Length() * Vector3.Up.Length();
                    double angleToUp = Math.Acos(tempNewPos.Y / vectorLengths);
                    double angle = (Math.PI - angleToUp);
                    //worldMatrix = Matrix.CreateBillboard(Origin, camera.Position, Vector3.Up, Vector3.Up);

                    Vector3 upDirection = Vector3.Transform(newPos, Matrix.CreateRotationY((float)angle)); //Sort it out
                    //Debug.WriteLine(angleFromY + " " + Vector3.Up + " " + tempNewPos + " " + angle);
                    normal = newPos;
                    Up = upDirection;
                    break;
                case BillBoardType.Cylindrical:
                    up = Vector3.Up;
                    tempNewPos = camera.Position - Origin;
                    newPos = Vector3.Normalize(new Vector3(tempNewPos.X, 0, tempNewPos.Z));
                    Normal = newPos;
                    break;
                case BillBoardType.None:
                    
                    break;
            }
        }

        #region Properties

        #endregion
    }
}
