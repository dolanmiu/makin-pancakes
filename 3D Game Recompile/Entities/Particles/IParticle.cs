﻿using Microsoft.Xna.Framework;

namespace _3D_Game_Recompile.Entities.Particles
{
    /// <Summary>
    /// Particle Interface: It is what all particles use as an interface
    /// </Summary>
    public interface IParticle
    {
        /// <Summary>
        /// void Update(GameTime gameTime);
        ///     Updates the particle per frame
        /// </Summary>
        void Update(GameTime gameTime);

        double Interval { get; set; }
        float Scale { get; set; }
        float Alpha { get; set; }
        bool Ticking { get; }
        Vector3 Position { get; }

        /// <Summary>
        /// void Update(GameTime gameTime);
        ///     Draw the particle per frame
        /// </Summary>
        void Draw(Matrix viewMatrix);
    }
}
