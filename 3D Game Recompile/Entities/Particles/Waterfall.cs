﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _3D_Game_Recompile.Entities.Particles
{
    /// <Summary>
    /// Water Fall Class: Water falls are cool and fancy, they are also made from particles.
    /// </Summary> 
    class Waterfall : ParticleMachine
    {
        private Vector2 sideGitter;
        private float gravity;
        private TimerComponent timer;
        private Vector3 lineStart;
        private Vector3 lineEnd;
        private Vector3 waterfallLength;
        private Vector3 deltaWaterfall;

        /// <Summary>
        /// public Waterfall(Vector3 lineStart, Vector3 lineEnd, Vector2 sideGitter, float gravity)
        ///     Water fall constructor
        /// </Summary>
        public Waterfall(Vector3 lineStart, Vector3 lineEnd, Vector2 sideGitter, float gravity)
        {
            this.sideGitter = sideGitter;
            this.gravity = gravity;
            this.lineStart = lineStart;
            this.lineEnd = lineEnd;
            this.waterfallLength = lineStart - lineEnd;
            this.deltaWaterfall = Vector3.Normalize(waterfallLength);
            timer = new TimerComponent(0.1);
        }

        /// <Summary>
        /// new public void Load(ThirdPersonCamera camera)
        ///     Loads the particle system
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            this.camera = camera;
            texture2D = Global.Content.Load<Texture2D>("Particles/Water Particle");
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Updates the particle every frame
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            base.BaseUpdate(gameTime);
            timer.Update(gameTime);
            if (timer.Ticking)
            {
                CreateParticleInLine(1, new Vector2(1, 1), Vector2.Zero, new Vector2(gravity / 10f, gravity / 10f), sideGitter / 10f, BillBoardType.Spherical);
            }
            CreateParticleInLine(2, new Vector2(0.25f, 0.25f), Vector2.Zero, new Vector2(gravity, gravity), sideGitter, BillBoardType.Spherical);
        }

        /// <Summary>
        /// protected void CreateParticleInLine(int count, Vector2 size, Vector2 xGitter, Vector2 yGitter, Vector2 zGitter, BillBoardType type)
        ///     Creates a particle in a line
        /// </Summary>
        private void CreateParticleInLine(int count, Vector2 size, Vector2 xGitter, Vector2 yGitter, Vector2 zGitter, BillBoardType type)
        {
            for (int i = 0; i < count; i++)
            {
                Particle2D particle = new Particle2D(size.X, size.Y, type, Global.GetRandomNumber(xGitter.X, xGitter.Y), Global.GetRandomNumber(yGitter.X, yGitter.Y), Global.GetRandomNumber(zGitter.X, zGitter.Y), 1);
                particle.Load(camera, "Particles/Explosions/Explosion1");

                Vector3 lineEq = lineStart + deltaWaterfall * Global.GetRandomNumber(0, waterfallLength.Length());
                particle.Position = lineEq;
                particles.Add(particle);
            }
        }
    }
}
