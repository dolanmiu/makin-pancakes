﻿using _3D_Game_Recompile.Entities.Particles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace _3D_Game_Recompile.Entities
{
    /// <Summary>
    /// Particle Machine Class: Basis of a particle system. All particle systems inherit from this.
    /// </Summary> 
    public abstract class ParticleMachine
    {
        protected List<IParticle> particles;
        protected ThirdPersonCamera camera;
        protected double interval;
        protected Model model;
        protected Texture2D texture2D;

        /// <Summary>
        /// public ParticleMachine():
        ///     Particle Machine constructor.
        /// </Summary>
        public ParticleMachine()
        {
            particles = new List<IParticle>();
            interval = 1;
        }

        /// <Summary>
        /// protected void AddPointSourceDynamicParticles(int count, string modelName, Vector3 spawnPos, Vector2 sideForces, Vector2 upForce, double startRand, double endRand) 
        ///     Adds 3D particles to the particle system given a point souce and forces.
        /// </Summary>
        protected void Add3DPointSourceDynamicParticles(int count, Model modelName, Vector3 spawnPos, Vector3 initialForce, Vector2 sideForces, Vector2 upForce, double startRand, double endRand) 
        {
            for (int i = 0; i < count; i++)
            {
                Particle3D particle = new Particle3D(Global.GetRandomNumber(sideForces.X, sideForces.Y), Global.GetRandomNumber(upForce.X, upForce.Y), Global.GetRandomNumber(sideForces.X, sideForces.Y), Global.GetRandomNumber(startRand, endRand));
                particle.InitialForce = initialForce;
                //particle.Load(camera, modelName);
                particle.Model = modelName;
                particle.Camera = camera;
                particle.Position = spawnPos;
                particle.Interval = interval;
                particles.Add(particle);
            }
        }

        /// <Summary>
        /// protected void Add2DPointSourceDynamicParticlesCylinder(int count, string textureName, Vector2 size, BillBoardType type, Vector3 spawnPos, Vector2 sideForces, Vector2 upForce, double startRand, double endRand, float deceleration, Vector3 initialForce)
        ///     Adds 2D particles to the particle system given a point souce and equal side forces (x and z forces).
        /// </Summary>
        protected void Add2DPointSourceDynamicParticlesCylinder(int count, Vector2 size, BillBoardType type, Vector3 spawnPos, Vector2 sideForces, Vector2 upForce, double startRand, double endRand, float deceleration, Vector3 initialForce)
        {
            Add2DPointSourceDynamicParticles(count, size, type, spawnPos, sideForces, upForce, sideForces, startRand, endRand, deceleration, initialForce);
        }

        /// <Summary>
        /// protected void Add2DPointSourceDynamicParticlesCylinder(int count, string textureName, Vector2 size, BillBoardType type, Vector3 spawnPos, Vector2 sideForces, Vector2 upForce, double startRand, double endRand, float deceleration, Vector3 initialForce)
        ///     Adds 2D particles to the particle system given a point souce and equal side forces (x and z forces).
        /// </Summary>
        protected void Add2DPointSourceDynamicParticles(int count, Vector2 size, BillBoardType type, Vector3 spawnPos, Vector2 xForce, Vector2 yForce, Vector2 zForce, double startRand, double endRand, float deceleration, Vector3 initialForce)
        {
            for (int i = 0; i < count; i++)
            {
                Particle2D particle = new Particle2D(size.X, size.Y, type, Global.GetRandomNumber(xForce.X, xForce.Y), Global.GetRandomNumber(yForce.X, yForce.Y), Global.GetRandomNumber(zForce.X, zForce.Y), Global.GetRandomNumber(startRand, endRand));
                particle.Load(camera);
                particle.Texture = texture2D;
                particle.Position = spawnPos;
                particle.InitialForce = initialForce;
                particles.Add(particle);
                particle.Interval = interval;
                particle.Deceleraton = deceleration;
            }
        }

        /// <Summary>
        /// protected void BaseUpdate(GameTime gameTime)
        ///     This is called on each Update() function of a particle machine.
        /// </Summary>
        protected void BaseUpdate(GameTime gameTime)
        {
            for (int i = 0; i < particles.Count; i++)
            {
                if (particles[i] != null)
                {
                    particles[i].Update(gameTime);
                    if (particles[i].Ticking)
                    {
                        particles[i] = null;
                    }
                }
            }
            particles = Global.RefactorList<IParticle>(particles);
        }

        /// <Summary>
        /// protected void ReduceAlpha(float amount)
        ///     Reduces the Alpha per frame.
        /// </Summary>
        protected void ReduceAlpha(float amount)
        {
            for (int i = 0; i < particles.Count; i++)
            {
                if (particles[i].Alpha >= 0)
                {
                    particles[i].Alpha -= amount;
                }
            }
        }

        /// <Summary>
        /// protected void BaseDraw(Matrix viewMatrix)
        ///     This is called on each Draw() function of a particle machine.
        /// </Summary>
        protected void BaseDraw(Matrix viewMatrix)
        {

            for (int i = 0; i < particles.Count; i++)
            {
                if (particles[i] != null) particles[i].Draw(viewMatrix);
            }
        }
    }
}
