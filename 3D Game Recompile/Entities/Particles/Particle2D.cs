﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _3D_Game_Recompile.Entities.Particles
{
    /// <Summary>
    /// 2D Particle Class: Leaves a trail of particles for a given moving object.
    /// </Summary> 
    class Particle2D : Billboard, IParticle
    {
        private TimerComponent timer;
        private Vector3 initialForce;
        private Vector3 force;
        private float deceleraton = 1;
        private double interval;
        private float originalWidth;

        /// <Summary>
        /// public Particle2D(float width, float height, BillBoardType type, float xForce, float yForce, float zForce, float scale) : base(width, height, type)
        ///     2D Particle constructor.
        /// </Summary>
        public Particle2D(float width, float height, BillBoardType type, float xForce, float yForce, float zForce, float scale) : base(width, height, type)
        {
            this.originalWidth = width;
            this.Width = this.Width * scale;
            this.Height = this.Height * scale;
            force = new Vector3(xForce, yForce, zForce);
            timer = new TimerComponent(1);
            initialForce = Vector3.Zero;
        }

        public void Load(ThirdPersonCamera camera)
        {
            base.BaseLoad(camera);
        }

        /// <Summary>
        /// new public void Update(GameTime gameTime)
        ///     Updates the particle every frame.
        /// </Summary>
        new public void Update(GameTime gameTime)
        {
            FaceCamera(camera.Position);
            force *= deceleraton;
            Position += force + initialForce;
            timer.Update(gameTime);
        }

        #region Properties
        public double Interval
        {
            get { return interval; }
            set 
            { 
                interval = value;
                timer.Interval = value;
            }
        }

        public bool Ticking
        {
            get { return timer.Ticking; }
        }
        public float Deceleraton
        {
            get { return deceleraton; }
            set { deceleraton = value; }
        }

        public Vector3 InitialForce
        {
            set { initialForce = value; }
        }

        public float Scale
        {
            get { return Width / originalWidth; }
            set { Width = value * originalWidth; }
        }

        public float Alpha
        {
            get { return quadEffect.Alpha; }
            set { quadEffect.Alpha = value; }
        }

        public ThirdPersonCamera Camera
        {
            set { camera = value; }
        }
        #endregion
    }
}
