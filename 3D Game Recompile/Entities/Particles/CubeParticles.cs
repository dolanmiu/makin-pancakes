﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace _3D_Game_Recompile.Entities.Particles
{
    /// <Summary>
    /// CubeParticles Class: The 3D particle effect where a cube turns into 8 peices when blown up.
    /// </Summary>
    class CubeParticles : ParticleMachine
    {
        SoundEffect cubeBreak1, cubeBreak2, cubeBreak3, cubeBreak4;
        private MovementTrail trail;

        /// <Summary>
        /// public CubeParticles()
        ///     Cube particle constructor
        /// </Summary>
        public CubeParticles()
        {
            trail = new MovementTrail();
            interval = 1;
        }

        /// <Summary>
        /// new public void Load(ThirdPersonCamera camera)
        ///     Loads the particle system
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            this.camera = camera;
            trail.Load(camera);
            cubeBreak1 = Global.Content.Load<SoundEffect>("Sounds/cubeBreak1");
            cubeBreak2 = Global.Content.Load<SoundEffect>("Sounds/cubeBreak2");
            cubeBreak3 = Global.Content.Load<SoundEffect>("Sounds/cubeBreak3");
            cubeBreak4 = Global.Content.Load<SoundEffect>("Sounds/cubeBreak4");
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Updates the particle system per frame
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            base.BaseUpdate(gameTime);
            trail.Update(gameTime);
            for (int i = 0; i < particles.Count; i++)
            {
                if (particles[i] != null) trail.CreateTrail(10, particles[i].Position, Vector3.Zero);
            }
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the particle system per frame
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            trail.Draw(viewMatrix);
            Global.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            base.BaseDraw(viewMatrix);
        }

        /// <Summary>
        ///  public void CreateCubes(string modelName, Vector3 spawnPos, float disp)
        ///     Creates the 8 cube particles
        /// </Summary>
        public void CreateCubes(Model model, Vector3 spawnPos, Vector3 initialForce, float disp)
        {
            CreateCubes(model, model, spawnPos, initialForce, disp, new Vector2(-0.001f, 0.001f), new Vector2(0.005f, 0.01f));
        }

        public void CreateCubes(Model model1, Model model2, Vector3 spawnPos, Vector3 initialForce, float disp, Vector2 sideForces, Vector2 upForce)
        {
            Add3DPointSourceDynamicParticles(1, model1, spawnPos + new Vector3(disp, 0, -disp), initialForce, sideForces, upForce, 0.5, 0.5);
            Add3DPointSourceDynamicParticles(1, model1, spawnPos + new Vector3(-disp, 0, disp), initialForce, sideForces, upForce, 0.5, 0.5);
            Add3DPointSourceDynamicParticles(1, model1, spawnPos + new Vector3(-disp, 0, -disp), initialForce, sideForces, upForce, 0.5, 0.5);
            Add3DPointSourceDynamicParticles(1, model1, spawnPos + new Vector3(disp, 0, disp), initialForce, sideForces, upForce, 0.5, 0.5);

            Add3DPointSourceDynamicParticles(1, model2, spawnPos + new Vector3(disp, disp * 2, -disp), initialForce, sideForces, upForce, 0.5, 0.5);
            Add3DPointSourceDynamicParticles(1, model2, spawnPos + new Vector3(-disp, disp * 2, disp), initialForce, sideForces, upForce, 0.5, 0.5);
            Add3DPointSourceDynamicParticles(1, model2, spawnPos + new Vector3(-disp, disp * 2, -disp), initialForce, sideForces, upForce, 0.5, 0.5);
            Add3DPointSourceDynamicParticles(1, model2, spawnPos + new Vector3(disp, disp * 2, disp), initialForce, sideForces, upForce, 0.5, 0.5);
            switch (Global.GetRandomInt(0, 4))
            {
                case 0:
                    SoundPlayer.Play(cubeBreak1);
                    break;
                case 1:
                    SoundPlayer.Play(cubeBreak2);
                    break;
                case 2:
                    SoundPlayer.Play(cubeBreak3);
                    break;
                case 3:
                    SoundPlayer.Play(cubeBreak4);
                    break;
            }
        }

        #region Properties
        public List<IParticle> Particles
        {
            get { return particles; }
            set { particles = value; }
        }
        #endregion
    }
}
