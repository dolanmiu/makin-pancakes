﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using _3D_Game_Recompile.Entities.Particles;

namespace _3D_Game_Recompile.Entities
{
    /// <Summary>
    /// 3D Particle Class: Leaves a trail of particles for a given moving object.
    /// </Summary> 
    public class Particle3D : BaseEntity, IParticle
    {
        private TimerComponent timer;
        private Vector3 initialForce;
        private Vector3 force;
        private double interval;

        /// <Summary>
        /// public Particle3D(float xForce, float yForce, float zForce, float scale)
        ///     3D Particle constructor.
        /// </Summary>
        public Particle3D(float xForce, float yForce, float zForce, float scale)
        {
            force = new Vector3(xForce, yForce, zForce);
            timer = new TimerComponent(2);
            scaleMatrix = Matrix.CreateScale(scale);
            initialForce = new Vector3(0, 0, 0);
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera, string modelName)
        ///     loads assets for the particle.
        /// </Summary>
        public void Load(ThirdPersonCamera camera, string modelName)
        {
            base.Load(modelName, camera);
        }

        /// <Summary>
        /// new public void Update(GameTime gameTime)
        ///     Updates the particle every frame.
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            base.BaseUpdate();
            //Vector3 velocity = force - new Vector3(0, 0.3f, 0);
            initialForce = initialForce - force;
            Position += initialForce;
            timer.Update(gameTime);
        }

        #region Properties
        public double Interval
        {
            get { return interval; }
            set
            {
                interval = value;
                timer.Interval = value;
            }
        }

        public bool Ticking
        {
            get { return timer.Ticking; }
        }

        public Vector3 InitialForce
        {
            set { initialForce = value; }
        }

        public float Scale
        {
            get { return 0; }
            set { scaleMatrix = Matrix.CreateScale(value); }
        }

        public float Alpha
        {
            get { return 1;  }
            set { Alpha = value; }
        }

        public ThirdPersonCamera Camera
        {
            set { camera = value; }
        }
        #endregion
    }
}
