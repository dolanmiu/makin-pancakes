﻿using Microsoft.Xna.Framework;

namespace _3D_Game_Recompile.Entities.Surfaces
{
    /// <Summary>
    /// Wall Class: The wall you see on the side of the island of the stage.
    /// </Summary> 
    public class Wall : Plane
    {
        /// <Summary>
        /// public Wall(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, Alignment alignment, float width, float height) : base(camera, modelPosition, modelRotation, width, height, width/25.5f, height/25.5f, alignment)
        ///     Wall constructor.
        /// </Summary>
        public Wall(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, Alignment alignment, float width, float height) : base(camera, modelPosition, modelRotation, width, height, width/25.5f, height/25.5f, alignment)
        {

        }

        /// <Summary>
        /// public void Load()
        ///     Load the assets for the wall.
        /// </Summary>
        public void Load()
        {
            base.BaseLoad("Planes/Wall");
        }

        /// <Summary>
        /// public void Extend(int amount)
        ///     Extends the wall given an amount.
        /// </Summary>
        public void Extend(int amount)
        {
            FLOOR_WIDTH += amount;
            FLOOR_TILE_U = FLOOR_WIDTH / 25.5f;
            FLOOR_TILE_V = FLOOR_HEIGHT / 25.5f;
            CreatePlane();
        }
    }
}
