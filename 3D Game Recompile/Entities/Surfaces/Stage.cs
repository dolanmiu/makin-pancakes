﻿using _3D_Game_Recompile.Entities.Particles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace _3D_Game_Recompile.Entities.Surfaces
{
    /// <Summary>
    /// Stage Class: What the stage looks like. Floors, fences, walls, water, you name it, its there.
    /// </Summary> 
    public class Stage
    {
        //private Wall leftWall;
        private Wall rightWall;
        private Wall frontWall;
        private Floor floor;
        private Water water;
        private int width;
        private int height;
        private FenceSystem fenceSystem;
        private CubeParticles cliffParticles;
        private Model cliffTopModel;
        private Model cliffBotModel;
        private SoundEffect cliffBreakSound, cliffAddSound;
        private List<Grass> grassModel;

        /// <Summary>
        /// public Stage(ThirdPersonCamera camera, int width, int height)
        ///     Stage constructor
        /// </Summary>
        public Stage(ThirdPersonCamera camera, int width, int height)
        {
            this.width = width;
            this.height = height;
            Matrix combinedRotation = Matrix.CreateRotationZ((float)Math.PI / 2) * Matrix.CreateRotationX((float)Math.PI / 2);
            floor = new Floor(camera, new Vector3(0, 0, -4f), Matrix.CreateRotationX(0), width, height); //needs cleaning, move camera to load? new Vector3(0, 0, -41.5f)
            frontWall = new Wall(camera, new Vector3(0, height - 3.5f, 0.5f), Matrix.CreateRotationX((float)Math.PI / 2), Plane.Alignment.Top, floor.Width, 10); //needs cleaning, move camera to load? 51.5
            rightWall = new Wall(camera, new Vector3(-floor.Height + 9, floor.Width / 2 + 0.5f, 0.5f), combinedRotation, Plane.Alignment.TopRight, floor.Height, 10);
            water = new Water(camera, new Vector3(0, -3, 10), Matrix.CreateRotationX(0), 102, 102);
            fenceSystem = new FenceSystem(camera, width - 1);
            cliffParticles = new CubeParticles();

            //testPlane = new Plane2(camera, new Vector3(0, 1, 0), Matrix.CreateRotationX(0), 3, 3, 1, 1, Plane2.Alignment.TopRight);
        }

        /// <Summary>
        /// public void Load()
        ///     Loads all the Stage assets.
        /// </Summary>
        public void Load(ThirdPersonCamera camera)
        {
            frontWall.Load();
            rightWall.Load();
            floor.Load();
            water.Load();
            cliffParticles.Load(camera);
            fenceSystem.AddFence(3);
            cliffTopModel = Global.Content.Load<Model>("cliffTop");
            cliffBotModel = Global.Content.Load<Model>("cliffTop");
            cliffBreakSound = Global.Content.Load<SoundEffect>("Sounds/CliffDestroy");
            cliffAddSound = Global.Content.Load<SoundEffect>("Sounds/CliffAdd");
            grassModel = new List<Grass>();
            for (int i = 0; i < 30; i ++)
            {
                Grass grass = new Grass(new Vector3(Global.GetRandomInt(-width / 2 + 1, width / 2 - 1), -0.5f, Global.GetRandomInt(-3, -1)));
                grassModel.Add(grass);
                grass.Load(camera);
            }

            //testPlane.Load();
        }

        /// <Summary>
        /// public void Update()
        ///     Updates all the Stage assets per frame.
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            int fences = height / fenceSystem.FenceWidth;
            if (fences > fenceSystem.Count / 2 + 1)
            {
                fenceSystem.AddFence(1);
            }
            if (fences <= fenceSystem.Count / 2)
            {
                fenceSystem.RemoveFence();
            }
            cliffParticles.Update(gameTime);
            foreach (Grass grass in grassModel)
            {
                grass.Update();
            }
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the stage per frame.
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            floor.Draw(viewMatrix);
            frontWall.Draw(viewMatrix);
            //rightWall.Draw(viewMatrix);
            water.Draw(viewMatrix);
            fenceSystem.Draw(viewMatrix);
            cliffParticles.Draw(viewMatrix);
            foreach (Grass grass in grassModel)
            {
                grass.Draw(viewMatrix);
            }

            //testPlane.Draw(viewMatrix);
        }

        /// <Summary>
        /// public void Extend(int amount)
        ///     Extends the stage for an amount given.
        /// </Summary>
        public void Extend(int amount)
        {
            Vector3 currentPosition = frontWall.Position;
            floor.Extend(amount);
            frontWall.Position = new Vector3(frontWall.Position.X, frontWall.Position.Y + amount, frontWall.Position.Z);
            height += amount;

            //rightWall.Extend(amount);
            //rightWall.Position = new Vector3(-floor.Height + 9, floor.Width / 2 + 0.5f, 0.5f);
            if (amount < 0)
            {
                for (int i = 0; i < width / 2; i++)
                {
                    cliffParticles.CreateCubes(cliffTopModel, cliffBotModel, new Vector3(-width / 2 + (i * 2) + 1, currentPosition.Z - 2.5f, currentPosition.Y - 1.5f), Vector3.Zero, 2f / 4f, new Vector2(-0.0001f, 0.0001f), new Vector2(0.003f, 0.003f));
                }
                SoundPlayer.Play(cliffBreakSound);
            }
            if (amount > 0)
            {
                SoundPlayer.Play(cliffAddSound);
            }
        }

        #region Properties
        public int Height
        {
            get { return height; }
        }

        public int Width
        {
            get { return width; }
        }
        #endregion
    }
}
