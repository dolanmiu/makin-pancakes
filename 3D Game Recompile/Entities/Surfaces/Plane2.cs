﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Plane Class: All flat surfaces inherhit this
    /// </Summary> 
    public class Plane2
    {
        protected float FLOOR_WIDTH = 102.0f;
        protected float FLOOR_HEIGHT = 102.0f;
        protected float FLOOR_TILE_U = 4.0f;
        protected float FLOOR_TILE_V = 4.0f;
        private VertexBuffer vertexBuffer;
        protected BasicEffect effect;
        private Texture2D texture;
        private ThirdPersonCamera camera;
        private float height = -0.51f;
        protected Vector3 modelPosition;
        protected Matrix modelRotation;

        public enum Alignment { Bottom, Centre, Top , TopLeft, TopRight}
        private Alignment alignment;

        /// <Summary>
        /// public Plane(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, float width, float height, float u, float v, Alignment alignment)
        ///     Plane constructor.
        /// </Summary>
        public Plane2(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, float width, float height, float u, float v, Alignment alignment)
        {
            this.FLOOR_WIDTH = width;
            this.FLOOR_HEIGHT = height;
            this.FLOOR_TILE_U = u;
            this.FLOOR_TILE_V = v;

            this.camera = camera;
            this.modelPosition = modelPosition;
            this.modelRotation = modelRotation;
            this.alignment = alignment;
            CreatePlane();
        }

        /// <Summary>
        /// protected void CreatePlane()
        ///     Creates a plane.
        /// </Summary>
        protected void CreatePlane()
        {
            effect = new BasicEffect(Global.GraphicsDevice);

            float w = FLOOR_WIDTH * 0.5f;
            float h = FLOOR_HEIGHT * 0.5f;
            float top = 0, top2 = 0, bottom = 0, bottom2 = 0;
            float left = 0, right = 0, left2 = 0, right2 = 0;

            switch (alignment)
            {
                case Alignment.Centre:
                    top = -h;
                    top2 = -h;
                    bottom = h;
                    bottom2 = h;
                    left = -w;
                    right = w;
                    left2 = -w;
                    right2 = w;
                    break;
                case Alignment.Bottom:
                    top = -2 * h;
                    top2 = -2 * h;
                    bottom = 0;
                    bottom2 = 0;
                    left = -w;
                    right = w;
                    left2 = -w;
                    right2 = w;
                    break;
                case Alignment.Top:
                    top = 0;
                    top2 = 0;
                    bottom = 2 * h;
                    bottom2 = 2 * h;
                    left = -w;
                    right = w;
                    left2 = -w;
                    right2 = w;
                    break;
                case Alignment.TopRight:
                    top = 0;
                    top2 = 0;
                    bottom = 2 * h;
                    bottom2 = 2 * h;
                    left = 0;
                    right = 2*w;
                    left2 = 0;
                    right2 = 2*w;
                    break;
                case Alignment.TopLeft:
                    top = 0;
                    top2 = 0;
                    bottom = 2 * h;
                    bottom2 = 2 * h;
                    left = 0;
                    right = -2 * w;
                    left2 = 0;
                    right2 = -2 * w;
                    break;
            }
            /*Vector3[] positions =
            {
                new Vector3(left, height, top),
                new Vector3(left + right/2, height, top),
                new Vector3(right, height, top), //right, height, top2
                new Vector3(left, height, top + bottom / 2),
                new Vector3(left + right / 2, height,  top + bottom / 2), //mid
                new Vector3(right, height, top + bottom / 2), //right, height, top2
                new Vector3(left, height,  bottom), //left2, height,  bottom
                new Vector3(left + right / 2, height,  bottom), //left2, height,  bottom
                new Vector3(right, height,  bottom), //right2, height,  bottom2
            };*/

            Vector3[] positions = new Vector3[8];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    positions[(4 * i) + j] = new Vector3(left * (i + 1), height, top * (i + 1));
                    positions[(4 * i) + j] = new Vector3(right * (i + 1), height, top * (i + 1)); //right, height, top2
                    positions[(4 * i) + j] = new Vector3(left * (i + 1), height, bottom * (i + 1)); //left2, height,  bottom
                    positions[(4 * i) + j] = new Vector3(right * (i + 1), height, bottom * (i + 1)); //right2, height,  bottom2
                }
            }

            Vector2[] texCoords =
            {
                new Vector2(0.0f, 0.0f),
                new Vector2(FLOOR_TILE_U + FLOOR_TILE_U / 2, 0.0f),
                new Vector2(FLOOR_TILE_U, 0.0f),
                new Vector2(0.0f, 0.0f + FLOOR_TILE_V / 2),
                new Vector2(0.0f + FLOOR_TILE_U / 2, 0.0f + FLOOR_TILE_V / 2), //mid
                new Vector2(FLOOR_TILE_U, 0.0f + FLOOR_TILE_V / 2),
                new Vector2(0.0f, FLOOR_TILE_V),
                new Vector2(0.0f + FLOOR_TILE_V / 2, FLOOR_TILE_V),
                new Vector2(FLOOR_TILE_U, FLOOR_TILE_V)
            };

            /*VertexPositionNormalTexture[] vertices =
            {
                new VertexPositionNormalTexture(positions[0], Vector3.Up, texCoords[0]), // 1,1
                //new VertexPositionNormalTexture(positions[1], Vector3.Up, texCoords[1]), //2,1
                new VertexPositionNormalTexture(positions[2], Vector3.Up, texCoords[2]), //3,1
                //new VertexPositionNormalTexture(positions[3], Vector3.Up, texCoords[3]), //1,2
                //new VertexPositionNormalTexture(positions[4], Vector3.Up, texCoords[4]), //2,2 mid
                //new VertexPositionNormalTexture(positions[5], Vector3.Up, texCoords[5]), //3,2
                new VertexPositionNormalTexture(positions[6], Vector3.Up, texCoords[6]), //1,3
                //new VertexPositionNormalTexture(positions[7], Vector3.Up, texCoords[7]), //2,3
                //new VertexPositionNormalTexture(positions[8], Vector3.Up, texCoords[8]), //3,3
            };*/

            VertexPositionNormalTexture[] vertices =
            {
                new VertexPositionNormalTexture(positions[0], Vector3.Up, texCoords[0]), // 1,1
                new VertexPositionNormalTexture(positions[1], Vector3.Up, texCoords[1]), //2,1
                new VertexPositionNormalTexture(positions[2], Vector3.Up, texCoords[2]), //3,1
                //new VertexPositionNormalTexture(positions[3], Vector3.Up, texCoords[8]), //1,2
                //new VertexPositionNormalTexture(positions[4], Vector3.Up, texCoords[4]), //2,2 mid
                //new VertexPositionNormalTexture(positions[5], Vector3.Up, texCoords[5]), //3,2
                //new VertexPositionNormalTexture(positions[6], Vector3.Up, texCoords[6]), //1,3
                //new VertexPositionNormalTexture(positions[7], Vector3.Up, texCoords[7]), //2,3
                //new VertexPositionNormalTexture(positions[8], Vector3.Up, texCoords[8]), //3,3
            };

            vertexBuffer = new VertexBuffer(Global.GraphicsDevice, typeof(VertexPositionNormalTexture), vertices.Length, BufferUsage.WriteOnly);
            vertexBuffer.SetData(vertices);
        }

        public void Load()
        {
            BaseLoad("Planes/Water");
        }
        /// <Summary>
        /// protected void BaseLoad(string textureName)
        ///     This gets called at every Load() function of an inherited object.
        /// </Summary>
        protected void BaseLoad(string textureName)
        {
            texture = Global.Content.Load<Texture2D>(textureName);
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     This Draws the object per frame.
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            Global.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            //effect.DiffuseColor = new Vector3(120, 186, 20);
            Global.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            effect.World = Matrix.Identity * Matrix.CreateTranslation(modelPosition) * modelRotation;
            //effect.View = viewMatrix;
            //effect.View = Matrix.CreateLookAt(new Vector3(0, 0, 10), new Vector3(0, 0, 0), Vector3.UnitY);
            effect.View = Matrix.Identity;
            effect.TextureEnabled = true;
            effect.Texture = texture;
            effect.Projection = camera.ViewProjectionMatrix;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                //Global.GraphicsDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
                //Global.GraphicsDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;
                Global.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleStrip, 0, 2);
            }
        }

        #region Properties
        public Texture2D Texture
        {
            set { texture = value; }
        }

        public float Height
        {
            get { return FLOOR_HEIGHT; }
            set { FLOOR_HEIGHT = value; }
        }

        public float Width
        {
            get { return FLOOR_WIDTH; }
            set { FLOOR_WIDTH = value; }
        }

        public Vector3 Position
        {
            get { return modelPosition; }
            set { modelPosition = value; }
        }
        #endregion
    }
}
