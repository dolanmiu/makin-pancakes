﻿using Microsoft.Xna.Framework;

namespace _3D_Game_Recompile.Entities.Surfaces
{
    /// <Summary>
    /// Floor Class: the grassy floor of the game
    /// </Summary> 
    public class Floor : Plane
    {
        /// <Summary>
        /// public Floor(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, int width, int height)
        ///     Floor constructor.
        /// </Summary>
        public Floor(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, int width, int height)
            : base(camera, modelPosition, modelRotation, width, height, width / 25.5f, height / 25.5f, Alignment.Top)
        {
        }

        /// <Summary>
        /// public void Load()
        ///     Loads assets for the floor.
        /// </Summary>
        public void Load()
        {
            base.BaseLoad("Planes/GrassFloor");
        }

        /// <Summary>
        /// public void Extend(int amount)
        ///     Makes the floor a little bigger given the amount of increase.
        /// </Summary>
        public void Extend(int amount)
        {
            FLOOR_HEIGHT += amount;
            FLOOR_TILE_U = FLOOR_WIDTH / 25.5f;
            FLOOR_TILE_V = FLOOR_HEIGHT / 25.5f;
            CreatePlane();
        }
    }
}
