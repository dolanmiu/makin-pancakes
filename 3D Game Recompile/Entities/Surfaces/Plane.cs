﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Plane Class: All flat surfaces inherhit this
    /// </Summary> 
    abstract public class Plane
    {
        protected float FLOOR_WIDTH = 102.0f;
        protected float FLOOR_HEIGHT = 102.0f;
        protected float FLOOR_TILE_U = 4.0f;
        protected float FLOOR_TILE_V = 4.0f;
        private VertexBuffer vertexBuffer;
        protected BasicEffect effect;
        private Texture2D texture;
        private ThirdPersonCamera camera;
        private float height = -0.51f;
        protected Vector3 modelPosition;
        protected Matrix modelRotation;

        public enum Alignment { Bottom, Centre, Top , TopLeft, TopRight}
        private Alignment alignment;

        /// <Summary>
        /// public Plane(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, float width, float height, float u, float v, Alignment alignment)
        ///     Plane constructor.
        /// </Summary>
        public Plane(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, float width, float height, float u, float v, Alignment alignment)
        {
            this.FLOOR_WIDTH = width;
            this.FLOOR_HEIGHT = height;
            this.FLOOR_TILE_U = u;
            this.FLOOR_TILE_V = v;

            this.camera = camera;
            this.modelPosition = modelPosition;
            this.modelRotation = modelRotation;
            this.alignment = alignment;
            CreatePlane();
        }

        /// <Summary>
        /// protected void CreatePlane()
        ///     Creates a plane.
        /// </Summary>
        protected void CreatePlane()
        {
            effect = new BasicEffect(Global.GraphicsDevice);

            float w = FLOOR_WIDTH * 0.5f;
            float h = FLOOR_HEIGHT * 0.5f;
            float top = 0, top2 = 0, bottom = 0, bottom2 = 0;
            float left = 0, right = 0, left2 = 0, right2 = 0;

            switch (alignment)
            {
                case Alignment.Centre:
                    top = -h;
                    top2 = -h;
                    bottom = h;
                    bottom2 = h;
                    left = -w;
                    right = w;
                    left2 = -w;
                    right2 = w;
                    break;
                case Alignment.Bottom:
                    top = -2 * h;
                    top2 = -2 * h;
                    bottom = 0;
                    bottom2 = 0;
                    left = -w;
                    right = w;
                    left2 = -w;
                    right2 = w;
                    break;
                case Alignment.Top:
                    top = 0;
                    top2 = 0;
                    bottom = 2 * h;
                    bottom2 = 2 * h;
                    left = -w;
                    right = w;
                    left2 = -w;
                    right2 = w;
                    break;
                case Alignment.TopRight:
                    top = 0;
                    top2 = 0;
                    bottom = 2 * h;
                    bottom2 = 2 * h;
                    left = 0;
                    right = 2*w;
                    left2 = 0;
                    right2 = 2*w;
                    break;
                case Alignment.TopLeft:
                    top = 0;
                    top2 = 0;
                    bottom = 2 * h;
                    bottom2 = 2 * h;
                    left = 0;
                    right = -2 * w;
                    left2 = 0;
                    right2 = -2 * w;
                    break;
            }
            Vector3[] positions =
            {
                new Vector3(left, height, top),
                new Vector3(right, height, top), //right, height, top2
                new Vector3(left, height,  bottom), //left2, height,  bottom
                new Vector3(right, height,  bottom) //right2, height,  bottom2
            };

            Vector2[] texCoords =
            {
                new Vector2(0.0f, 0.0f),
                new Vector2(FLOOR_TILE_U, 0.0f),
                new Vector2(0.0f, FLOOR_TILE_V),
                new Vector2(FLOOR_TILE_U, FLOOR_TILE_V)
            };

            VertexPositionNormalTexture[] vertices =
            {
                new VertexPositionNormalTexture(positions[0], Vector3.Up, texCoords[0]),
                new VertexPositionNormalTexture(positions[1], Vector3.Up, texCoords[1]),
                new VertexPositionNormalTexture(positions[2], Vector3.Up, texCoords[2]),
                new VertexPositionNormalTexture(positions[3], Vector3.Up, texCoords[3]),
            };

            /*VertexPositionNormalTexture[] vertices = new VertexPositionNormalTexture[(int)Math.Abs((left - right) * (top - bottom))/2];
            Vector2[] texCoords = new Vector2[(int)Math.Abs((left - right) * (top - bottom))];
            Vector3[] positions = new Vector3[(int)Math.Abs((left - right) * (top - bottom))];
            int count = 0;
            for (float i = left; i < right; i++)
            {
                for (float j = top; j < bottom; j++)
                {
                    positions[count] = new Vector3(i, height, j);
                    count++;
                }
            }
            /*Vector3[] positions =
            {
                new Vector3(wLeft, height, topLeft),
                new Vector3(wRight, height, topRight),
                new Vector3(wTop, height,  bottomLeft),
                new Vector3(wBottom, height,  bottomRight)
            };

            Vector2[] texCoords =
            {
                new Vector2(0.0f, 0.0f),
                new Vector2(FLOOR_TILE_U, 0.0f),
                new Vector2(0.0f, FLOOR_TILE_V),
                new Vector2(FLOOR_TILE_U, FLOOR_TILE_V)
            };

            VertexPositionNormalTexture[] vertices =
            {
                new VertexPositionNormalTexture(positions[0], Vector3.Up, texCoords[0]),
                new VertexPositionNormalTexture(positions[1], Vector3.Up, texCoords[1]),
                new VertexPositionNormalTexture(positions[2], Vector3.Up, texCoords[2]),
                new VertexPositionNormalTexture(positions[3], Vector3.Up, texCoords[3]),
            };*/

            vertexBuffer = new VertexBuffer(Global.GraphicsDevice, typeof(VertexPositionNormalTexture), vertices.Length, BufferUsage.WriteOnly);
            vertexBuffer.SetData(vertices);
        }

        /// <Summary>
        /// protected void BaseLoad(string textureName)
        ///     This gets called at every Load() function of an inherited object.
        /// </Summary>
        protected void BaseLoad(string textureName)
        {
            texture = Global.Content.Load<Texture2D>(textureName);
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     This Draws the object per frame.
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            Global.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            //effect.DiffuseColor = new Vector3(120, 186, 20);
            Global.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            effect.World = Matrix.Identity * Matrix.CreateTranslation(modelPosition) * modelRotation;
            //effect.View = viewMatrix;
            //effect.View = Matrix.CreateLookAt(new Vector3(0, 0, 10), new Vector3(0, 0, 0), Vector3.UnitY);
            effect.View = Matrix.Identity;
            effect.TextureEnabled = true;
            effect.Texture = texture;
            effect.Projection = camera.ViewProjectionMatrix;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                //Global.GraphicsDevice.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
                //Global.GraphicsDevice.SamplerStates[0].AddressV = TextureAddressMode.Wrap;
                Global.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleStrip, 0, 2);
            }
        }

        #region Properties
        public Texture2D Texture
        {
            set { texture = value; }
        }

        public float Height
        {
            get { return FLOOR_HEIGHT; }
            set { FLOOR_HEIGHT = value; }
        }

        public float Width
        {
            get { return FLOOR_WIDTH; }
            set { FLOOR_WIDTH = value; }
        }

        public Vector3 Position
        {
            get { return modelPosition; }
            set { modelPosition = value; }
        }
        #endregion
    }
}
