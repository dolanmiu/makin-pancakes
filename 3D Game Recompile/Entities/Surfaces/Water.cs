﻿
using Microsoft.Xna.Framework;
namespace _3D_Game_Recompile.Entities.Surfaces
{
    /// <Summary>
    /// Water Class: Water is blue and a liquid, and on the Stage
    /// </Summary> 
    public class Water : Plane
    {
        /// <Summary>
        /// public Water(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, float width, float height) : base(camera, modelPosition, modelRotation, width, height, width / 5f, height / 5f, Alignment.Centre)
        ///     Water constructor.
        /// </Summary>
        public Water(ThirdPersonCamera camera, Vector3 modelPosition, Matrix modelRotation, float width, float height) : base(camera, modelPosition, modelRotation, width, height, width / 5f, height / 5f, Alignment.Centre)
        {

        }

        /// <Summary>
        /// public void Load()
        ///     Load the assets for the water.
        /// </Summary>
        public void Load()
        {
            base.BaseLoad("Planes/Water");
        }
    }
}
