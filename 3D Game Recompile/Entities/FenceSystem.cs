﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace _3D_Game_Recompile.Entities
{
    /// <Summary>
    /// Fence System Class: A collection of fences. A class managing the fences.
    /// </Summary> 
    class FenceSystem
    {
        private List<Fence> fences;
        private int gridWidth;
        private ThirdPersonCamera camera;
        private int fenceWidth = 5;

        /// <Summary>
        /// public FenceSystem(ThirdPersonCamera camera, int gridWidth)
        ///     Fence System constructor.
        /// </Summary>
        public FenceSystem(ThirdPersonCamera camera, int gridWidth)
        {
            this.gridWidth = gridWidth;
            this.camera = camera;
            fences = new List<Fence>();
        }

        /// <Summary>
        /// public void Update()
        ///     Updates the fences at every frame.
        /// </Summary>
        public void Update()
        {
            for (int i = 0; i < fences.Count; i++)
            {
                fences[i].Update();
            }
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the fences at every frame.
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            for (int i = 0; i < fences.Count; i++)
            {
                fences[i].Draw(viewMatrix);
            }
        }

        /// <Summary>
        /// public void AddFence(int amount)
        ///     Adds a new fence to the fence system.
        /// </Summary>
        public void AddFence(int amount)
        {
            int currentCount = fences.Count / 2;
            for (int i = 0; i < amount; i++)
            {
                Fence fence = new Fence(new Vector3(-gridWidth / 2, -0.5f, (currentCount * fenceWidth + fenceWidth * i) ));
                fence.RotationMatrix = Matrix.CreateRotationY((float)Math.PI / 2);
                fence.Load(camera);
                fence.TransformMatrix = Matrix.CreateTranslation(fence.Position);
                fences.Add(fence);
            }
            for (int i = 0; i < amount; i++)
            {
                Fence fence = new Fence(new Vector3(gridWidth / 2, -0.5f, (currentCount * fenceWidth + fenceWidth * i)));
                fence.RotationMatrix = Matrix.CreateRotationY((float)Math.PI / 2);
                fence.Load(camera);
                fence.TransformMatrix = Matrix.CreateTranslation(fence.Position);
                fences.Add(fence);
            }
        }

        /// <Summary>
        /// public void RemoveFence()
        ///     Removes a fence from the system
        /// </Summary>
        public void RemoveFence()
        {
            fences[fences.Count - 1] = null;
            fences[fences.Count - 2] = null;
            fences = Global.RefactorList<Fence>(fences);
        }

        #region Properties
        public int FenceWidth
        {
            get { return fenceWidth; }
        }

        public int Count
        {
            get { return fences.Count; }
        }
        #endregion

    }
}
