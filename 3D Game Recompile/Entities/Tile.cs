﻿using _3D_Game_Recompile.Core_Game_System;
using _3D_Game_Recompile.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Tile Class: A class for the tiles used in the play grid
    /// </Summary>
    public class Tile : Quad
    {
        private Texture2D texture, textureSelected, textureMultiply, textureFused;
        private Boolean hover;
        private Cube cube;
        private TimerComponent timer;
        private TileState tileState;
        private Marker marker;

        public enum TileState
        {
            Selected,
            MultiplySelected,
            Fused,
            Neutral
        }

        /// <Summary>
        /// public Tile(Vector3 origin, Vector3 normal, Vector3 up, float width, float height) : base(origin, normal, up, width, height)
        ///     Constructor for a tile
        /// </Summary>
        public Tile(Vector3 origin, Vector3 normal, Vector3 up, float width, float height) : base(origin, normal, up, width, height)
        {
            timer = new TimerComponent(10);
            tileState = TileState.Neutral;
            marker = new Marker();
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera)
        ///     Loads the assets for a tile
        /// </Summary> 
        public void Load(ThirdPersonCamera camera)
        {
            base.BaseLoad(camera);
            texture = Global.Content.Load<Texture2D>("Tiles/GroundTile");
            textureSelected = Global.Content.Load<Texture2D>("Tiles/GroundTileSelected");
            textureMultiply = Global.Content.Load<Texture2D>("Tiles/GroundTileMultiply");
            textureFused = Global.Content.Load<Texture2D>("Tiles/GroundTileFused");

            marker.Load(camera);
            marker.Position = new Vector3(origin.X, origin.Y+3, origin.Z);
            //Matrix View = Matrix.CreateLookAt(new Vector3(0, 0, 1), Vector3.Zero, Vector3.Up);
            //Matrix Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, 4.0f / 3.0f, 1, 500);
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Updates the tile for each frame
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            hover = false;

            if (marker != null) marker.Update(gameTime);
            switch(tileState) {
                case TileState.Fused:
                    quadEffect.Texture = textureFused;
                    marker.SetFused();
                    break;
                case TileState.Selected:
                    quadEffect.Texture = textureSelected;
                    marker.SetSelected();
                    break;
                case TileState.MultiplySelected:
                    quadEffect.Texture = textureMultiply;
                    marker.SetMultiply();
                    break;
                case TileState.Neutral:
                    quadEffect.Texture = texture;
                    timer.Reset();
                    break;
            }

            if (tileState == TileState.Selected || tileState == TileState.Fused)
            {
                timer.Update(gameTime);
                if (timer.Ticking)
                {
                    tileState = TileState.Neutral;
                }
            }
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the tile for each frame
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            base.BaseDraw(viewMatrix);

            switch (tileState)
            {
                case TileState.Fused:
                    marker.Draw(viewMatrix);
                    break;
                case TileState.Selected:
                    marker.Draw(viewMatrix);
                    break;
                case TileState.MultiplySelected:
                    marker.Draw(viewMatrix);
                    break;
                case TileState.Neutral:
                    break;
            }
        }

        /// <Summary>
        /// public void SetCubeOff()
        ///     Removes the associated cube from tile
        /// </Summary>
        public void SetCubeOff()
        {
            this.cube = null;
        }

        #region Properties
        public Cube Cube
        {
            get { return cube; }
            set { cube = value; }
        }

        public Boolean Hover
        {
            get { return hover; }
            set { hover = value; }
        }

        public TileState State
        {
            get { return tileState; }
            set { tileState = value; }
        }
        #endregion
    }
}
