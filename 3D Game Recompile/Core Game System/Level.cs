﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Level Class: A level in the game containing all the data for a level
    /// </Summary>
    public class Level
    {
        private List<Wave> waveData = new List<Wave>();
        private int currentWaveIndex;
        private int levelID;
        private int width, height;
        private Vector3 blockData;
        private string name;

        /// <Summary>
        /// public Level():
        ///     The constructor for a Level
        /// </Summary>
        public Level()
        {
            blockData = new Vector3(0, 0, 0); //GoodBlocks, BadBlocks, MultiplyBlocks respectively.
        }

        /// <Summary>
        /// public void AddWave(string waveString):
        ///     Adds a wave of cubes to the Level
        /// </Summary>
        public void AddWave(string waveString)
        {
            Wave wave = new Wave(waveString);
            waveData.Add(wave);
            blockData += wave.BlockCount;
        }

        /// <Summary>
        /// public void AddWave(string waveString):
        ///     Adds a wave of cubes to the Level
        /// </Summary>
        public Wave NextWave(int steps)
        {
            currentWaveIndex += steps;
            try
            {
                return waveData[currentWaveIndex];
            }
            catch (System.ArgumentOutOfRangeException)
            {
                //throw new System.ArgumentOutOfRangeException("index parameter is out of range.", e);
                return null;
            }
        }

        #region Properties
        public List<Wave> Waves
        {
            get { return waveData; }
        }

        public Wave CurrentWave
        {
            get {
                try
                {
                    return waveData[currentWaveIndex];
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    return null;
                }
            }
        }

        public int CurrentWaveIndex
        {
            get { return waveData.IndexOf(CurrentWave); }
            set { currentWaveIndex = value; }
        }

        public int ID
        {
            get { return levelID; }
            set { levelID = value; }
        }

        public int Width
        {
            get { return width; }
            set { width = value; }
        }

        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        public Vector3 BlockData
        {
            get { return blockData; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        #endregion
    }
}
