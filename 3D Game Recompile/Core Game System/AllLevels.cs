﻿using System.Collections.Generic;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// AllLevels Class: This is a static class which contains all the levels
    /// </Summary>
    public static class AllLevels
    {
        private static List<Level> levels = new List<Level>();
        private static List<Level> customLevels = new List<Level>();
        private static Level currentLevel;

        /// <Summary>
        /// public static Level CreateLevel(): 
        ///     Creates a Level
        /// </Summary>
        public static Level CreateLevel() //this is used in the XML reader.
        {
            Level level = new Level();
            levels.Add(level);
            level.ID = levels.IndexOf(level);
            return level;
        }

        /// <Summary>
        /// public static Level NextLevel(int steps): 
        ///     Moves into the next level given the number of steps
        /// </Summary>
        public static Level NextLevel(int steps)
        {
            int index = levels.IndexOf(currentLevel);
            try
            {
                currentLevel = levels[index + steps];
            }
            catch (System.ArgumentOutOfRangeException)
            {
                return null;
            }

            return currentLevel;
        }

        /// <Summary>
        /// public static void ResetLevels(): 
        ///     Clears all the levels loaded. Needed when restarting a level or playing a level again.
        /// </Summary>
        public static void ResetLevels()
        {
            levels.Clear();
            customLevels.Clear();
            XMLReader.ReadLevels();
            XMLReader.ReadCustomLevels();
        }

        /// <Summary>
        /// public static Level GetLevel(int index): 
        ///     Gets the level based on a number given
        /// </Summary>
        public static Level GetLevel(int index)
        {
            return levels[index];
        }

        #region Properties
        public static List<Level> Levels
        {
            get { return levels; }
            set { levels = value; }
        }

        public static List<Level> CustomLevels
        {
            get { return customLevels; }
            set { customLevels = value; }
        }

        public static Level CurrentLevel
        {
            get { return currentLevel; }
            set { currentLevel = value; }
        }

        public static int CurrentLevelIndex
        {
            get { return levels.IndexOf(currentLevel); }
        }
        #endregion
    }
}
