﻿using _3D_Game_Recompile.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// PlayGrid Class: The Grid of the game
    /// </Summary>
    public class PlayGrid
    {
        private int width, height;
        private int tileWidth, tileHeight;
        private Tile[,] tiles;
        private Player player;
        private Level currentLevel;
        private Boolean tileMarked;
        private ThirdPersonCamera camera;
        private Wave currentWave;
        private Vector2 offset = new Vector2(0, 4);
        enum CollisionType { None, Building, Boundary, Target }
        private SoundEffect tileMarkSound;
        private PlayScreen screen;

        /// <Summary>
        /// public PlayGrid(PlayScreen screen, ThirdPersonCamera camera, int width, int height, Level level)
        ///     The constructor of the play grid
        /// </Summary>
        public PlayGrid(PlayScreen screen, Player player, ThirdPersonCamera camera, int width, int height, Level level)
        {
            this.width = width;
            this.height = height;
            this.camera = camera;
            this.screen = screen;
            this.player = player;
            
            tileWidth = 2;
            tileHeight = 2;

            this.currentLevel = level;
            tiles = new Tile[width, height];
            CreateTiles();
        }

        /// <Summary>
        /// private void CreateTiles()
        ///     Creates the tiles for the grid
        /// </Summary>
        private void CreateTiles()
        {
            offset.X = -width / 2;
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    tiles[i, j] = new Tile(new Vector3((i + offset.X) * tileWidth, -0.5f, (j) * tileHeight), Vector3.Up, Vector3.Forward, tileWidth, tileHeight); //change to make tiles go upwards or downwards
                }
            }
        }

        /// <Summary>
        /// public void Load(Level level)
        ///     Loads the assets for the grid
        /// </Summary>
        public void Load(Level level)
        {
            LoadTiles();
            currentLevel = level;
            LoadNextWave(0);
            tileMarkSound = Global.Content.Load<SoundEffect>("Sounds/selectTile");
        }

        /// <Summary>
        /// public Boolean LoadNextWave(int steps)
        ///     Loads the next wave for the current level
        /// </Summary>
        public Boolean LoadNextWave(int steps)
        {
            Boolean levelInProgress = true;
            currentWave = currentLevel.NextWave(steps);
            if (currentWave == null)                                                                        //Finish!
            {
                levelInProgress = false;
                return levelInProgress;
            }
            currentWave.Load(camera, tileWidth, tileHeight, tiles);                                        //Create  wave
            return levelInProgress;
        }

        /// <Summary>
        /// private void LoadTiles()
        ///     Loads the assets for the tiles
        /// </Summary>
        private void LoadTiles()
        {
            foreach (Tile tile in tiles)
            {
                tile.Load(camera);
            }
        }

        /// <Summary>
        /// public void Update(Player player, GameTime gameTime)
        ///     Updates the PlayGrid at teach time interval
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            if (currentWave != null)
            {
                int count = 0;
                for (int i = 0; i < tiles.GetLength(0); i++)
                {
                    for (int j = 0; j < tiles.GetLength(1); j++)
                    {
                        count += UpdateTiles(gameTime, tiles[i, j], count);
                        SetPlayerHover(tiles[i, j]);
                        tiles[i, j].SetCubeOff();
                        SetCubeHover(tiles[i, j]);
                        CheckFusedTiles(gameTime, tiles[i, j]);
                    }
                }
                currentWave.Update(gameTime, tiles.GetLength(1) * tileWidth - 1);
                if (count > 0) { tileMarked = true; }
                else { tileMarked = false; }

                for (int i = 0; i < currentWave.BoundingBoxes.Length; i++)
                {
                    Vector3 newPosForward = currentWave.BoundingBoxes[i].GetCorners()[3];
                    Vector3 newPosLeft = currentWave.BoundingBoxes[i].GetCorners()[2];
                    Vector3 newPosBottom = currentWave.BoundingBoxes[i].GetCorners()[4];

                    BoundingSphere playerSphereForward = new BoundingSphere(player.Position + new Vector3(0, 0, -Global.CollisionDistance), 0.01f);
                    BoundingSphere playerSphereBackward = new BoundingSphere(player.Position + new Vector3(0, 0, Global.CollisionDistance), 0.01f);
                    BoundingSphere playerSphereLeft = new BoundingSphere(player.Position + new Vector3(-Global.CollisionDistance, 0, 0), 0.01f);
                    BoundingSphere playerSphereRight = new BoundingSphere(player.Position + new Vector3(Global.CollisionDistance, 0, 0), 0.01f);

                    if (CheckContact(playerSphereForward, currentWave.BoundingBoxes[i]) == CollisionType.Building) player.Position = new Vector3(player.Position.X, player.Position.Y, newPosLeft.Z + Global.CollisionDistance);
                    if (CheckContact(playerSphereBackward, currentWave.BoundingBoxes[i]) == CollisionType.Building) player.Position = new Vector3(player.Position.X, player.Position.Y, newPosBottom.Z - Global.CollisionDistance);
                    if (CheckContact(playerSphereLeft, currentWave.BoundingBoxes[i]) == CollisionType.Building) player.Position = new Vector3(newPosLeft.X + Global.CollisionDistance, player.Position.Y, player.Position.Z);
                    if (CheckContact(playerSphereRight, currentWave.BoundingBoxes[i]) == CollisionType.Building) player.Position = new Vector3(newPosForward.X - Global.CollisionDistance, player.Position.Y, player.Position.Z);
                }
            }
        }

        #region UpdateMethods
        /// <Summary>
        /// private int UpdateTiles(GameTime gameTime, Tile q, int count)
        ///     Updates the tiles
        /// </Summary>
        private int UpdateTiles(GameTime gameTime, Tile q, int count)
        {
            q.Update(gameTime);
            if (q.State == Tile.TileState.Selected || q.State == Tile.TileState.Fused) return 1;
            else return 0;
        }

        /// <Summary>
        /// private void SetCubeHover(Tile quad)
        ///     Sets what cube is on top of what quad
        /// </Summary>
        private void SetCubeHover(Tile quad)
        {
            for (int k = 0; k < currentWave.Cubes.GetLength(0); k++)
            {
                for (int l = 0; l < currentWave.Cubes.GetLength(1); l++)
                {
                    if (currentWave.Cubes[k, l] != null)
                    {
                        if (InQuad(quad, currentWave.Cubes[k, l])) quad.Cube = currentWave.Cubes[k, l];
                    }
                }
            }
        }

        /// <Summary>
        /// private Boolean InQuad(Tile q, BaseEntity entity)
        ///     Checks to see what Entity is within what Tile
        /// </Summary>
        private Boolean InQuad(Tile q, BaseEntity entity)
        {
            if (q.UpperLeft.X < entity.Position.X && q.UpperRight.X > entity.Position.X)
            {
                if (q.UpperLeft.Z < entity.Position.Z && q.LowerLeft.Z > entity.Position.Z)
                {
                    return true;
                }
            }
            return false;
        }

        /// <Summary>
        /// private void SetPlayerHover(Tile quad)
        ///     Set which tile the player is currently standing on
        /// </Summary>
        private void SetPlayerHover(Tile quad)
        {
            if (InQuad(quad, player))
            {
                quad.Hover = true;
            }
            else
            {
                quad.Hover = false;
            }
        }

        /// <Summary>
        /// private void CheckFusedTiles(GameTime gameTime, Tile quad)
        ///     Checks which tiles are fused
        /// </Summary>
        private void CheckFusedTiles(GameTime gameTime, Tile tile)
        {
            if (tile.Cube != null)
            {
                if (tile.State == Tile.TileState.Fused && currentWave.Timer.Ticking)
                {
                    DetonateTile(tile);
                }
            }
            if (currentWave.Timer.Ticking) UnfuseTile(tile);
        }

        /// <Summary>
        /// private CollisionType CheckCollision(BoundingSphere sphere)
        ///     Checks if the player if colliding with the cubes or stage boundry
        /// </Summary>
        /*private CollisionType CheckCollision(BoundingSphere sphere) //unused
        {
            if (currentWave != null)
            {
                for (int i = 0; i < currentWave.BoundingBoxes.Length; i++)
                    if (currentWave.BoundingBoxes[i].Contains(sphere) != ContainmentType.Disjoint)
                        return CollisionType.Building;

                /*if (playGridBoundingBox.Contains(sphere) != ContainmentType.Contains)
                    return CollisionType.Boundary;
            }

            return CollisionType.None;
        }*/

        /// <Summary>
        /// private CollisionType CheckCollision(BoundingSphere sphere)
        ///     Checks if the player if colliding with the cubes or stage boundry
        /// </Summary>
        private CollisionType CheckContact(BoundingSphere player, BoundingBox cubeBox)
        {
            if (cubeBox.Contains(player) != ContainmentType.Disjoint)
            {
                return CollisionType.Building;
            }
            return CollisionType.None;
        }
        #endregion

        #region Tile Actions
        /// <Summary>
        /// public void MarkTile()
        ///     Marks a tile with a black circle
        /// </Summary>
        public void MarkTile()
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    Tile q = tiles[i, j];
                    if (q.State == Tile.TileState.Fused) return;
                    if (q.Hover)
                    {
                        SoundPlayer.Play(tileMarkSound);
                        q.State = Tile.TileState.Selected;
                    }
                }
            }
        }

        /// <Summary>
        /// public void FuseTile(int i, int j)
        ///     Turns the marked tile into a fused red tile.
        /// </Summary>
        public void FuseTile(int i, int j)
        {
            try
            {
                tiles[i, j].State = Tile.TileState.Fused;
            }
            catch (System.IndexOutOfRangeException)
            { }
        }

        /// <Summary>
        /// public void FuseTile()
        ///     Checks for any marked tiles then, turns the marked tile into a fused red tile.
        /// </Summary>
        public void FuseTile()
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    if (tiles[i, j].State == Tile.TileState.Selected) tiles[i, j].State = Tile.TileState.Fused;
                }
            }
        }

        /// <Summary>
        /// public void FuseTile(int i, int j)
        ///     Turns a tile into a green multiply tile
        /// </Summary>
        public void MarkMultiplytile(Tile quad)
        {
            quad.State = Tile.TileState.MultiplySelected;
        }

        /// <Summary>
        /// public void FuseAreaTile()
        ///     Fuses a 3x3 area tile Area
        /// </Summary>
        public void FuseAreaTile()
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    if (tiles[i, j].State == Tile.TileState.MultiplySelected)
                    {
                        for (int k = -1; k < 2; k++)
                        {
                            for (int l = -1; l < 2; l++)
                            {
                                try
                                {
                                    if (tiles[i + k, j + l].State != Tile.TileState.MultiplySelected)
                                        FuseTile(i + k, j + l);
                                }
                                catch (IndexOutOfRangeException) { }
                            }
                        }
                        tiles[i, j].State = Tile.TileState.Fused;
                    }
                }
            }
        }

        /// <Summary>
        /// public void UnfuseTile(Tile quad)
        ///     Changes back the fused tile to a neutral state
        /// </Summary>
        public void UnfuseTile(Tile quad)
        {
            if (quad.State == Tile.TileState.Fused)
            {
                quad.State = Tile.TileState.Neutral;
            }
        }

        /// <Summary>
        /// private void DetonateTile(Tile quad)
        ///     Detonates a fused tile, carrying a block with it
        /// </Summary>
        private void DetonateTile(Tile quad)
        {
            currentWave.Detonate(quad, screen);
        }
        #endregion

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the PlayGrid on screen
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    tiles[i, j].Draw(viewMatrix);
                }
            }

            if (currentWave != null) currentWave.Draw(viewMatrix);
        }

        /// <Summary>
        /// public Tile[,] EditRow(int j)
        ///     Adds or Subtracts a row from the grid
        /// </Summary>
        public Tile[,] EditRow(int j)
        {
            int newHeight = tiles.GetLength(1) + j;
            tiles = Global.ResizeArray<Tile>(tiles, tiles.GetLength(0), newHeight);

            for (int k = 0; k < tiles.GetLength(0); k++)
            {
                for (int l = 0; l < tiles.GetLength(1); l++)
                {
                    if (l == newHeight - 1)
                    {
                        tiles[k, l] = new Tile(new Vector3((k + offset.X) * tileWidth, -0.5f, (l) * tileHeight), Vector3.Up, Vector3.Forward, tileWidth, tileHeight);
                        tiles[k, l].Load(camera);
                    }
                }
            }
            return tiles;
        }

        #region Properties
        public Cube[,] Cubes
        {
            get { return currentWave.Cubes; }
        }

        public Boolean TileMarked
        {
            get { return tileMarked; }
            set { tileMarked = value; }
        }
        public Wave CurrentWave
        {
            get { return currentWave; }
        }

        public Tile[,] Quads
        {
            get { return tiles; }
        }

        public int TileWidth
        {
            get { return tileWidth; }
        }
        #endregion
    }
}
