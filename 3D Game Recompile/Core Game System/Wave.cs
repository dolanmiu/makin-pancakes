﻿using _3D_Game_Recompile.Entities;
using _3D_Game_Recompile.Entities.Particles;
using _3D_Game_Recompile.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Wave Class: A class storing a set of cubes associated with a level
    /// </Summary>
    public class Wave
    {
        private Cube[,] cubes;
        private ThirdPersonCamera camera;
        private int tileWidth, tileHeight;
        private bool cleared = false;
        private int goodCount, darkCount, multiplyCount;
        private BoundingBox[] cubeBoundingBoxes;
        private string waveString;
        private TimerComponent cubeTimer;
        private CubeParticles cubeParticles;
        private ExplosionMachine cubeSmoke;
        private SplashParticles splashParticles;
        private bool snared = false;
        private Random random;

        /// <Summary>
        /// public Wave(string waveString)
        ///     Constructor for a Wave
        /// </Summary>
        public Wave(string waveString)
        {
            this.waveString = waveString;
            goodCount = 0; darkCount = 0; multiplyCount = 0;
            List<string> waveRows = waveString.Split(',').ToList<string>();
            cubeTimer = new TimerComponent(2);

            cubes = new Cube[waveRows.Count, waveRows[0].Length];
            cubeParticles = new CubeParticles();
            cubeSmoke = new ExplosionMachine();
            splashParticles = new SplashParticles();

            for (int i = 0; i < waveRows.Count; i++)                                    //slice of wave, maybe a row.
            {
                char[] waveChar = waveRows[i].ToCharArray();
                for (int j = 0; j < waveChar.Length; j++)                               //each invidual cube, slice of a row.
                {
                    int cubeID;
                    if (!int.TryParse(waveChar[j].ToString(), out cubeID))
                    {
                        //Do something to correct the problem
                        //Go back to home screen with error?
                    }

                    cubes[i, j] = cubeSelection(cubeID);
                }
            }
        }

        /// <Summary>
        /// public void Load(ThirdPersonCamera camera, int tileWidth, int tileHeight, Tile[,] quads, Vector2 offset)
        ///     Loads the asset for a Wave
        /// </Summary>
        public void Load(ThirdPersonCamera camera, int tileWidth, int tileHeight, Tile[,] quads)
        {
            this.camera = camera;
            this.tileHeight = tileHeight;
            this.tileWidth = tileWidth;
            cubeParticles.Load(camera);
            cubeSmoke.Load(camera);
            splashParticles.Load(camera);
            random = new Random();
            for (int i = 0; i < cubes.GetLength(0); i++)
            {
                for (int j = 0; j < cubes.GetLength(1); j++)
                {
                    if (cubes[i, j] != null)
                    {
                        cubes[i, j].Load(camera);
                        cubes[i, j].Position = new Vector3((quads[i, j].Position.X), 0.5f, (quads[i, j].Position.Z));
                        cubes[i, j].SetAnimation();
                    }
                }
            }
        }

        /// <Summary>
        /// public void SetRollTime(float time)
        ///     Sets how long it takes for cubes to roll
        /// </Summary>
        public void SetRollTime(float time)
        {
            cubeTimer.Interval = time;
            foreach (Cube cube in cubes)
            {
                if (cube != null) cube.Interval = time * 0.5f;
            }
        }

        /// <Summary>
        /// private Cube cubeSelection(int number)
        ///     Selects a cube based on an index
        /// </Summary>
        private Cube cubeSelection(int number)
        {
            switch (number)
            {
                case 0:
                    break;
                case 1:
                    Cube defaultCube = new DefaultCube();
                    goodCount++;
                    return defaultCube;
                case 2:
                    Cube multiplyCube = new MultiplyCube();
                    multiplyCount++;
                    return multiplyCube;
                case 3:
                    Cube forbiddenCube = new ForbiddenCube();
                    darkCount++;
                    return forbiddenCube;
            }
            return null;
        }

        /// <Summary>
        /// public void Update(GameTime gameTime)
        ///     Updates the Wave at each frame
        /// </Summary>
        public void Update(GameTime gameTime, int stageEdge)
        {
            int nullCounter = 0;
            cubeTimer.Update(gameTime);
            cubeParticles.Update(gameTime);
            cubeSmoke.Update(gameTime);
            splashParticles.Update(gameTime);
            for (int i = 0; i < cubes.GetLength(0); i++)
            {
                for (int j = 0; j < cubes.GetLength(1); j++)
                {
                    if (cubes[i, j] != null)
                    {
                        cubes[i, j].Update(this, gameTime, cubeTimer);
                        CheckBounds(i, j, stageEdge);
                    }
                    else
                    {
                        nullCounter++;
                    }
                }
            }

            if (cubes.Length == nullCounter)
            {
                cleared = true;
            }
            else
            {
                cleared = false;
            }

            cubeBoundingBoxes = SetBoundingBox();
        }

        /// <Summary>
        /// private BoundingBox[] SetBoundingBox()
        ///     Sets the collision box for the Wave
        /// </Summary>
        private BoundingBox[] SetBoundingBox()
        {
            List<BoundingBox> bbList = new List<BoundingBox>();
            for (int i = 0; i < Cubes.GetLength(0); i++)
            {
                for (int j = 0; j < Cubes.GetLength(1); j++)
                {
                    if (Cubes[i, j] != null)
                    {
                        Vector3[] cubePoints = new Vector3[2];
                        cubePoints[0] = Cubes[i, j].AnimationPositionBottomRight;
                        cubePoints[1] = Cubes[i, j].AnimationPositionTopLeft;
                        BoundingBox buildingBox = BoundingBox.CreateFromPoints(cubePoints);
                        bbList.Add(buildingBox);
                    }
                }
            }
            return bbList.ToArray();
        }

        /// <Summary>
        /// private void CheckBounds(int i, int j)
        ///     Checks to see if the cubes of the wave have went over the edge of the river
        /// </Summary>
        private void CheckBounds(int i, int j, int stageEdge)
        {
            if (cubes[i, j].AnimationPosition.Z > stageEdge)
            {
                cubes[i, j].FallDown();
            }
            if (cubes[i, j].Position.Y < -2 && !cubes[i, j].Sunk)
            {
                splashParticles.CreateSplash(new Vector3(cubes[i, j].Position.X,cubes[i, j].Position.Y, cubes[i, j].Position.Z+2));
                cubes[i, j].Sunk = true;
            }
            if (cubes[i, j].Position.Y < -20)
            {
                if (cubes[i, j].ModelName == "defaultCube")
                {
                    PlayerStats.LifeLine--;
                    PlayerStats.AddNextRows = 0;
                    if (PlayerStats.LifeLine <= 0)
                    {
                        PlayerStats.AddNextRows = -1;
                    }
                }
                cubes[i, j] = null;
            }
        }

        /// <Summary>
        /// public void Draw(Matrix viewMatrix)
        ///     Draws the Wave at each frame
        /// </Summary>
        public void Draw(Matrix viewMatrix)
        {
            for (int i = 0; i < cubes.GetLength(0); i++)
            {
                for (int j = 0; j < cubes.GetLength(1); j++)
                
                {
                    if (cubes[i, j] != null)
                    {
                        cubes[i, j].Draw(viewMatrix);
                    }
                }
            }
            cubeParticles.Draw(viewMatrix);
            cubeSmoke.Draw(viewMatrix);
            splashParticles.Draw(viewMatrix);
        }

        /// <Summary>
        /// public void Detonate(Tile quad, PlayScreen screen)
        ///     Detonates a cube, removes the cube from the List
        /// </Summary>
        public void Detonate(Tile quad, PlayScreen screen)
        {
            Vector2 coords = FindCube(quad.Cube);
            int i = (int)coords.X;
            int j = (int)coords.Y;
            if (cubes[i, j] != null) cubes[i, j].DetonationAction(quad, screen);
            float disp = (float)tileWidth / 4f;
            cubeParticles.CreateCubes(cubes[i, j].Model, cubes[i, j].Position, new Vector3(0, 0.3f, 0), disp);
            cubeSmoke.CreateExplosion(20, cubes[i, j].Position + new Vector3(disp, disp * 2, disp));
            cubes[i, j] = null;
        }

        /// <Summary>
        /// public Vector2 FindCube(Cube cube)
        ///     Finds a cube in the List based on a cube referance
        /// </Summary>
        public Vector2 FindCube(Cube cube)
        {
            for (int i = 0 ; i < cubes.GetLength(0); i++)
                for(int j = 0 ; j < cubes.GetLength(1) ; j++)
                {
                    if ( cubes[i,j] == cube)
                    {
                        return new Vector2(i, j);
                    }
                }
            return new Vector2(-1, -1);
        }

        /// <Summary>
        /// public void MoveFast()
        ///     Speeds up the wave
        /// </Summary>
        /*public void MoveFast()
        {
            for (int i = 0; i < cubes.GetLength(0); i++)
            {
                for (int j = 0; j < cubes.GetLength(1); j++)
                {
                    if (cubes[i, j] != null)
                    {
                        cubes[i, j].MoveFast(cubeTimer); //referance to the cube. Values can be set inside the cube.
                    }
                }
            }
        }*/

        #region Properties
        public Cube[,] Cubes
        {
            get { return cubes; }
        }

        public bool Cleared
        {
            get { return cleared; }
        }

        public Vector3 BlockCount
        {
            get { return new Vector3(goodCount, darkCount, multiplyCount); }
        }

        public BoundingBox[] BoundingBoxes
        {
            get { return cubeBoundingBoxes; }
        }

        
        public string WaveString
        {
            get { return waveString; }
        }

        public TimerComponent Timer
        {
            get { return cubeTimer; }
        }

        public bool Snared
        {
            get { return snared; }
            set { snared = value; }
        }
        #endregion
    }
}
