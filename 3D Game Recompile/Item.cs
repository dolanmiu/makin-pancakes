﻿using Microsoft.Xna.Framework.Graphics;

namespace _3D_Game_Recompile
{
    public class Item
    {
        private Texture2D thumbnail;
        private string name;
        private int price;
        private int id;

        public Item(string name, int price, string thumbnailPath)
        {
            this.thumbnail = Global.Content.Load<Texture2D>(thumbnailPath);
            this.name = name;
            this.price = price;
        }

        public override string ToString()
        {
            return name;
        }

        #region Properties
        public Texture2D Thumnnail
        {
            get { return thumbnail; }
        }

        public int Price
        {
            get { return price; }
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        #endregion
    }
}
