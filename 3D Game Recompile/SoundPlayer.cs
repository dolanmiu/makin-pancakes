﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace _3D_Game_Recompile
{
    public static class SoundPlayer
    {
        private static bool soundFXMuted;
        private static bool songMuted;
        private static List<Song> songs = new List<Song>();
        private static Song selectedSong;

        public static void Play(SoundEffect sound)
        {
            if (!soundFXMuted) sound.Play();
        }

        public static void LoadMusic()
        {
            Song menuMusic = Global.Content.Load<Song>("Sounds/Songs/menuMusic");
            LoadSong(menuMusic);
            Song levelMusic = Global.Content.Load<Song>("Sounds/Songs/levelMusic");
            LoadSong(levelMusic);
            Song levelDesignerMusic = Global.Content.Load<Song>("Sounds/Songs/levelDesignerMusic");
            LoadSong(levelDesignerMusic);
        }

        public static void Play(SoundEffect sound, float volume, float pitch, float pan)
        {
            if (!soundFXMuted) sound.Play(volume, pitch, pan);
        }

        public static void PlaySong(string name)
        {
            if (!songMuted)
            {
                foreach (Song song in songs)
                {
                    if (song.Name == name)
                    {
                        selectedSong = song;
                    }
                }
                if (selectedSong != null) MediaPlayer.Play(selectedSong);
                MediaPlayer.IsRepeating = true;
            }
        }

        public static void LoadSong(Song song)
        {
            songs.Add(song);
        }

        public static void StopSong()
        {
            MediaPlayer.Stop();
        }

        public static void PauseSong()
        {
            MediaPlayer.Pause();
        }

        public static void ResumeSong()
        {
            MediaPlayer.Resume();
        }

        #region Properties
        public static bool SoundFXMuted
        {
            get { return soundFXMuted; }
            set { soundFXMuted = value; }
        }

        public static bool SongMuted
        {
            get { return songMuted; }
            set { songMuted = value; }
        }
        #endregion
    }
}
