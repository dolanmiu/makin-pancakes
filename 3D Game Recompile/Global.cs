﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Windows.Storage;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Global Class: Global Variables and functions.
    /// </Summary>
    public static class Global
    {
        private static Vector2 screenSize;
        private static GraphicsDevice graphicsDevice;
        private static SpriteBatch spriteBatch;
        private static ContentManager content;
        private static StorageFolder gameDirectory = Windows.Storage.ApplicationData.Current.LocalFolder; //KnownFolders.DocumentsLibrary;
        private static Random random = new Random();
        //await ApplicationData.Current.LocalFolder.GetFileAsync(fileName);
        #region UI Scaling
        public static float buttonScale;
        public static float CreditsScale = 0.7f;
        public static int ButtonSep;
        #endregion

        #region Physics variables
        public static float CollisionDistance = 0.3f;
        public static float Gravity = 0.3f; //needs to be used;
        #endregion

        /// <Summary>
        /// public static T[,] ResizeArray<T>(T[,] original, int rows, int cols):
        ///     Resies a 2D array.
        /// </Summary>
        public static T[,] ResizeArray<T>(T[,] original, int rows, int cols)
        {
            var newArray = new T[rows, cols];
            int minRows = Math.Min(rows, original.GetLength(0));
            int minCols = Math.Min(cols, original.GetLength(1));
            for (int i = 0; i < minRows; i++)
                for (int j = 0; j < minCols; j++)
                    newArray[i, j] = original[i, j];
            return newArray;
        }

        /// <Summary>
        /// private List<T> RefactorList<T>(List<T> list):
        ///     Reduces a List into the correct sizes, and removing nulls.
        /// </Summary>
        public static List<T> RefactorList<T>(List<T> list)
        {
            List<T> newList = new List<T>();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] != null) newList.Add(list[i]);
            }
            return newList;
        }

        /// <Summary>
        /// protected float GetRandomNumber(double minimum, double maximum)
        ///     Creates a random number between 2 numbers.
        /// </Summary>
        public static float GetRandomNumber(double minimum, double maximum)
        {
            return (float)(random.NextDouble() * (maximum - minimum) + minimum);
        }

        /// <Summary>
        /// public static float GetRandomInt(int minimum, int maximum)
        ///     Creates a random integer between 2 numbers.
        /// </Summary>
        public static int GetRandomInt(int minimum, int maximum)
        {
            return random.Next(minimum, maximum);
        }

        /// <Summary>
        /// public static Stream GenerateStreamFromString(string s): 
        ///     Generates a steam from a string.
        /// </Summary>
        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        /// <Summary>
        /// public static Vector2 ScreenSize:
        ///     Gets the screen size and sets the global variables to its appropriate values
        /// </Summary>
        public static Vector2 ScreenSize
        {
            get { return screenSize; }
            set { 
                screenSize = value; 
                float desiredRatio = 29f / 192f;
                float currentRatio = 290f / screenSize.X;
                buttonScale = desiredRatio / currentRatio;
                ButtonSep = (int)(10 * buttonScale);
                Debug.WriteLine(Global.buttonScale);
            }
        }

        public static GraphicsDevice GraphicsDevice
        {
            get { return graphicsDevice; }
            set { graphicsDevice = value; }
        }

        public static SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
            set { spriteBatch = value; }
        }

        public static ContentManager Content
        {
            get { return content; }
            set { content = value; }
        }

        public static StorageFolder GameDirectory
        {
            get { return gameDirectory; }
            set { gameDirectory = value; }
        }
    }
}
