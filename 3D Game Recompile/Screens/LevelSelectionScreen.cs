﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Level Selection Screen Class: Selecting a Level.
    /// </Summary>
    class LevelSelectionScreen : Screen
    {

        private List<LevelIcon> levelIcons, customIcons;
        private Button backButton, switchLevelsButton;
        private int tilesPerRow;
        private int tileDimension;
        private int leftMargin;
        private bool fading = false;
        private float fadeAlpha = 0f;
        private Level selectedLevel;
        private enum LevelIconState { Normal, Custom }
        private LevelIconState levelIconState;

        /// <Summary>
        /// public LevelSelectionScreen(Game1 game):
        ///     Level Selection Screen constructor.
        /// </Summary>
        public LevelSelectionScreen(Game1 game) : base(game)
        {
            tileDimension = 100;
            levelIcons = new List<LevelIcon>();
            customIcons = new List<LevelIcon>();
            backButton = new Button(new Rectangle((int)Global.ScreenSize.X / 2, (int)Global.ScreenSize.Y - 50, 100, 50), "Back", menuFont, Alignment.Center);
            switchLevelsButton = new Button(new Rectangle((int)Global.ScreenSize.X - 200, (int)Global.ScreenSize.Y - 200, 200, 50), "Custom Levels", menuFont, Alignment.Right);
            CalculateDistances();

            List<Rectangle> boundsList = GenerateIconPositions<Level>(AllLevels.Levels, tileDimension);
            for (int i = 0; i < boundsList.Count; i++)
            {
                LevelIcon levelIcon = new LevelIcon(boundsList[i], AllLevels.Levels[i], PlayerStats.GetStars(i));
                levelIcons.Add(levelIcon);
            }
        }

        /// <Summary>
        /// private void CalculateDistances():
        ///     Calculates the distances between each Level Icon.
        /// </Summary>
        private void CalculateDistances()
        {
            int maxWidth = (int)Global.ScreenSize.X - ((int)Global.ScreenSize.X / 4);
            tilesPerRow = maxWidth / (tileDimension + Global.ButtonSep);
            leftMargin = (int)Global.ScreenSize.X / 8;
        }

        /// <Summary>
        /// public void Update():
        ///     Updates the Screen per frame.
        /// </Summary>
        public void Update()
        {
            base.BaseUpdate();
            backButton.Update();
            switchLevelsButton.Update();
            if (keyboardState.IsKeyDown(Keys.Back) && lastKeyboardState.IsKeyUp(Keys.Back))
            {
                game.StartScreen();
            }
            lastKeyboardState = keyboardState;

            if (backButton.Clicked()) game.StartScreen();

            if (switchLevelsButton.Clicked())
            {
                if (levelIconState == LevelIconState.Normal)
                {
                    levelIconState = LevelIconState.Custom;
                    switchLevelsButton.Text = "Vanilla Levels";
                }
                else
                {
                    levelIconState = LevelIconState.Normal;
                    switchLevelsButton.Text = "Custom Levels";
                }
                int column = 0;
                int row = 0;
                for (int i = 0; i < AllLevels.CustomLevels.Count; i++)
                {
                    if (column == tilesPerRow)
                    {
                        column = 0;
                        row++;
                    }

                    Rectangle bounds = new Rectangle(leftMargin + column * (tileDimension + Global.ButtonSep), 400 + (tileDimension + Global.ButtonSep) * row, tileDimension, tileDimension);
                    LevelIcon customIcon = new LevelIcon(bounds, AllLevels.CustomLevels[i], 0); //maybe no stars
                    customIcons.Add(customIcon);

                    column++;
                }
            }

                switch (levelIconState)
                {
                    case LevelIconState.Normal:
                        for (int i = 0; i < levelIcons.Count; i++)
                        {

                            if (levelIcons[i].Clicked())
                            {
                                fading = true;
                                selectedLevel = levelIcons[i].Level;
                                SoundPlayer.PlaySong("levelMusic");
                            }
                            levelIcons[i].Update();
                        }
                        break;
                    case LevelIconState.Custom:
                        for (int i = 0; i < customIcons.Count; i++)
                        {

                            if (customIcons[i].Clicked())
                            {
                                fading = true;
                                selectedLevel = customIcons[i].Level;
                                SoundPlayer.PlaySong("levelMusic");
                            }
                            customIcons[i].Update();
                        }
                        break;
                }

            

            if (fadeAlpha >= 2f) game.StartGame(selectedLevel);
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Screen per frame.
        /// </Summary>
        public void Draw(Color[] skyColours)
        {
            base.BaseDraw("Level Select", skyColours);
            Global.SpriteBatch.Begin();
            backButton.Draw();
            switchLevelsButton.Draw();

            switch (levelIconState)
            {
                case LevelIconState.Normal:
                    for (int i = 0; i < levelIcons.Count; i++)
                    {
                        levelIcons[i].Draw();
                    }
                    break;
                case LevelIconState.Custom:
                    for (int i = 0; i < customIcons.Count; i++)
                    {
                        customIcons[i].Draw();
                    }
                    break;
            }

            if (fading)
            {
                Global.SpriteBatch.Draw(WhiteTexture, new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Color.Black * fadeAlpha);
                fadeAlpha += 0.04f;
            }
            Global.SpriteBatch.End();
        }

    }
}
