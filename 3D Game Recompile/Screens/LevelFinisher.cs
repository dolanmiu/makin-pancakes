﻿using _3D_Game_Recompile.PlayerData;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Threading.Tasks;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Level Finisher Class: What is shown after level is complete.
    /// </Summary>
    public class LevelFinisher : Panel
    {
        private SpriteFont HUDFont, menuFont;
        private Rectangle bounds;
        private Vector2 scorePos;
        private Button menuButton, retryButton, nextButton;
        private InGameScreen screen;
        private Texture2D starFull, starEmpty;
        private Rectangle starSize = new Rectangle(0, 0, 35, 31);
        private int star;
        private float percentage;
        private Game1 game;

        /// <Summary>
        /// public LevelFinisher(Game1 game, InGameScreen screen, Rectangle bounds):
        ///     Level Finisher constructor.
        /// </Summary>
        public LevelFinisher(Game1 game, InGameScreen screen, Rectangle bounds) : base()
        {
            this.bounds = bounds;
            this.screen = screen;
            this.game = game;
            int centerLineX = bounds.X + bounds.Width / 2;
            HUDFont = Global.Content.Load<SpriteFont>("Fonts/LogoFont");
            menuFont = Global.Content.Load<SpriteFont>("Fonts/MenuFont");
            scorePos = new Vector2(bounds.X + bounds.Width / 2 - 250, bounds.Y + 200);
            menuButton = new Button(new Rectangle(centerLineX - 230, bounds.Y + bounds.Height - 200, 120, 100), "Menu", menuFont, Alignment.Left);
            retryButton = new Button(new Rectangle(centerLineX - 60, bounds.Y + bounds.Height - 200, 120, 100), "Retry", menuFont, Alignment.Left);
            nextButton = new Button(new Rectangle(centerLineX + 110, bounds.Y + bounds.Height - 200, 120, 100), "Next", menuFont, Alignment.Left);
            starFull = Global.Content.Load<Texture2D>("GUI/StarFull");
            starEmpty = Global.Content.Load<Texture2D>("GUI/StarEmpty");
        }

        /// <Summary>
        /// public void Update():
        ///     Update the Level Finisher per frame.
        /// </Summary>
        public void Update()
        {
            menuButton.Update();
            retryButton.Update();
            nextButton.Update();
            UpdateMouse();

            percentage = (float)PlayerStats.GoodBlocks / (float)screen.Level.BlockData.X;
            if (percentage == 1f) star = 3;
            if (percentage >= 0.5f && percentage < 1f) star = 2;
            if (percentage >= 0f && percentage < 0.2f) star = 0;
            if (percentage >= 0.2f && percentage < 0.5f) star = 1;

            if (menuButton.Clicked())
            {
                if (PlayerStats.GetStars(screen.Level) < star)
                {
                    PlayerStats.SetStars(screen.Level, star);
                }
                SetPlayerStats();
                SoundPlayer.PlaySong("menuMusic");
                game.StartScreen();
            }

            if (retryButton.Clicked())
            {
                SetPlayerStats();
                if (AllLevels.CurrentLevel.ID != -1) game.StartGame(AllLevels.GetLevel(AllLevels.CurrentLevel.ID));
            }

            if (nextButton.Clicked())
            {
                if (PlayerStats.GetStars(screen.Level) < star)
                {
                    PlayerStats.SetStars(screen.Level, star);
                }
                SetPlayerStats();
                try
                {
                    game.StartGame(AllLevels.GetLevel(AllLevels.CurrentLevel.ID + 1));
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    game.StartScreen();
                    SoundPlayer.PlaySong("menuMusic");
                }
            }
        }


        /// <Summary>
        /// private async void SetPlayerStats():
        ///     Sets the new Player Stats after completing a level. OMITTED AWIT.
        /// </Summary>
        private void SetPlayerStats()
        {
            PlayerStats.TotalBlocksDestroyed += PlayerStats.GoodBlocks;
            AllLevels.ResetLevels();
            PlayerStats.ResetLevelStats();
            XMLWriter.WritePlayerData("playerData");
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Panel per frame.
        /// </Summary>
        public void Draw()
        {
            Global.SpriteBatch.Draw(WhiteTexture, new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Color.Black * 0.3f);
            Global.SpriteBatch.Draw(WhiteTexture, new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height), Color.DarkOrange);
            DrawString(menuFont, "Score:  " + PlayerStats.GoodBlocks, new Rectangle(bounds.X, bounds.Y + 50, bounds.Width, bounds.Height), Alignment.Center, Color.White);
            DrawString(HUDFont, "Level Clear", new Rectangle(bounds.X, bounds.Y+20, bounds.Width, bounds.Height), Alignment.Top, Color.White);
            DrawString(menuFont, "Total Blocks: " + (PlayerStats.TotalBlocksDestroyed + PlayerStats.GoodBlocks), new Rectangle(bounds.X, bounds.Y + 200, bounds.Width, bounds.Height), Alignment.Top, Color.White);

            float percentage = (float)PlayerStats.GoodBlocks / (float)screen.Level.BlockData.X;

            if (percentage == 1f)
            {
                DrawString(HUDFont, "PERFECT", new Rectangle(bounds.X, bounds.Y + 100, bounds.Width, bounds.Height), Alignment.Top, Color.White);
            }
            if (percentage >= 0.5f && percentage < 1f)
            {
                DrawString(HUDFont, "50%-90%", new Rectangle(bounds.X, bounds.Y + 100, bounds.Width, bounds.Height), Alignment.Top, Color.White);
            }
            if (percentage >= 0f && percentage < 0.2f)
            {
                DrawString(HUDFont, "0 stars", new Rectangle(bounds.X, bounds.Y + 100, bounds.Width, bounds.Height), Alignment.Top, Color.White);
            }
            if (percentage >= 0.2f && percentage < 0.5f)
            {
                DrawString(HUDFont, "1 stars", new Rectangle(bounds.X, bounds.Y + 100, bounds.Width, bounds.Height), Alignment.Top, Color.White);
            }

            for (int i = 0; i < star; i++)
            {
                Global.SpriteBatch.Draw(starFull, new Rectangle(bounds.X + (bounds.Width / 2) - 53 + (35 * i), bounds.Y + (bounds.Height / 2) - 35, 35, 31), Color.White);
            }
            for (int i = star; i < 3; i++)
            {
                Global.SpriteBatch.Draw(starEmpty, new Rectangle(bounds.X + (bounds.Width / 2) - 53 + (35 * i), bounds.Y + (bounds.Height / 2) - 35, 35, 31), Color.White);
            }
            menuButton.Draw();
            retryButton.Draw();
            nextButton.Draw();
        }
    }
}
