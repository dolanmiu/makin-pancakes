﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Option Screen Class: Change the options of the game.
    /// </Summary>
    class OptionsScreen : Screen
    {
        private Button backButton;
        private Button soundButton;
        private Button musicButton;
        private Button saveDataButton;
        private Button creditsButton;

        /// <Summary>
        /// public OptionsScreen(Game1 game):
        ///     Option screen constructor.
        /// </Summary>
        public OptionsScreen(Game1 game) : base(game)
        {
            int centerLineX = (int)Global.ScreenSize.X / 2;
            backButton = new Button(new Rectangle(centerLineX, (int)Global.ScreenSize.Y - 5*Global.ButtonSep, 100, 50), "Back", menuFont, Alignment.Center);
            soundButton = new Button(new Rectangle(centerLineX - Global.ButtonSep, (int)Global.ScreenSize.Y / 2 - 60, 120, 50), "Sound", menuFont, Alignment.Right);
            musicButton = new Button(new Rectangle(centerLineX + Global.ButtonSep, (int)Global.ScreenSize.Y / 2 - 60, 120, 50), "Music", menuFont, Alignment.Left);
            saveDataButton = new Button(new Rectangle(centerLineX, musicButton.Bounds.Y + musicButton.Bounds.Height + Global.ButtonSep, 250, 50), "Save Data", menuFont, Alignment.Center);
            creditsButton = new Button(new Rectangle(centerLineX, saveDataButton.Bounds.Y + saveDataButton.Bounds.Height + Global.ButtonSep, 150, 50), "Credits", menuFont, Alignment.Center);
            if (SoundPlayer.SongMuted)
            {
                musicButton.Text = "Muted";
            }
            if (SoundPlayer.SoundFXMuted)
            {
                soundButton.Text = "Muted";
            }
        }

        /// <Summary>
        /// public void Update():
        ///     Updates the Screen per frame.
        /// </Summary>
        public void Update()
        {
            base.BaseUpdate();
            backButton.Update();
            soundButton.Update();
            musicButton.Update();
            saveDataButton.Update();
            creditsButton.Update();

            if (keyboardState.IsKeyDown(Keys.Back) && lastKeyboardState.IsKeyUp(Keys.Back))
            {
                game.StartScreen();
            }
            lastKeyboardState = keyboardState;

            if (backButton.Clicked()) game.StartScreen();
            if (soundButton.Clicked()) {
                if (SoundPlayer.SoundFXMuted)
                {
                    SoundPlayer.SoundFXMuted = false;
                    soundButton.Text = "Sound";
                }
                else
                {
                    SoundPlayer.SoundFXMuted = true;
                    soundButton.Text = "Muted";
                }
            }
            if (musicButton.Clicked()) 
            {
                if (SoundPlayer.SongMuted)
                {
                    SoundPlayer.SongMuted = false;
                    SoundPlayer.PlaySong("backgroundMusic");
                    musicButton.Text = "Music";
                }
                else
                {
                    SoundPlayer.SongMuted = true;
                    SoundPlayer.StopSong();
                    musicButton.Text = "Muted";
                }
            }
            if (saveDataButton.Clicked()) { }
            if (creditsButton.Clicked()) game.ShowCredits();
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Screen per frame.
        /// </Summary>
        public void Draw(Color[] skyColours)
        {
            base.BaseDraw("Options", skyColours);
            Global.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            //Global.SpriteBatch.DrawString(font, buffer.ToString(), new Vector2((int)Global.ScreenSize.X / 2 - 300, 300), Color.Black);
            backButton.Draw();
            soundButton.Draw();
            musicButton.Draw();
            saveDataButton.Draw();
            creditsButton.Draw();
            Global.SpriteBatch.End();
        }
    }
}
