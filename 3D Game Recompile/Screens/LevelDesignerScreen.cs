﻿using _3D_Game_Recompile.PlayerData;
using _3D_Game_Recompile.Screens.Elements;
using _3D_Game_Recompile.Screens.Level_Designer;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Level Designer Screen Class: Screen used to design levels.
    /// </Summary>
    public class LevelDesignerScreen : Screen
    {
        private Button addRow, deleteRow, addColumn, deleteColumn;
        private Button newLevel;
        private Button addWave, deleteWave;
        private Button backButton;
        private Button renderButton, saveButton;
        //private List<Button> waveButtons;
        private ScrollingList waveList;
        private List<TileIcon[,]> tileSets;

        private Level currentLevel;

        private TestState testState;
        private TestingStage testLevel;
        //private TileIcon[,] tiles;
        private int currentTilesIndex;

        /// <Summary>
        /// public LevelDesignerScreen(Game1 game):
        ///     Level Designer screen constructor.
        /// </Summary>
        public LevelDesignerScreen(Game1 game) : base(game)
        {
            SoundPlayer.PlaySong("levelDesignerMusic");
            currentLevel = new Level();

            int centerLineY = (int)Global.ScreenSize.Y / 2;
            int centerLineX = (int)Global.ScreenSize.X / 2;

            newLevel = new Button(new Rectangle(0, 50, 230, 50), "Restart", menuFont, Alignment.Left);
            addWave = new Button(new Rectangle(0, 150, 230, 50), "New Wave", menuFont, Alignment.Left);
            deleteWave = new Button(new Rectangle(0, 250, 230, 50), "Delete Wave", menuFont, Alignment.Left);

            addRow = new Button(new Rectangle(centerLineX + 35, 0, 50, 50), "+", menuFont, Alignment.Left);
            deleteRow = new Button(new Rectangle(centerLineX - 35, 0, 50, 50), "-", menuFont, Alignment.Left);
            addColumn = new Button(new Rectangle(centerLineX * 2, centerLineY - 35, 50, 50), "+", menuFont, Alignment.Right);
            deleteColumn = new Button(new Rectangle(centerLineX * 2, centerLineY + 35, 50, 50), "-", menuFont, Alignment.Right);

            backButton = new Button(new Rectangle(centerLineX, centerLineY * 2 - 50, 130, 50), "Back", menuFont, Alignment.Center);
            renderButton = new Button(new Rectangle(centerLineX * 2, centerLineY * 2 - 65, 130, 50), "Try it!", menuFont, Alignment.Right);

            saveButton = new Button(new Rectangle(centerLineX * 2 - renderButton.Bounds.Width - Global.ButtonSep, centerLineY * 2 - 65, 130, 50), "Save", menuFont, Alignment.Right);

            tileSets = new List<TileIcon[,]>();
            waveList = new ScrollingList(0, 400, 230, (int)(Global.ScreenSize.Y - 400 - 50*Global.buttonScale));
            CreateTileSet();
        }

        private void CreateTileSet()
        {
            TileIcon[,] tiles = new TileIcon[4, 5];
            tiles = InitTiles(tiles);
            tileSets.Add(tiles);
            waveList.Add();
            waveList.Index = tileSets.Count - 1;
        }

        private void DestroyTileSet()
        {
            if (tileSets.Count > 1)
            {
                tileSets[currentTilesIndex] = null;
                tileSets = Global.RefactorList<TileIcon[,]>(tileSets);
                waveList.Recalculate("Wave", tileSets.Count);
                currentTilesIndex = tileSets.Count - 1;
            }
        }

        /// <Summary>
        /// public void Update(GameTime gameTime):
        ///     Updates the Screen per frame.
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            base.BaseUpdate();

            switch (testState)
            {
                case TestState.DesignMode:
                    UpdateDesigner();
                    break;
                case TestState.TestMode:
                    if (testLevel != null)
                        testLevel.Update(gameTime, this);
                    break;
            }
        }

        /// <Summary>
        /// private void UpdateDesigner():
        ///     Updates the Designer part of the screen
        /// </Summary>
        private void UpdateDesigner()
        {
            addRow.Update();
            addColumn.Update();
            addWave.Update();
            newLevel.Update();
            deleteRow.Update();
            deleteColumn.Update();
            addWave.Update();
            deleteWave.Update();
            backButton.Update();
            renderButton.Update();
            saveButton.Update();

            for (int i = 0; i < tileSets[currentTilesIndex].GetLength(0); i++)
            {
                for (int j = 0; j < tileSets[currentTilesIndex].GetLength(1); j++)
                {
                    tileSets[currentTilesIndex][i, j].Update();
                }
            }

            currentTilesIndex = waveList.Update();


            if (renderButton.Clicked())
            {
                RenderTiles();
            }

            if (newLevel.Clicked())
            {
                tileSets.Clear();
                waveList.Clear();
                currentTilesIndex = 0;
                CreateTileSet();
            }

            if (addWave.Clicked())
            {
                CreateTileSet();
            }

            if (deleteWave.Clicked())
            {
                DestroyTileSet();
            }

            if (addRow.Clicked())
            {
                for (int i = 0; i < tileSets.Count; i++)
                {
                    tileSets[i] = EditRowColumn(tileSets[i], 0, 1);
                }
                //RepositionTiles();
            }

            if (deleteRow.Clicked())
            {
                for (int i = 0; i < tileSets.Count; i++)
                {
                    if (tileSets[currentTilesIndex].GetLength(1) > 1)
                    {
                        tileSets[i] = EditRowColumn(tileSets[i], 0, -1);
                    }
                }
            }

            if (addColumn.Clicked())
            {
                for (int i = 0; i < tileSets.Count; i++)
                {
                    tileSets[i] = EditRowColumn(tileSets[i], 1, 0);
                }
            }

            if (deleteColumn.Clicked())
            {
                for (int i = 0; i < tileSets.Count; i++)
                {
                    if (tileSets[currentTilesIndex].GetLength(0) > 1)
                    {
                        tileSets[i] = EditRowColumn(tileSets[i], -1, 0);
                    }
                }
            }

            if (backButton.Clicked())
            {
                game.StartScreen();
                SoundPlayer.PlaySong("menuMusic");
            }
            if (saveButton.Clicked())
            {
                GamePage.saveLevel.Visibility = Windows.UI.Xaml.Visibility.Visible;
                GamePage.saveLevel.Screen = this;
            }
        }

        /// <Summary>
        /// public void SaveLevel(String levelName):
        ///     Saves the created level.
        /// </Summary>
        public void SaveLevel(String levelName)
        {
            currentLevel.Width = tileSets[currentTilesIndex].GetLength(0);
            currentLevel.Height = tileSets[currentTilesIndex].GetLength(1);
            for (int i = 0; i < tileSets.Count; i++)
            {
                currentLevel.AddWave(CreateWaveString(tileSets[i]));
            }
            XMLWriter.WriteLevelData(levelName, currentLevel);
        }

        /// <Summary>
        /// private void RepositionTiles():
        ///     Reposition the tiles to match the aspect ratio and screen resolution.
        /// </Summary>
        private void RepositionTiles()
        {
            int tilesX = tileSets[currentTilesIndex].GetLength(0);
            int tilesY = tileSets[currentTilesIndex].GetLength(1);

            int centerLineY = (int)Global.ScreenSize.Y / 2;
            int centerLineX = (int)Global.ScreenSize.X / 2;

            int tileWidth = (((int)Global.ScreenSize.X / 4) * 3) / (tilesX + 10);
            Vector2 size = FindTotalSizeOfGrid();

            for (int i = 0; i < tileSets[currentTilesIndex].GetLength(0); i++)
            {
                for (int j = 0; j < tileSets[currentTilesIndex].GetLength(1); j++)
                {
                    //tiles[i, j].Width = tileWidth;
                    tileSets[currentTilesIndex][i, j].Position = new Vector2((int)Global.ScreenSize.X / 8, (int)Global.ScreenSize.Y / 8); //new Vector2(centerLineX - (size.X / 2), centerLineY - (size.Y / 2));
                }
            }
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Screen per frame.
        /// </Summary>
        public void Draw()
        {
            switch (testState)
            {
                case TestState.DesignMode:
                    DrawDesigner();
                    break;
                case TestState.TestMode:
                    if (testLevel != null)
                        testLevel.Draw();
                    break;
            }
        }

        /// <Summary>
        /// private void DrawDesigner():
        ///     Draws the Designer part of the screen.
        /// </Summary>
        private void DrawDesigner()
        {
            Global.SpriteBatch.Begin();
            Global.GraphicsDevice.Clear(Color.White);
            for (int i = 0; i < tileSets[currentTilesIndex].GetLength(0); i++)
            {
                for (int j = 0; j < tileSets[currentTilesIndex].GetLength(1); j++)
                {
                    tileSets[currentTilesIndex][i, j].Draw();
                }
            }

            waveList.Draw();

            #region Draw Buttons
            addRow.Draw();
            addColumn.Draw();
            newLevel.Draw();
            addWave.Draw();
            newLevel.Draw();
            deleteRow.Draw();
            deleteColumn.Draw();
            addWave.Draw();
            deleteWave.Draw();
            backButton.Draw();
            renderButton.Draw();
            saveButton.Draw();
            #endregion
            Global.SpriteBatch.End();
        }

        /// <Summary>
        /// private void RenderTiles():
        ///     Puts the screen into TestMode and renders the game.
        /// </Summary>
        private void RenderTiles()
        {
            currentLevel.Width = tileSets[currentTilesIndex].GetLength(0);
            currentLevel.Height = tileSets[currentTilesIndex].GetLength(1);
            for (int i = 0; i < tileSets.Count; i++)
            {
                currentLevel.AddWave(CreateWaveString(tileSets[i]));
            }
            testState = TestState.TestMode;
            testLevel = new TestingStage(game, currentLevel);
        }

        /// <Summary>
        /// private string CreateWaveString():
        ///     Cretes a string representation of the Wave in a level to be used in XML.
        /// </Summary>
        private string CreateWaveString(TileIcon[,] tiles)
        {
            String waveString = "";
            for (int j = 0; j < tiles.GetLength(0); j++)
            {
                for (int i = 0; i < tiles.GetLength(1); i++)
                {
                    switch (tiles[j, i].Type)
                    {
                        case TileIcon.TileType.Empty:
                            waveString += "0";
                            break;
                        case TileIcon.TileType.NormalCube:
                            waveString += "1";
                            break;
                        case TileIcon.TileType.MultiplyCube:
                            waveString += "2";
                            break;
                        case TileIcon.TileType.ForbiddenCube:
                            waveString += "3";
                            break;
                    }
                }
                waveString += ",";
            }
            return waveString;
        }

        /// <Summary>
        /// private void InitTiles():
        ///     Inits the Tiles.
        /// </Summary>
        private TileIcon[,] InitTiles(TileIcon[,] tiles)
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    Vector2 startPos = new Vector2((int)Global.ScreenSize.X / 8, (int)Global.ScreenSize.Y / 8);
                    TileIcon tile = new TileIcon(new Rectangle((int)(100 * Global.buttonScale * i), (int)(100 * Global.buttonScale * j), 90, 90));
                    tile.Position = startPos;
                    tiles[i, j] = tile;
                }
            }
            return tiles;
        }

        /// <Summary>
        /// private Vector2 FindTotalSizeOfGrid():
        ///     Finds the size of the grid.
        /// </Summary>
        private Vector2 FindTotalSizeOfGrid()
        {
            int tilesX = tileSets[currentTilesIndex].GetLength(0);
            int tilesY = tileSets[currentTilesIndex].GetLength(1);

            int unitSize = tileSets[currentTilesIndex][0, 0].Bounds.Width + 10;
            int tilesWidth = tilesX * unitSize;
            int tilesHeight = tilesY * unitSize;

            return new Vector2(tilesWidth, tilesHeight);
        }

        /// <Summary>
        /// private TileIcon[,] EditRowColumn(int i, int j):
        ///     Adds or deletes rows of the grid.
        /// </Summary>
        private TileIcon[,] EditRowColumn(TileIcon[,] tiles, int i, int j)
        {
            int newWidth = tiles.GetLength(0) + i;
            int newHeight = tiles.GetLength(1) + j;
            tiles = Global.ResizeArray<TileIcon>(tiles, newWidth, newHeight);

            for (int k = 0; k < tiles.GetLength(0); k++)
            {
                for (int l = 0; l < tiles.GetLength(1); l++)
                {
                    Vector2 startPos = new Vector2((int)Global.ScreenSize.X / 8, (int)Global.ScreenSize.Y / 8);
                    if (k == newWidth - 1) tiles[k, l] = new TileIcon(new Rectangle((int)(100 * Global.buttonScale * k), (int)(100 * Global.buttonScale * l), 90, 90));
                    if (l == newHeight - 1) tiles[k, l] = new TileIcon(new Rectangle((int)(100 * Global.buttonScale * k), (int)(100 * Global.buttonScale * l), 90, 90));
                    tiles[k, l].Position = startPos;
                }
            }
            return tiles;
        }

        #region Properties
        public TestState State
        {
            get { return testState; }
            set { testState = value; }
        }
        #endregion
    }
}
