﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Settings Widget Class: A settings menu.
    /// </Summary>
    public class SettingsWidget : Panel
    {
        Rectangle bounds;
        PlayScreen screen;
        private SpriteFont HUDFont, menuFont;
        private Button continueButton, skipButton, retryButton, levelSelectButton, startScreenButton;
        private Game1 game;

        /// <Summary>
        /// public SettingsWidget(Game1 game, PlayScreen screen, Rectangle bounds):
        ///     Setting Widget constructor.
        /// </Summary>
        public SettingsWidget(Game1 game, PlayScreen screen, Rectangle bounds) : base()
        {
            this.bounds = bounds;
            this.screen = screen;
            this.game = game;
            int centerLineX = bounds.X + bounds.Width / 2;
            HUDFont = Global.Content.Load<SpriteFont>("Fonts/LogoFont");
            menuFont = Global.Content.Load<SpriteFont>("Fonts/MenuFont");

            continueButton = new Button(new Rectangle(centerLineX, bounds.Y + (int)(120*Global.buttonScale), 260, 50), "Continue", menuFont, Alignment.Center);
            skipButton = new Button(new Rectangle(centerLineX, continueButton.Bounds.Y + continueButton.Bounds.Height + Global.ButtonSep, 260, 50), "Skip Level", menuFont, Alignment.Center);
            retryButton = new Button(new Rectangle(centerLineX, skipButton.Bounds.Y + skipButton.Bounds.Height + Global.ButtonSep, 260, 50), "Retry Level", menuFont, Alignment.Center);
            levelSelectButton = new Button(new Rectangle(centerLineX, retryButton.Bounds.Y + retryButton.Bounds.Height + Global.ButtonSep, 260, 50), "Level Select", menuFont, Alignment.Center);
            startScreenButton = new Button(new Rectangle(centerLineX, levelSelectButton.Bounds.Y + levelSelectButton.Bounds.Height + Global.ButtonSep, 260, 50), "Main Menu", menuFont, Alignment.Center);
        }

        /// <Summary>
        /// public void Update():
        ///     Updates the Widget per frame.
        /// </Summary>
        public void Update()
        {
            continueButton.Update();
            skipButton.Update();
            retryButton.Update();
            levelSelectButton.Update();
            startScreenButton.Update();

            if (continueButton.Clicked()) screen.InGameState = InGameState.StageInProgress;
            if (skipButton.Clicked())
            {
                AllLevels.ResetLevels();
                PlayerStats.ResetLevelStats();
                try
                {
                    game.StartGame(AllLevels.GetLevel(AllLevels.CurrentLevel.ID + 1));
                }
                catch (System.ArgumentOutOfRangeException)
                {
                    game.StartScreen();
                    SoundPlayer.PlaySong("menuMusic");
                }
            }

            if (retryButton.Clicked())
            {
                AllLevels.ResetLevels();
                PlayerStats.ResetLevelStats();
                game.StartGame(AllLevels.GetLevel(AllLevels.CurrentLevel.ID));
            }

            if (levelSelectButton.Clicked())
            {
                AllLevels.ResetLevels();
                PlayerStats.ResetLevelStats();
                game.LevelSelect();
                SoundPlayer.PlaySong("menuMusic");
            }

            if (startScreenButton.Clicked())
            {
                AllLevels.ResetLevels();
                PlayerStats.ResetLevelStats();
                game.StartScreen();
                SoundPlayer.PlaySong("menuMusic");
            }
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Widget per frame.
        /// </Summary>
        public void Draw()
        {
            Global.SpriteBatch.Draw(WhiteTexture, new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Color.Black * 0.3f);
            Global.SpriteBatch.Draw(WhiteTexture, new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height), Color.DarkOrange);
            DrawString(HUDFont, "Paused", new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height), Alignment.Top, Color.White);

            continueButton.Draw();
            skipButton.Draw();
            retryButton.Draw();
            levelSelectButton.Draw();
            startScreenButton.Draw();
        }
    }
}
