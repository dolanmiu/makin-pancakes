﻿using _3D_Game_Recompile.Screens.Level_Designer;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Test Finisher Class: A Panel that shows up after a Level is complete.
    /// </Summary>
    class TestFinisher : Panel
    {
        private Rectangle bounds;
        private TestingStage screen;
        private SpriteFont menuFont;
        private Button backButton;

        /// <Summary>
        /// public TestFinisher(Game1 game, TestingStage screen, Rectangle bounds):
        ///     Test Finisher constructor.
        /// </Summary>
        public TestFinisher(Game1 game, TestingStage screen, Rectangle bounds)
            : base()
        {
            this.bounds = bounds;
            this.screen = screen;
            int centerLineX = bounds.X + bounds.Width / 2;
            menuFont = Global.Content.Load<SpriteFont>("Fonts/MenuFont");
            backButton = new Button(new Rectangle(centerLineX - 60, bounds.Y + bounds.Height / 2, 120, 100), "Back", menuFont, Alignment.Center);
        }

        /// <Summary>
        /// public void Update(LevelDesignerScreen screen):
        ///     Updates the test finisher per frame.
        /// </Summary>
        public void Update(LevelDesignerScreen screen)
        {
            backButton.Update();
            UpdateMouse();

            if (backButton.Clicked()) screen.State = TestState.DesignMode;
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the test finisher per frame.
        /// </Summary>
        public void Draw()
        {
            Global.SpriteBatch.Draw(WhiteTexture, new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Color.Black * 0.3f);
            Global.SpriteBatch.Draw(WhiteTexture, new Rectangle(bounds.X, bounds.Y, bounds.Width, bounds.Height), Color.DarkOrange);
            backButton.Draw();
        }
    }
}
