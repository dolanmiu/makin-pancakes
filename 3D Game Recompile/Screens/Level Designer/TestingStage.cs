﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace _3D_Game_Recompile.Screens.Level_Designer
{
    /// <Summary>
    /// Testing Stage Class: Testing the level created using the level creator.
    /// </Summary>
    class TestingStage : PlayScreen
    {
        private TestFinisher testFinisher;

        /// <Summary>
        /// public TestingStage(Game1 game, Level level):
        ///     Testing Stage constructor.
        /// </Summary>
        public TestingStage(Game1 game, Level level) : base(game, level)
        {
            testFinisher = new TestFinisher(game, this, new Rectangle((int)(int)Global.ScreenSize.X / 4, (int)(int)Global.ScreenSize.Y / 4, (int)(int)Global.ScreenSize.X / 2, (int)Global.ScreenSize.Y / 2));
        }

        /// <Summary>
        /// public void Update(GameTime gameTime, LevelDesignerScreen screen):
        ///     Updates the Testing Stage per frame.
        /// </Summary>
        public void Update(GameTime gameTime, LevelDesignerScreen screen)
        {
            base.PlayUpdate(gameTime);

            switch (inGameState)
            {
                case InGameState.FadeIn:
                    if (fadeAlpha <= 0) inGameState = InGameState.StageInProgress;
                    break;
                case InGameState.PreStage:
                    ProcessKeys();
                    stageClearTimer.Update(gameTime);
                    if (stageClearTimer.Ticking) inGameState = InGameState.StageInProgress;
                    break;
                case InGameState.StageInProgress:
                    ProcessKeys();
                    if (playGrid.CurrentWave != null) if (playGrid.CurrentWave.Cleared) inGameState = InGameState.RowJudgement;// maybe bad idea?
                    break;
                case InGameState.WaveClear:
                    stageClearTimer.Update(gameTime);
                    ProcessKeys();
                    if (stageClearTimer.Ticking)
                    {
                        Boolean noWavesLeft = playGrid.LoadNextWave(1);
                        if (!noWavesLeft) { inGameState = InGameState.LevelClear; }
                        else { inGameState = InGameState.PreStage; }
                    }
                    break;
                case InGameState.RowJudgement:
                    stageClearTimer.Update(gameTime);
                    if (stageClearTimer.Ticking)
                    {
                        Extend(PlayerStats.AddNextRows);
                        PlayerStats.AddNextRows = 1;
                        inGameState = InGameState.WaveClear;
                    }

                    break;
                case InGameState.LevelClear:
                    testFinisher.Update(screen);
                    break;
                case InGameState.Paused:
                    //settingsWidget.Update();
                    break;
            }
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Testing Stage per frame.
        /// </Summary>
        public void Draw()
        {
            Global.GraphicsDevice.Clear(Color.CornflowerBlue);
            Global.GraphicsDevice.BlendState = BlendState.Opaque;
            Global.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            Global.GraphicsDevice.Clear(ClearOptions.DepthBuffer | ClearOptions.Target, Color.Black, 1.0f, 0);
            Matrix viewMatrix = camera.ViewMatrix; //use your own camere class here
            base.PlayDraw(viewMatrix);

            Global.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            switch (inGameState)
            {
                case InGameState.FadeIn:
                    Global.SpriteBatch.Draw(WhiteTexture, new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Color.Black * fadeAlpha);
                    fadeAlpha -= 0.04f;
                    break;
                case InGameState.PreStage:
                    DrawString(HUDFont, "WAVE " + (level.CurrentWaveIndex + 1), new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Alignment.Center, Color.White);
                    break;
                case InGameState.StageInProgress:
                    break;
                case InGameState.WaveClear:
                    DrawString(HUDFont, "Wave Clear!", new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Alignment.Center, Color.White);
                    break;
                case InGameState.LevelClear:
                    testFinisher.Draw();
                    break;
                case InGameState.Paused:
                    //settingsWidget.Draw();
                    break;
            }
            Global.SpriteBatch.End();
        }

        #region Properties
        #endregion
    }
}
