﻿using _3D_Game_Recompile.Screens.Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace _3D_Game_Recompile.Screens.Level_Designer
{
    /// <Summary>
    /// Tile Icon Class: A 2D tile used in the level creator to represent the 3D tile in game.
    /// </Summary>
    class TileIcon : Clickable
    {
        //private int tileType;
        private TileType tileType;
        private Vector2 startPos;
        private Vector2 previousPos;
        public enum TileType { Empty, NormalCube, MultiplyCube, ForbiddenCube }
        private int size;

        /// <Summary>
        /// public TileIcon(Rectangle bounds):
        ///     Tile Icon constructor.
        /// </Summary>
        public TileIcon(Rectangle bounds) : base(bounds, Alignment.Left)
        {
            tileType = 0;
            //AdjustPosition();
        }

        /// <Summary>
        /// public void AdjustPosition():
        ///     Adjusts the position of the tile Icon.
        /// </Summary>
        public void AdjustPosition() 
        {
            bounds.X = bounds.X - (int)previousPos.X + (int)startPos.X;
            bounds.Y = bounds.Y - (int)previousPos.Y + (int)startPos.Y;
            previousPos = startPos;
        }

        /// <Summary>
        /// new public void Update():
        ///     Updates the Tile Icon per frame.
        /// </Summary>
        new public void Update()
        {
            switch (tileType)
            {
                case TileType.Empty:
                    break;
                case TileType.NormalCube:
                    break;
                case TileType.MultiplyCube:
                    break;
                case TileType.ForbiddenCube:
                    break;
            }

            UpdateMouse();
            if (DetectBounds(bounds))
            {
                hoverOver = true;
            }
            else
            {
                hoverOver = false;
            }

            if (Clicked()) ClickActions();
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Tile Icon per frame.
        /// </Summary>
        public void Draw()
        {
            switch (tileType)
            {
                case TileType.Empty:
                    Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.AliceBlue);
                    break;
                case TileType.NormalCube:
                    Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.Wheat);
                    break;
                case TileType.MultiplyCube:
                    Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.LawnGreen);
                    break;
                case TileType.ForbiddenCube:
                    Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.DarkGray);
                    break;
            }
        }

        /// <Summary>
        /// public void ClickActions():
        ///     What happens when clicked.
        /// </Summary>
        public void ClickActions()
        {
            if (tileType == TileType.Empty)
            {
                tileType = TileType.NormalCube;
                return;
            }
            if (tileType == TileType.NormalCube)
            {
                tileType = TileType.MultiplyCube;
                return;
            }
            if (tileType == TileType.MultiplyCube)
            {
                tileType = TileType.ForbiddenCube; 
                return;
            }
            if (tileType == TileType.ForbiddenCube)
            {
                tileType = TileType.Empty;
                return;
            }

        }

        #region Properties
        public int Widhtemp
        {
            get { return size; }
            set { 
                size = value;
                Rectangle tempBounds = bounds;
                bounds = new Rectangle(tempBounds.X, tempBounds.Y, size, size);
            }
        }

        public Vector2 Position
        {
            get { return startPos; }
            set
            {
                startPos = value;
                AdjustPosition();
            }
        }

        public TileType Type
        {
            get { return tileType; }
        }
        #endregion
    }
}
