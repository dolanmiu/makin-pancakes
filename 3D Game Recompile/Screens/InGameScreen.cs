﻿using _3D_Game_Recompile.Entities.Particles;
using _3D_Game_Recompile.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// In Game Screen Class: What is showed in game play.
    /// </Summary>
    public class InGameScreen : PlayScreen
    {
        private LevelFinisher levelFinisher;
        private SettingsWidget settingsWidget;
        private SoundEffect perfectSound;

        RenderTarget2D leftEye;
        RenderTarget2D rightEye;
        //Effect anaglyphEffect;
        QuadRenderer quad;
        float ammount = 0.05f;

        //ParticleSystem smokePlumeParticles;
        //SpriteBatchRenderer particleRenderer;
        //ParticleEffect particleEffect;

        //BoundingBox playGridBoundingBox;
        //StorageFile file = null;
        //byte[] shaderBytes;
        //Effect customEffect;

        Billboard billboardTest;
        Waterfall waterFallTest;

        /// <Summary>
        /// public InGameScreen(Game1 game, Level level):
        ///     In Game Screen constructor.
        /// </Summary>
        public InGameScreen(Game1 game, Level level) : base(game, level)
        {

            GamePage.ClearUI();
            GamePage.ShowInGameUI();
            levelFinisher = new LevelFinisher(game, this, new Rectangle((int)Global.ScreenSize.X / 4, (int)Global.ScreenSize.Y / 4, (int)Global.ScreenSize.X / 2, (int)Global.ScreenSize.Y / 2));
            settingsWidget = new SettingsWidget(game, this, new Rectangle((int)Global.ScreenSize.X / 3, (int)Global.ScreenSize.Y / 4, (int)Global.ScreenSize.X / 3, (int)Global.ScreenSize.Y / 2));

            //smokePlumeParticles = new SmokePlumeParticleSystem(game, Global.Content);
            //smokePlumeParticles.DrawOrder = 100;
            //game.Components.Add(smokePlumeParticles);

            //Load Content
            //ConvertImagetoByte();
            //customEffect = new Effect(Global.GraphicsDevice, shaderBytes);
            leftEye = new RenderTarget2D(Global.GraphicsDevice, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y);
            rightEye = new RenderTarget2D(Global.GraphicsDevice, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y);
            //anaglyphEffect = Global.Content.Load<Effect>("Effect1");
            quad = new QuadRenderer(Global.GraphicsDevice);
            billboardTest = new Billboard(3, 3, BillBoardType.Cylindrical);
            billboardTest.Load(camera, "Tiles/GroundTile");
            waterFallTest = new Waterfall(new Vector3(-3,0,9), new Vector3(-11,0,9), new Vector2(0.05f, 0.1f), -0.1f);
            waterFallTest.Load(camera);
            perfectSound = Global.Content.Load<SoundEffect>("Sounds/sparkle");

            /*particleEffect = new ParticleEffect();
            {
                new SphereEmitter
                {
                    Budget = 1000,
                    Term = 3f,
                    Name = "FirstEmitter",
                    BlendMode = EmitterBlendMode.Alpha,
                    ReleaseQuantity = 3,
                    ReleaseRotation = new VariableFloat { Value = 0f, Variation = MathHelper.Pi },
                    ReleaseScale = 64f,
                    ReleaseSpeed = new VariableFloat { Value = 64f, Variation = 32f },
                    ParticleTextureAssetPath = "Particle001",
                    Modifiers = new ModifierCollection
                    {
                        new OpacityInterpolator2
                        {
                            InitialOpacity = 1f,
                            FinalOpacity = 0f,
                        },
                        new ColourInterpolator2
                        {
                            InitialColour = Color.Tomato.ToVector3(),
                            FinalColour = Color.Lime.ToVector3(),
                        },
                    },
                };
            };

            particleRenderer.LoadContent(Global.Content);
            particleEffect = Global.Content.Load<ParticleEffect>("Particles/BasicExplosion");
            //particleEffect.LoadContent(Global.Content);
            //particleEffect.Initialise();*/
            //Content.Load<Effect>("testcompiled");
            //byte[] bytes = File.ReadAllBytes("C:\\MyPicture.jpg");
        }

        /*private async void ConvertImagetoByte()
        {
            StorageFile sf = await Windows.Storage.KnownFolders.DocumentsLibrary.GetFileAsync("testcompiled.mgfxo");
            file = sf;
            IRandomAccessStream fileStream = await sf.OpenAsync(FileAccessMode.Read);
            var reader = new Windows.Storage.Streams.DataReader(fileStream.GetInputStreamAt(0));
            await reader.LoadAsync((uint)fileStream.Size);

            byte[] pixels = new byte[fileStream.Size];

            reader.ReadBytes(pixels);

            shaderBytes = pixels;
        }*/

        /// <Summary>
        /// public void Update(GameTime gameTime):
        ///     Updates the Screen per frame.
        /// </Summary>
        public void Update(GameTime gameTime)
        {
            //UpdateSmokePlume();
            CheckPlayerBounds();
            switch (inGameState)
            {
                case InGameState.FadeIn:
                    if (fadeAlpha <= 0) inGameState = InGameState.StageInProgress;
                    ProcessKeys();
                    base.PlayUpdate(gameTime);
                    break;
                case InGameState.PreStage:
                    ProcessKeys();
                    stageClearTimer.Update(gameTime);
                    if (stageClearTimer.Ticking) inGameState = InGameState.StageInProgress;
                    base.PlayUpdate(gameTime);
                    break;
                case InGameState.StageInProgress:
                    ProcessKeys();
                    if (playGrid.CurrentWave != null) if (playGrid.CurrentWave.Cleared) inGameState = InGameState.RowJudgement;// maybe bad idea?
                    base.PlayUpdate(gameTime);
                    break;
                case InGameState.WaveClear:
                    stageClearTimer.Update(gameTime);
                    ProcessKeys();
                    base.PlayUpdate(gameTime);
                    if (stageClearTimer.Ticking)
                    {
                        PlayerStats.AddNextRows = 1;
                        PlayerStats.LifeLine = 3;
                        Boolean noWavesLeft = playGrid.LoadNextWave(1);
                        if (!noWavesLeft) { inGameState = InGameState.LevelClear; }
                        else { inGameState = InGameState.PreStage; }
                    }
                    break;
                case InGameState.RowJudgement:
                    base.PlayUpdate(gameTime);
                    stageClearTimer.Update(gameTime);
                    if (stageClearTimer.Ticking)
                    {
                        Extend(PlayerStats.AddNextRows);
                        inGameState = InGameState.WaveClear;
                    }

                    break;
                case InGameState.LevelClear:
                    base.PlayUpdate(gameTime);
                    levelFinisher.Update();
                    break;
                case InGameState.Paused:
                    settingsWidget.Update();
                    break;
                case InGameState.Falling:
                    base.PlayUpdate(gameTime);
                    break;
            }
            //billboardTest.Update(gameTime);
            //waterFallTest.Update(gameTime);
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Screen per frame.
        /// </Summary>
        public void Draw()
        {
            Global.GraphicsDevice.BlendState = BlendState.Opaque;
            //Global.GraphicsDevice.Clear(ClearOptions.DepthBuffer | ClearOptions.Target, Color.White, 1.0f, 0);
            Matrix viewMatrix = camera.ViewMatrix; //use your own camere class here
            //The vector pointing to the right (1,0,0) as seen from the view matrix is stored
            //in the view matrix as (M11, M21, M31)
            Vector3 right = new Vector3(viewMatrix.M11, viewMatrix.M21, viewMatrix.M31) * ammount; //ofset from the center for each eye
            Matrix viewMatrixLeft = Matrix.CreateLookAt(camera.Position - right, camera.Target, Vector3.Up);
            Matrix viewMatrixRight = Matrix.CreateLookAt(camera.Position + right, camera.Target, Vector3.Up);
            //effect.View = Matrix.CreateLookAt(new Vector3(0, 0, 10), new Vector3(0, 0, 0), Vector3.UnitY);

            base.PlayDraw(viewMatrix);
            Global.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            //waterFallTest.Draw(viewMatrix);
            Global.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            
            switch (inGameState)
            {
                case InGameState.FadeIn:
                    Global.SpriteBatch.Draw(WhiteTexture, new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Color.Black * fadeAlpha);
                    if (level.ID >= 0)
                    {
                        DrawString(logoFont, "Level " + (level.ID + 1), new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Alignment.Center, Color.White);
                    }
                    else
                    {
                        DrawString(logoFont, level.Name, new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Alignment.Center, Color.White);
                    }
                    fadeAlpha -= 0.01f;
                    break;
                case InGameState.PreStage:
                    DrawString(HUDFont, "WAVE " + (level.CurrentWaveIndex + 1), new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Alignment.Center, Color.White);
                    break;
                case InGameState.StageInProgress:
                    break;
                case InGameState.WaveClear:
                    if (PlayerStats.AddNextRows > 0)
                    {
                        DrawString(HUDFont, "PERFECT!", new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Alignment.Center, Color.White);
                        SoundPlayer.Play(perfectSound, 0.5f, 0, 0);
                    }
                    else
                    {
                        DrawString(HUDFont, "Wave Clear!", new Rectangle(0, 0, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Alignment.Center, Color.White);
                    }
                    break;
                case InGameState.LevelClear:
                    levelFinisher.Draw();
                    break;
                case InGameState.Paused:
                    settingsWidget.Draw();
                    break;
            }
            Global.SpriteBatch.End();
            /*Global.GraphicsDevice.SetRenderTarget(leftEye);
            DrawForEye(viewMatrixLeft, camera.ProjectionMatrix);

            Global.GraphicsDevice.SetRenderTarget(rightEye);
            DrawForEye(viewMatrixRight, camera.ProjectionMatrix);

            Global.GraphicsDevice.SetRenderTarget(null);

            anaglyphEffect.Techniques["Anaglyphs"].Passes[0].Apply();
            anaglyphEffect.Parameters["left"].SetValue(leftEye);
            anaglyphEffect.Parameters["right"].SetValue(rightEye);
            quad.DrawFullScreenQuad();
            */
            

            //smokePlumeParticles.SetCamera(Matrix.Identity, camera.ViewProjectionMatrix);
        }

        /*private void SetUpBoundingBoxes()
        {
            List<BoundingBox> bbList = new List<BoundingBox>();
            for (int i = 0; i < playGrid.Quads.GetLength(0); i++)
            {
                for (int j = 0; j < playGrid.Quads.GetLength(1); j++)
                {
                    Quad quad = playGrid.Quads[i, j];
                    if (quad.Cube != null)
                    {
                        int buildingHeight = 2;
                        Vector3[] buildingPoints = new Vector3[2];
                        buildingPoints[0] = quad.LowerRight;
                        buildingPoints[1] = new Vector3(quad.UpperLeft.X, buildingHeight, quad.UpperLeft.Z);
                        BoundingBox buildingBox = BoundingBox.CreateFromPoints(buildingPoints);
                        bbList.Add(buildingBox);
                    }
                }
            }
            cubeBoundingBoxes = bbList.ToArray();

            Vector3[] boundaryPoints = new Vector3[2];
            boundaryPoints[0] = new Vector3(0, 0, 0);
            boundaryPoints[1] = new Vector3(50, 20, -50);
            playGridBoundingBox = BoundingBox.CreateFromPoints(boundaryPoints);
        }*/

        private void DrawForEye(Matrix viewMatrix, Matrix projectionMatrix)
        {
            playGrid.Draw(viewMatrix);
            player.Draw(viewMatrix);
        }

        /// <Summary>
        /// private void CheckPlayerBounds():
        ///     Checks to see if the player is within the level.
        /// </Summary>
        private void CheckPlayerBounds()
        {
            if (player.Position.Z > stage.Height - 4 || player.Position.Z < -3.5f)
            {
                if (player.Position.Y > -10)
                {
                    player.FallDown();
                    inGameState = InGameState.Falling;
                }
                else
                {
                    inGameState = InGameState.LevelClear;
                }
            }

            if (player.Position.X > stage.Width / 2 || player.Position.X < - stage.Width / 2)
            {
                if (player.Position.Y > -10)
                {
                    player.FallDown();
                    inGameState = InGameState.Falling;
                }
                else
                {
                    inGameState = InGameState.LevelClear;
                }
            }
        }
    }
}
