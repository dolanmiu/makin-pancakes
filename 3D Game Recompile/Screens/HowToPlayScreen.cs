﻿using _3D_Game_Recompile.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Text;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// How to play Screen Class: A tutorial on how to play.
    /// </Summary>
    class HowToPlayScreen : Screen
    {
        private Button backButton;
        private Button moreInfoButton;
        private SpriteFont font;
        private Texture2D cheatSheet;

        private enum Page { CheatSheet, Lore }
        private Page page;


        /// <Summary>
        /// HowToPlayScreen(Game1 game):
        ///     How to play screen constructor.
        /// </Summary>
        public HowToPlayScreen(Game1 game) : base(game)
        {
            page = Page.CheatSheet;
            int centerX = (int)Global.ScreenSize.X / 2;
            logoFont = Global.Content.Load<SpriteFont>("Fonts/LogoFont");
            font = Global.Content.Load<SpriteFont>("Fonts/HighScoreFont");
            cheatSheet = Global.Content.Load<Texture2D>("Other/tutorialTexture");
            backButton = new Button(new Rectangle(centerX - 50, (int)(Global.ScreenSize.Y - 50*Global.buttonScale), 100, 50), "Back", menuFont, Alignment.Center);
            moreInfoButton = new Button(new Rectangle((int)(centerX * 2 - 200*Global.buttonScale), (int)Global.ScreenSize.Y - 200, 200, 50), "More Info", menuFont, Alignment.Center);
        }

        /// <Summary>
        /// public void Update():
        ///     Updates the Screen per frame.
        /// </Summary>
        public void Update()
        {
            base.BaseUpdate();
            backButton.Update();
            moreInfoButton.Update();
            if (keyboardState.IsKeyDown(Keys.Back) && lastKeyboardState.IsKeyUp(Keys.Back))
            {
                game.StartScreen();
            }

            lastKeyboardState = keyboardState;

            if (backButton.Clicked()) game.StartScreen();

            if (moreInfoButton.Clicked())
            {
                switch (page)
                {
                    case Page.CheatSheet:
                        page = Page.Lore;
                        moreInfoButton.Text = "Less Info";
                        break;
                    case Page.Lore:
                        page = Page.CheatSheet;
                        moreInfoButton.Text = "More Info";
                        break;
                }
            }
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Screen per frame.
        /// </Summary>
        public void Draw(Color[] skyColours)
        {
            base.BaseDraw("How to play", skyColours);
            StringBuilder buffer = new StringBuilder();
            buffer.AppendLine("You play as Foxy, the couragous Fox! A cunning re-incarnation of Mother Natrue.");
            buffer.AppendLine("There are three types of Blocks in the game. Good, Power and Dark.");
            buffer.AppendLine("The objective of the game is to capture the Good Blocks. Good blocks");
            buffer.AppendLine("are the white ones, shown on the side. These Blocks are rolling down the");
            buffer.AppendLine("stage, eventually over a cliff edge. Therefore these blocks must be");
            buffer.AppendLine("captured within a limited amount of steps (as they will fall off, otherwise).");
            buffer.AppendLine();
            buffer.AppendLine("To Mark a tile for capture, press the O key, to Fuse the tile, press the O");
            buffer.AppendLine("key again. Once a Block lands on a Fused tile, it will capture.");
            buffer.AppendLine();
            buffer.AppendLine("Capturing a Power block (Green in colour) creates a special marked tile");
            buffer.AppendLine("which when activated captures all Blocks touching it. To activate such");
            buffer.AppendLine("tile, press the F key. Pressing SPACE will speed the cubes up.");
            buffer.AppendLine();
            buffer.AppendLine("Do not capture Dark Blocks. You will be punished.");
            buffer.AppendLine();
            Global.SpriteBatch.Begin();
            switch (page)
            {
                case Page.CheatSheet:
                    Vector2 bgOrigin = GetTextureCentre(cheatSheet);
                    Global.SpriteBatch.Draw(cheatSheet, new Vector2(Global.ScreenSize.X / 2, Global.ScreenSize.Y / 10 * 6), null, Color.White, 0, bgOrigin, new Vector2(GetTextureRatio(cheatSheet, Global.ScreenSize.Y / 2), GetTextureRatio(cheatSheet, Global.ScreenSize.Y / 2)), spriteEffect, 1);
                    break;
                case Page.Lore:
                    Global.SpriteBatch.DrawString(font, buffer.ToString(), new Vector2((int)Global.ScreenSize.X / 2 - 300, (int)Global.ScreenSize.Y / 2 - (14 * 10 * Global.CreditsScale)), Color.Black, 0, Vector2.Zero, Global.CreditsScale * Global.buttonScale, SpriteEffects.None, 0);
                    break;
            }
            backButton.Draw();
            moreInfoButton.Draw();
            Global.SpriteBatch.End();
        }
    }
}
