﻿using _3D_Game_Recompile.Screens;
using _3D_Game_Recompile.Screens.Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Shop Screen Class: Buy Items here.
    /// </Summary>
    class ShopScreen : Screen
    {
        private SpriteFont font;
        private Button backButton;
        private List<ItemIcon> itemIcons;
        private List<Item> items;
        private SoundEffect coinBuy;
        private string errorMessage = " ";

        /// <Summary>
        /// public ShopScreen(Game1 game):
        ///     Shop Screen constructor.
        /// </Summary>
        public ShopScreen(Game1 game) : base(game)
        {
            logoFont = Global.Content.Load<SpriteFont>("Fonts/LogoFont");
            font = Global.Content.Load<SpriteFont>("Fonts/HighScoreFont");
            coinBuy = Global.Content.Load<SoundEffect>("Sounds/coinBuy");
            backButton = new Button(new Rectangle((int)Global.ScreenSize.X / 2, (int)Global.ScreenSize.Y - 50, 100, 50), "Back", menuFont, Alignment.Center);
            itemIcons = new List<ItemIcon>();
            items = new List<Item>();
            CreateItems();
            CreateItemIcons();
        }

        /// <Summary>
        /// private void CreateItems():
        ///     Create the items for the game.
        /// </Summary>
        private void CreateItems()
        {
            Item item = ItemList.CreateItem(0);
            items.Add(item);
        }

        /// <Summary>
        /// private void CreateItemIcons():
        ///     Create the Item Icons.
        /// </Summary>
        private void CreateItemIcons()
        {
            List<Rectangle> boundsList = GenerateIconPositions<Item>(items, 200);
            for (int i = 0; i < boundsList.Count; i++)
            {
                ItemIcon itemIcon = new ItemIcon(boundsList[i], items[i]);
                itemIcons.Add(itemIcon);
            }
        }

        /// <Summary>
        /// public void Update():
        ///     Updates the Screen per frame.
        /// </Summary>
        public void Update()
        {
            base.BaseUpdate();
            backButton.Update();
            if (keyboardState.IsKeyDown(Keys.Back) && lastKeyboardState.IsKeyUp(Keys.Back))
            {
                game.StartScreen();
            }
            lastKeyboardState = keyboardState;

            if (backButton.Clicked()) game.StartScreen();

            foreach (ItemIcon icon in itemIcons)
            {
                icon.Update();
                if (icon.Clicked())
                {
                    SoundPlayer.Play(coinBuy);
                    bool success = PlayerStats.AddItem(icon.Item);
                    if (!success)
                    {
                        errorMessage = "Not enough blocks, or already obtained!";
                    }
                }
            }
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Screen per frame.
        /// </Summary>
        public void Draw(Color[] skyColours)
        {
            base.BaseDraw("Shop", skyColours);
            Global.SpriteBatch.Begin();
            backButton.Draw();
            Global.SpriteBatch.DrawString(logoFont, PlayerStats.TotalBlocksDestroyed + " Blocks", new Vector2((int)Global.ScreenSize.X - 600, (int)Global.ScreenSize.Y - 200), Color.White);
            DrawString(HUDFont, errorMessage, new Rectangle(0, (int)Global.ScreenSize.Y - (int)Global.ScreenSize.Y / 8, (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y - (int)Global.ScreenSize.Y / 4), Alignment.Top, Color.White);
            foreach (ItemIcon icon in itemIcons)
            {
                icon.Draw();
            }
            Global.SpriteBatch.End();
        }
    }
}
