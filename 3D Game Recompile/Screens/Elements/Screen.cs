﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Screen Class: A screen is a section of the game
    /// </Summary>
    public abstract class Screen : Panel
    {
        private Texture2D logoTexture;
        private Texture2D bgTexture, bgStars, bgSunGlow;
        protected SpriteFont logoFont, menuFont, debugFont, HUDFont;
        protected KeyboardState lastKeyboardState, keyboardState;
        protected Game1 game;
        protected SpriteEffects spriteEffect;

        /// <Summary>
        /// public Screen(Game1 game):
        ///     Screen constructor.
        /// </Summary>
        public Screen(Game1 game) : base()
        {
            this.game = game;
            logoTexture = Global.Content.Load<Texture2D>("Other/GameLogo");
            bgTexture = Global.Content.Load<Texture2D>("Other/menuBackground");
            bgStars = Global.Content.Load<Texture2D>("Other/starsMenu");
            bgSunGlow = Global.Content.Load<Texture2D>("Other/sunGlowBackground");
            menuFont = Global.Content.Load<SpriteFont>("Fonts/MenuFont");
            logoFont = Global.Content.Load<SpriteFont>("Fonts/LogoFont");
            HUDFont = Global.Content.Load<SpriteFont>("Fonts/HUDFont");
            debugFont = Global.Content.Load<SpriteFont>("Fonts/DebugFont");
            spriteEffect = new SpriteEffects();
            lastKeyboardState = Keyboard.GetState();
        }

        /// <Summary>
        /// protected virtual void BaseUpdate():
        ///     This gets called at every Update() function of a inherited screen.
        /// </Summary>
        protected virtual void BaseUpdate()
        {
            keyboardState = Keyboard.GetState();
            UpdateMouse();
        }

        /// <Summary>
        /// protected virtual void BaseDraw(string title):
        ///     This gets called at every Draw() function of a inherited screen.
        /// </Summary>
        protected virtual void BaseDraw(string title, Color[] bgColour)
        {
            Vector2 bgOrigin = GetTextureCentre(bgTexture);
            Vector2 logoOrigin = GetTextureCentre(logoTexture);
            Global.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            Global.GraphicsDevice.Clear(bgColour[0]);
            Vector2 logoPosition = new Vector2((int)Global.ScreenSize.X / 2, (int)Global.ScreenSize.X / 8);
            Vector2 logosize = new Vector2(Global.ScreenSize.X / 3, logoTexture.Height * GetTextureRatio(logoTexture, Global.ScreenSize.X / 3));
            Global.SpriteBatch.Draw(logoTexture, logoPosition, null, Color.White, 0, logoOrigin, new Vector2(GetTextureRatio(logoTexture, Global.ScreenSize.X/3), GetTextureRatio(logoTexture, Global.ScreenSize.X/3)), spriteEffect, 1);
            Global.SpriteBatch.Draw(bgStars, new Vector2(Global.ScreenSize.X / 2, Global.ScreenSize.Y / 2), null, bgColour[1], 0, bgOrigin, new Vector2(GetTextureRatio(bgTexture, Global.ScreenSize.X), GetTextureRatio(bgTexture, Global.ScreenSize.X)), spriteEffect, 1);
            Global.SpriteBatch.Draw(bgSunGlow, new Vector2(Global.ScreenSize.X / 2, Global.ScreenSize.Y / 2), null, bgColour[2], 0, bgOrigin, new Vector2(GetTextureRatio(bgTexture, Global.ScreenSize.X), GetTextureRatio(bgTexture, Global.ScreenSize.X)), spriteEffect, 1);
            Global.SpriteBatch.Draw(bgTexture, new Vector2(Global.ScreenSize.X / 2, Global.ScreenSize.Y / 2), null, Color.White, 0, bgOrigin, new Vector2(GetTextureRatio(bgTexture, Global.ScreenSize.X), GetTextureRatio(bgTexture, Global.ScreenSize.X)), spriteEffect, 1);
            //Global.SpriteBatch.DrawString(logoFont, title, , Color.White);
            DrawString(logoFont, title, new Rectangle(0, (int)logoPosition.Y + (int)(logosize.Y / 8), (int)Global.ScreenSize.X, (int)Global.ScreenSize.Y), Alignment.Top, Color.White);
            Global.SpriteBatch.End();
        }

        /// <Summary>
        /// private List<Rectangle> GenerateIconPositions<T>(List<T> list):
        ///     Generate Icon Positions.
        /// </Summary>
        protected List<Rectangle> GenerateIconPositions<T>(List<T> list, int tileDimension)
        {
            List<Rectangle> boundsList = new List<Rectangle>();
            int maxWidth = (int)Global.ScreenSize.X - ((int)Global.ScreenSize.X / 4);
            int tilesPerRow = maxWidth / (tileDimension + Global.ButtonSep);
            int column = 0;
            int row = 0;
            int tileWidth = (int)(tileDimension * Global.buttonScale);
            for (int i = 0; i < list.Count; i++)
            {
                if (column == tilesPerRow)
                {
                    column = 0;
                    row++;
                }

                Rectangle bounds = new Rectangle((int)Global.ScreenSize.X / 8 + column * (tileWidth + Global.ButtonSep), 400 + (tileWidth + Global.ButtonSep) * row, tileDimension, tileDimension);
                boundsList.Add(bounds);
                column++;
            }
            return boundsList;
        }
    }
}
