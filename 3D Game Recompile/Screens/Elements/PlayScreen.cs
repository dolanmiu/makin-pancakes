﻿using _3D_Game_Recompile.Entities;
using _3D_Game_Recompile.Entities.Surfaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Text;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Play Screen Class: The main play area of the game.
    /// </Summary>
    public abstract class PlayScreen : Screen
    {
        protected Player player;
        protected ThirdPersonCamera camera;
        protected Stage stage;
        protected PlayGrid playGrid;
        protected InGameState inGameState;
        protected TimerComponent stageClearTimer;
        protected Level level;
        protected float fadeAlpha = 1;
        private Texture2D lifeLine;

        /// <Summary>
        /// public PlayScreen(Game1 game, Level level):
        ///     Play Screen constructor.
        /// </Summary>
        public PlayScreen(Game1 game, Level level) : base(game)
        {
            this.level = level;
            AllLevels.CurrentLevel = level;
            player = new Player();
            camera = new ThirdPersonCamera();
            stageClearTimer = new TimerComponent(4);
            camera.Perspective(80, (float)Global.GraphicsDevice.DisplayMode.Width / (float)Global.GraphicsDevice.DisplayMode.Height, 1.0f, 2048f);
            camera.LookAt(new Vector3(0f, 2.6f * 8f, 1 * 14.0f), Vector3.Zero, Vector3.Up);
            playGrid = new PlayGrid(this, player, camera, level.Width, level.Height, level);
            stage = new Stage(camera, level.Width * 2 + 10, level.Height * 2 + 3);
            inGameState = InGameState.FadeIn;
            stageClearTimer.Interval = 2f;

            //Load Content
            player.Load(camera);
            player.Position = new Vector3(player.Position.X, player.Position.Y, stage.Height - 7);
            playGrid.Load(level);
            stage.Load(camera);
            lifeLine = Global.Content.Load<Texture2D>("GUI/Lifeline");
        }

        /// <Summary>
        /// protected void PlayUpdate(GameTime gameTime):
        ///     This gets called by the Update() function of anything which inherits it.
        /// </Summary>
        protected void PlayUpdate(GameTime gameTime)
        {
            player.Projection = camera.ProjectionMatrix;
            player.Update(gameTime);
            camera.LookAt(player.Position);
            camera.Update(gameTime);
            playGrid.Update(gameTime);
            stage.Update(gameTime);
            base.BaseUpdate();
        }

        /// <Summary>
        /// protected void PlayDraw(Matrix viewMatrix):
        ///     This gets called by the Draw() function of anything which inherits it.
        /// </Summary>
        protected void PlayDraw(Matrix viewMatrix)
        {
            stage.Draw(viewMatrix);
            playGrid.Draw(viewMatrix);
            player.Draw(viewMatrix);
            DrawText();
            DrawUIElements();
        }

        /// <Summary>
        /// public void Extend(int rows)
        ///     Extends the Stage and the Play Grid at the same time by the same amount.
        /// </Summary>
        public void Extend(int rows)
        {
            playGrid.EditRow(rows);
            stage.Extend(rows*playGrid.TileWidth);
        }

        /// <Summary>
        /// private void DrawText():
        ///     Draws the nessesary UI text on the screen.
        /// </Summary>
        private void DrawText()
        {
            StringBuilder buffer = new StringBuilder();
            buffer.AppendLine();
            buffer.AppendLine("Player Position");
            buffer.AppendFormat("  Player X Location: {0}\n", player.Position.X.ToString("f2"));
            buffer.AppendFormat("  Player Y Location: {0}\n", player.Position.Y.ToString("f2"));
            buffer.AppendFormat("  Player Z Location: {0}\n", player.Position.Z.ToString("f2"));
            //buffer.AppendFormat("  GameTime: {0}, startTime: {1}\n", startTime.ToString("f2"), startTime.ToString("f2"));
            buffer.AppendLine();
            buffer.AppendFormat("  Quad 0,0 Location: {0}\n", playGrid.Quads[0,0].Origin);
            //buffer.AppendFormat("  Direction Foxy is moving in: {0}\n", playGrid.Quads[0, 0].Origin);
            //buffer.AppendFormat("  Cube Location: {0}\n", playGrid.Cubes[1,0].Position); //causes exception?
            buffer.AppendFormat("  Tiles on stage marked: {0}\n", playGrid.TileMarked);
            Global.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            Global.SpriteBatch.DrawString(debugFont, buffer.ToString(), new Vector2(1.0f, 1.0f), Color.Black);
            Global.SpriteBatch.DrawString(HUDFont, "Score: " + PlayerStats.GoodBlocks, new Vector2((int)Global.ScreenSize.X - 250, 50.0f), Color.White);
            Global.SpriteBatch.DrawString(HUDFont, "Wave: " + (level.CurrentWaveIndex + 1), new Vector2((int)Global.ScreenSize.X - 250, 100.0f), Color.White);
            Global.SpriteBatch.End();
            //Global.GraphicsDevice.BlendState = BlendState.Opaque;
            //Global.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
        }

        /// <Summary>
        /// private void DrawUIElements():
        ///     Draws the UI Elements on screen.
        /// </Summary>
        private void DrawUIElements()
        {
            Global.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            int lifeWidth = 100;
            for (int i = 0; i < PlayerStats.LifeLine; i++)
            {
                Global.SpriteBatch.Draw(lifeLine, new Rectangle((int)((Global.ScreenSize.X * 3f / 4f) + ((lifeWidth * Global.buttonScale) * (i + 1))), (int)(Global.ScreenSize.Y * 3f / 4f), (int)(lifeWidth * Global.buttonScale), (int)(lifeWidth * Global.buttonScale)), Color.White);
            }
            Global.SpriteBatch.End();
        }

        /// <Summary>
        /// protected void ProcessKeys():
        ///     Processes all the keys pressed.
        /// </Summary>
        protected void ProcessKeys()
        {
            Vector3 forward = Vector3.Zero;
            Vector3 left = Vector3.Zero;
            Vector3 backward = Vector3.Zero;
            Vector3 right = Vector3.Zero;

            keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyUp(Keys.Escape)) game.Exit();
            if (keyboardState.GetPressedKeys().Length > 0)
            {
                if (keyboardState.IsKeyDown(Keys.W)) forward = Vector3.Forward;
                if (keyboardState.IsKeyDown(Keys.A)) left = Vector3.Left;
                if (keyboardState.IsKeyDown(Keys.S)) backward = Vector3.Backward;
                if (keyboardState.IsKeyDown(Keys.D)) right = Vector3.Right;
                /*if (keyboardState.IsKeyDown(Keys.Q))
                {
                    playGrid.Quads[0, 0].Up = Vector3.Up;
                    Vector3 tempNewPos = camera.Position - playGrid.Quads[0, 0].Origin;
                    Vector3 newPos = Vector3.Normalize(new Vector3(tempNewPos.X, 0, tempNewPos.Z));
                    playGrid.Quads[0, 0].Normal = newPos;
                }
                /*if (keyboardState.IsKeyDown(Keys.T) && lastKeyboardState.IsKeyUp(Keys.T))
                {
                    Extend(1);
                }
                if (keyboardState.IsKeyDown(Keys.Y) && lastKeyboardState.IsKeyUp(Keys.Y))
                {
                    Extend(-1);
                }*/
                if (keyboardState.IsKeyDown(Keys.O) && lastKeyboardState.IsKeyUp(Keys.O))
                {
                    if (playGrid.TileMarked) playGrid.FuseTile(); else playGrid.MarkTile();
                }
                if (keyboardState.IsKeyDown(Keys.P) && lastKeyboardState.IsKeyUp(Keys.P))
                {
                    playGrid.FuseAreaTile();
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Escape) && lastKeyboardState.IsKeyUp(Keys.Escape))
                {
                    inGameState = InGameState.Paused;
                }
                if (keyboardState.IsKeyDown(Keys.Space)) playGrid.CurrentWave.SetRollTime(0.2f);
                lastKeyboardState = keyboardState;

                Vector3 resultantDirection = Vector3.Add(Vector3.Add(Vector3.Add(forward, left), backward), right);
                player.Move(resultantDirection);
            }
            else
            {
                if (keyboardState.IsKeyUp(Keys.W) || keyboardState.IsKeyUp(Keys.A) || keyboardState.IsKeyUp(Keys.S) || keyboardState.IsKeyUp(Keys.D)) player.SlowDown();
            }
        }

        #region Properties
        public InGameState InGameState
        {
            set { inGameState = value; }
        }

        public Level Level
        {
            get { return level; }
        }

        public PlayGrid PlayGrid
        {
            get { return playGrid; }
        }
        #endregion
    }
}
