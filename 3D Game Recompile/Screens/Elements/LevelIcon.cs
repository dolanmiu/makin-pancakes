﻿using _3D_Game_Recompile.Screens.Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Level Icon Class: The level icon used to display a summary of a level.
    /// </Summary>
    class LevelIcon : Clickable
    {
        private Level level;
        private Texture2D starFull, starEmpty;
        private Vector2 starSize;
        private int star;

        protected string text;
        protected float textRatio;
        protected SpriteFont font;

        /// <Summary>
        /// public LevelIcon(Rectangle bounds, Level level, int star) : base(bounds, Alignment.Left):
        ///     Level Icon constructor.
        /// </Summary>
        public LevelIcon(Rectangle bounds, Level level, int star) : base(bounds, Alignment.Left)
        {
            this.level = level;
            this.star = star;
            this.font = Global.Content.Load<SpriteFont>("Fonts/MenuFont");
            this.starFull = Global.Content.Load<Texture2D>("GUI/StarFull");
            this.starEmpty = Global.Content.Load<Texture2D>("GUI/StarEmpty");

            float iconWidth = bounds.Width * Global.buttonScale;
            float starWidth = iconWidth / 3f;
            float starRatio = 31f / 35f;
            float starHeight = starRatio * starWidth;
            starSize = new Vector2(starWidth, starHeight); //default is 35 * 31;

            if (level.ID >= 0)
            {
                text = "Level " + (level.ID + 1);
            }
            else
            {
                text = level.Name;
            }

            Vector2 size = font.MeasureString(text);
            float clickableWidth = bounds.Width * Global.buttonScale;
            textRatio = clickableWidth / size.X;
            if (textRatio >= Global.buttonScale) textRatio = Global.buttonScale;
        }

        /// <Summary>
        /// public void Draw():
        ///     Draw the Level Icon per frame.
        /// </Summary>
        public void Draw()
        {
            if (hoverOver)
            {
                Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.Red);
            }
            else
            {
                Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.DarkRed);
            }

            DrawString(font, text, bounds, Alignment.Center, Color.White, textRatio, true);

            for (int i = 0; i < star; i++)
            {
                Global.SpriteBatch.Draw(starFull, new Rectangle(bounds.X + ((int)starSize.X * i), bounds.Y + bounds.Height - (int)starSize.X, (int)starSize.X, (int)starSize.Y), Color.White);
            }
            for (int i = star; i < 3; i++)
            {
                Global.SpriteBatch.Draw(starEmpty, new Rectangle(bounds.X + ((int)starSize.X * i), bounds.Y + bounds.Height - (int)starSize.X, (int)starSize.X, (int)starSize.Y), Color.White);
            }

        }

        #region Properties
        public Level Level
        {
            get { return level; }
        }
        #endregion
    }
}
