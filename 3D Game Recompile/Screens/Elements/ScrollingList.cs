﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
namespace _3D_Game_Recompile.Screens.Elements
{
    class ScrollingList : Panel
    {
        private Button upWaveListButton, downWaveListButton;
        private SpriteFont menuFont;
        private Rectangle bounds;
        private List<Button> buttons;
        private int index;
        private int buttonHeight = 50;

        public ScrollingList(int x, int y, int width, int height) 
        {
            index = 0;
            bounds = new Rectangle(x, y, width, height);
            buttons = new List<Button>();
            menuFont = Global.Content.Load<SpriteFont>("Fonts/MenuFont");
            upWaveListButton = new Button(new Rectangle(x + width/2, y, 50, 50), "^", menuFont, Alignment.Left);
            downWaveListButton = new Button(new Rectangle(x + width / 2, y + height, 50, 50), "v", menuFont, Alignment.Left);
        }

        public void Add()
        {
            int startingPoint = 0;
            if (buttons.Count == 0)
            {
                startingPoint = bounds.Y;
            }
            else
            {
                startingPoint = buttons[buttons.Count - 1].Bounds.Y;
            }

            Button button = new Button(new Rectangle(bounds.X, startingPoint + Global.ButtonSep + buttonHeight, 230, 50), "Wave " + (buttons.Count + 1), menuFont, Alignment.Left);
            buttons.Add(button);
        }

        public void Recalculate(string rootName, int toListCount)
        {
            buttons.Clear();
            for (int i = 0; i < toListCount; i++)
            {
                Add();
            }
        }

        public void Clear()
        {
            buttons.Clear();
        }

        private void ResursiveIndexCheck()
        {
            if (index > buttons.Count - 1)
            {
                index--;
                ResursiveIndexCheck();
            }
            else
            {
                return;
            }
        }

        public int Update()
        {
            ResursiveIndexCheck();
            upWaveListButton.Update();
            downWaveListButton.Update();

            for (int i = 0; i < buttons.Count; i++)
            {
                if ((buttons[i].Bounds.Y + buttons[i].Bounds.Height < bounds.Y + bounds.Height) && buttons[i].Bounds.Y > bounds.Y)
                {
                    buttons[i].Update();
                    if (buttons[i].Clicked())
                    {
                        index = i;
                    }
                }
            }

            if (upWaveListButton.Clicked())
            {
                for (int i = 0; i < buttons.Count; i++)
                {
                    buttons[i].Bounds = new Rectangle(buttons[i].Bounds.X, buttons[i].Bounds.Y - buttonHeight - Global.ButtonSep, buttons[i].Bounds.Width, buttons[i].Bounds.Height);
                }
            }

            if (downWaveListButton.Clicked())
            {
                for (int i = 0; i < buttons.Count; i++)
                {
                    buttons[i].Bounds = new Rectangle(buttons[i].Bounds.X, buttons[i].Bounds.Y + buttonHeight + Global.ButtonSep, buttons[i].Bounds.Width, buttons[i].Bounds.Height);
                }
            }

            return index;
        }

        public void Draw()
        {
            upWaveListButton.Draw();
            downWaveListButton.Draw();

            for (int i = 0; i < buttons.Count; i++)
            {
                if ((buttons[i].Bounds.Y + buttons[i].Bounds.Height < bounds.Y + bounds.Height) && buttons[i].Bounds.Y > bounds.Y)
                {
                    buttons[i].Draw();
                }
            }
        }

        #region Properties
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        #endregion
    }
}
