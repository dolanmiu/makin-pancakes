﻿using _3D_Game_Recompile.Screens.Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Button Class: Every button seen in the GUI
    /// </Summary>
    class Button : Clickable
    {
        protected string text;
        protected float textRatio;
        protected SpriteFont font;

        /// <Summary>
        /// public Button(Rectangle bounds, string text, SpriteFont font, Alignment alignment) : base(bounds, alignment): 
        ///     Button constructor
        /// </Summary>
        public Button(Rectangle bounds, string text, SpriteFont font, Alignment alignment) : base(bounds, alignment)
        {
            this.text = text;
            this.font = font;

            Vector2 size = font.MeasureString(text);
            float clickableWidth = bounds.Width * Global.buttonScale;
            textRatio = clickableWidth / size.X;
            if (textRatio >= Global.buttonScale) textRatio = Global.buttonScale;
        }

        /// <Summary>
        /// public void Draw(): 
        ///     Draws the button per frame
        /// </Summary>
        public void Draw()
        {
            if (hoverOver)
            {
                Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.Red);
            }
            else
            {
                Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.DarkRed);
            }

            DrawString(font, text, bounds, Alignment.Center, Color.White, textRatio, true);
        }

        #region Properties
        public string Text
        {
            set { text = value; }
        }
        #endregion
    }
}
