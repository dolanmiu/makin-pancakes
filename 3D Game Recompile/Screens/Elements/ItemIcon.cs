﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace _3D_Game_Recompile.Screens.Elements
{
    class ItemIcon : Clickable
    {
        private float textRatio;
        private Item item;
        private SpriteFont font;
        private SpriteEffects spriteEffect;

        public ItemIcon(Rectangle bounds, Item item) : base(bounds, Alignment.Left)
        {
            this.font = Global.Content.Load<SpriteFont>("Fonts/MenuFont");
            this.item = item;

            Vector2 size = font.MeasureString(item.ToString());
            float clickableWidth = bounds.Width * Global.buttonScale;
            textRatio = clickableWidth / size.X;
            if (textRatio >= Global.buttonScale) textRatio = Global.buttonScale;
            spriteEffect = new SpriteEffects();
        }

        public void Draw()
        {
            if (hoverOver)
            {
                Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.Red);
            }
            else
            {
                Global.SpriteBatch.Draw(WhiteTexture, bounds, Color.DarkRed);
            }

            Global.SpriteBatch.Draw(item.Thumnnail, new Vector2(bounds.X, bounds.Y), null, Color.White, 0, new Vector2(0, 0), new Vector2(GetTextureRatio(item.Thumnnail, bounds.Width), GetTextureRatio(item.Thumnnail, bounds.Height)), spriteEffect, 1);
            DrawString(font, item.ToString(), bounds, Alignment.Top, Color.White, textRatio, true);
            if (PlayerStats.ContainsItem(item))
            {
                DrawString(font, "Bought!", bounds, Alignment.Bottom, Color.White, textRatio, true);
            }
            else
            {
                DrawString(font, item.Price + " Blocks", bounds, Alignment.Bottom, Color.White, textRatio, true);
            }
        }

        #region Properties
        public Item Item
        {
            get { return item; }
        }
        #endregion
    }
}
