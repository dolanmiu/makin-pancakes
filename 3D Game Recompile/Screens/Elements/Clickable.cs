﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace _3D_Game_Recompile.Screens.Elements
{
    /// <Summary>
    /// Clickable Class: A clickable object.
    /// </Summary>
    public abstract class Clickable : Panel
    {
        protected Rectangle bounds;
        protected bool hoverOver;
        protected int centerLineY;
        protected int centerLineX;
        protected bool clicked;

        /// <Summary>
        /// public Clickable(Rectangle bounds, Alignment alignment):
        ///     Clickable constructor.
        /// </Summary>
        public Clickable(Rectangle bounds, Alignment alignment) : base()
        {
            int buttonPosX = 0;
            switch (alignment)
            {
                case Alignment.Center:
                    buttonPosX = (int)(bounds.X - ((bounds.Width * Global.buttonScale)/2));
                    break;
                case Alignment.Left:
                    buttonPosX = (int)(bounds.X);
                    break;
                case Alignment.Right:
                    buttonPosX = (int)(bounds.X - (bounds.Width * Global.buttonScale));
                    break;
            }
            this.bounds = new Rectangle(buttonPosX, (int)(bounds.Y), (int)(bounds.Width * Global.buttonScale), (int)(bounds.Height * Global.buttonScale));
            centerLineY = (int)Global.ScreenSize.X / 2;
            centerLineX = (int)Global.ScreenSize.Y / 2;
        }

        /// <Summary>
        /// public void Update():
        ///     Updates the clickable object every frame.
        /// </Summary>
        public void Update()
        {
            UpdateMouse();
            if (DetectBounds(bounds))
            {
                hoverOver = true;
            }
            else
            {
                hoverOver = false;
            }
        }

        /// <Summary>
        /// public bool Clicked():
        ///     Returns true if clicked, returns false if not clicked.
        /// </Summary>
        public bool Clicked()
        {
            if (mouseState.LeftButton == ButtonState.Pressed && lastMouseState.LeftButton == ButtonState.Released) clicked = false;
            if (mouseState.LeftButton == ButtonState.Released && lastMouseState.LeftButton == ButtonState.Pressed)
            {
                if (hoverOver && !clicked)
                {
                    SoundPlayer.Play(clickSound);
                    clicked = true;
                    return true;
                }
            }
            lastMouseState = mouseState;
            return false;
        }

        #region Properties
        public bool HoverOver
        {
            get { return hoverOver; }
            set { hoverOver = value; }
        }

        public Rectangle Bounds
        {
            get { return bounds; }
            set { bounds = value; }
        }

        public Vector2 MidPoint
        {
            get
            {
                int midX = bounds.X + (bounds.Width / 2);
                int midY = bounds.Y + (bounds.Height / 2);
                return new Vector2(midX, midY);
            }
        }
        #endregion
    }
}
