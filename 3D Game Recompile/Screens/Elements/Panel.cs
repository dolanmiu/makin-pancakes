﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Panel Class: Panel class used to contain the GUI. Like Java Swings version.
    /// </Summary>
    public abstract class Panel
    {
        protected int mouseX, mouseY;
        protected MouseState mouseState;
        protected MouseState lastMouseState;
        protected Texture2D WhiteTexture;
        protected SoundEffect clickSound;

        [Flags]
        public enum Alignment { Center = 0, Left = 1, Right = 2, Top = 4, Bottom = 8 }

        /// <Summary>
        /// public Panel():
        ///     Panel constructor.
        /// </Summary>
        public Panel()
        {
            WhiteTexture = new Texture2D(Global.GraphicsDevice, 1, 1);
            WhiteTexture.SetData(new Color[] { Color.White });
            clickSound = Global.Content.Load<SoundEffect>("Sounds/menuClick");
        }

        /// <Summary>
        /// protected void UpdateMouse():
        ///     Updates the position and state of the mouse.
        /// </Summary>
        protected void UpdateMouse()
        {
            mouseState = Mouse.GetState();
            mouseX = mouseState.X;
            mouseY = mouseState.Y;
        }

        /// <Summary>
        /// protected Boolean DetectBounds(Rectangle bounds):
        ///     Checks to see if the mouse is within the rectangle.
        /// </Summary>
        protected Boolean DetectBounds(Rectangle bounds)
        {
            if (mouseX > bounds.X && mouseX < bounds.X + bounds.Width)
            {
                if (mouseY > bounds.Y && mouseY < bounds.Y + bounds.Height)
                {
                    return true;
                }
            }
            return false;
        }

        /// <Summary>
        /// public void DrawString(SpriteFont font, string text, Rectangle bounds, Alignment align, Color color):
        ///     Draws a string on screen given the font and text with scaling enabled.
        /// </Summary>
        public void DrawString(SpriteFont font, string text, Rectangle bounds, Alignment align, Color color)
        {
            DrawString(font, text, bounds, align, color, 1, false);
        }

        /// <Summary>
        /// protected Vector2 GetTextureCentre(Texture2D texture):
        ///     Gets the centrepoint of the texture.
        /// </Summary>
        protected Vector2 GetTextureCentre(Texture2D texture)
        {
            Vector2 origin = new Vector2(texture.Bounds.X + (texture.Bounds.Width / 2), texture.Bounds.Y + (texture.Bounds.Height / 2));
            return origin;
        }

        /// <Summary>
        /// protected float GetTextureRatio(Texture2D texture, float preferedSize):
        ///     Gets the ratio between the texture size and preferred size.
        /// </Summary>
        protected float GetTextureRatio(Texture2D texture, float preferedSize)
        {
            float ratio = preferedSize / texture.Width;
            return ratio;
        }

        /// <Summary>
        /// public void DrawString(SpriteFont font, string text, Rectangle bounds, Alignment align, Color color, float scale, bool scaleOverride):
        ///     Draws a string on screen given the font and text.
        /// </Summary>
        public void DrawString(SpriteFont font, string text, Rectangle bounds, Alignment align, Color color, float scale, bool scaleOverride)
        {
            Vector2 size = font.MeasureString(text);
            Vector2 pos = new Vector2(bounds.Center.X, bounds.Center.Y);
            Vector2 origin = size * 0.5f;

            if (align.HasFlag(Alignment.Left))
                origin.X += bounds.Width / 2 - size.X / 2;

            if (align.HasFlag(Alignment.Right))
                origin.X -= bounds.Width / 2 - size.X / 2;

            if (align.HasFlag(Alignment.Top))
                origin.Y += (bounds.Height / 2 - size.Y / 2) / Global.buttonScale;

            if (align.HasFlag(Alignment.Bottom))
                origin.Y -= bounds.Height / 2 - size.Y / 2;

            if (scaleOverride)
            {
                Global.SpriteBatch.DrawString(font, text, pos, color, 0, origin, scale, SpriteEffects.None, 0);
            }
            else
            {
                Global.SpriteBatch.DrawString(font, text, pos, color, 0, origin, Global.buttonScale * scale, SpriteEffects.None, 0);
            }
        }

    }
}
