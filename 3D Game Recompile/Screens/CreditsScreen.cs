﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Text;

namespace _3D_Game_Recompile.Screens
{
    /// <Summary>
    /// Credits Screen Class: A screen showing the credits.
    /// </Summary>
    class CreditsScreen : Screen
    {
        private Button backButton;
        private SpriteFont font;

        /// <Summary>
        /// public CreditsScreen(Game1 game):
        ///     Credits screen constructor.
        /// </Summary>
        public CreditsScreen(Game1 game) : base(game)
        {
            font = Global.Content.Load<SpriteFont>("Fonts/HighScoreFont");
            backButton = new Button(new Rectangle((int)Global.ScreenSize.X / 2, (int)Global.ScreenSize.Y - 50, 100, 50), "Back", menuFont, Alignment.Center);
        }

        /// <Summary>
        /// public void Update():
        ///     Updates the Screen per frame.
        /// </Summary>
        public void Update()
        {
            backButton.Update();
            base.BaseUpdate();
            if (keyboardState.IsKeyDown(Keys.Back) && lastKeyboardState.IsKeyUp(Keys.Back))
            {
                game.StartScreen();
            }
            lastKeyboardState = keyboardState;

            if (backButton.Clicked()) game.StartScreen();
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Screen per frame.
        /// </Summary>
        public void Draw(Color[] skyColours)
        {
            base.BaseDraw("Credits", skyColours);
            StringBuilder buffer = new StringBuilder();
            buffer.AppendLine("Makin Pancakes ver 0.1Beta");
            buffer.AppendLine("Copyright 2012 by BulletFight Productions");
            buffer.AppendLine("A game developed by BulletFight and Published by UCL");
            buffer.AppendLine();
            buffer.AppendLine("Programmer: ");
            buffer.AppendLine("Dolan Miu");
            buffer.AppendLine();
            buffer.AppendLine("Artwork: ");
            buffer.AppendLine("Dolan Miu");
            buffer.AppendLine("Mandy Cheung");
            buffer.AppendLine();
            buffer.AppendLine("Music: ");
            buffer.AppendLine("Kyle Riley");
            buffer.AppendLine();
            buffer.AppendLine("Quality Assurance: ");
            buffer.AppendLine("Chris Powell");
            buffer.AppendLine("Kelvin Miu");
            buffer.AppendLine();
            buffer.AppendLine("Special Thanks:");
            buffer.AppendLine("Dolly Cheung");
            buffer.AppendLine("Dean Mohammedally");
            buffer.AppendLine("Jon Bird");
            buffer.AppendLine("MonoGame forums");
            buffer.AppendLine("Google");
            buffer.AppendLine();
            Global.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            Global.SpriteBatch.DrawString(font, buffer.ToString(), new Vector2((int)Global.ScreenSize.X / 2 - 300, (int)Global.ScreenSize.Y / 2 - (25*10*Global.CreditsScale)), Color.Black, 0, Vector2.Zero, Global.CreditsScale * Global.buttonScale, SpriteEffects.None, 0);
            backButton.Draw();
            Global.SpriteBatch.End();
        }
    }
}
