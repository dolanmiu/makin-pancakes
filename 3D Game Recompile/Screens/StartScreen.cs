﻿using _3D_Game_Recompile.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace _3D_Game_Recompile
{
    /// <Summary>
    /// Start Screen Class: The Main Screen/Menu.
    /// </Summary>
    class StartScreen : Screen
    {
        private Button playButton, shopButton, howToPlayButton, optionsButton, levelDesignerButton, creditsButton;

        /// <Summary>
        /// public StartScreen(Game1 game):
        ///     Start Screen constructor.
        /// </Summary>
        public StartScreen(Game1 game) : base(game)
        {
            int centerLineX = (int)Global.ScreenSize.X / 2;
            playButton = new Button(new Rectangle(centerLineX, (int)Global.ScreenSize.Y / 2, 290, 50), "Play", menuFont, Alignment.Center);
            shopButton = new Button(new Rectangle(centerLineX, playButton.Bounds.Y + playButton.Bounds.Height + Global.ButtonSep, 290, 50), "Shop", menuFont, Alignment.Center);
            howToPlayButton = new Button(new Rectangle(centerLineX, shopButton.Bounds.Y + shopButton.Bounds.Height + Global.ButtonSep, 290, 50), "How to play", menuFont, Alignment.Center);
            optionsButton = new Button(new Rectangle(centerLineX, howToPlayButton.Bounds.Y + howToPlayButton.Bounds.Height + Global.ButtonSep, 290, 50), "Options", menuFont, Alignment.Center);
            levelDesignerButton = new Button(new Rectangle(centerLineX, optionsButton.Bounds.Y + optionsButton.Bounds.Height + Global.ButtonSep, 290, 50), "Level Designer", menuFont, Alignment.Center);
            creditsButton = new Button(new Rectangle(centerLineX, levelDesignerButton.Bounds.Y + levelDesignerButton.Bounds.Height + Global.ButtonSep, 290, 50), "Credits", menuFont, Alignment.Center);
        }

        /// <Summary>
        /// public void Update():
        ///     Updates the Screen per frame.
        /// </Summary>
        public void Update()
        {
            base.BaseUpdate();

            if (keyboardState.IsKeyDown(Keys.Enter) && lastKeyboardState.IsKeyUp(Keys.Enter))
            {
                game.LevelSelect();
            }

            lastKeyboardState = keyboardState;

            playButton.Update();
            shopButton.Update();
            howToPlayButton.Update();
            optionsButton.Update();
            levelDesignerButton.Update();
            creditsButton.Update();

            if (playButton.Clicked())
            {
                AllLevels.ResetLevels();
                game.LevelSelect();
            }
            if (shopButton.Clicked()) game.ShopMenu();
            if (howToPlayButton.Clicked()) game.HowToPlayScreen();
            if (optionsButton.Clicked()) game.ShowOptions();
            if (levelDesignerButton.Clicked()) game.ShowLevelDesigner();
            if (creditsButton.Clicked()) game.ShowCredits();
        }

        /// <Summary>
        /// public void Draw():
        ///     Draws the Screen per frame.
        /// </Summary>
        public void Draw(Color[] skyColours)
        {
            base.BaseDraw("", skyColours);
            Global.SpriteBatch.Begin();
            playButton.Draw();
            shopButton.Draw();
            howToPlayButton.Draw();
            optionsButton.Draw();
            levelDesignerButton.Draw();
            creditsButton.Draw();
            Global.SpriteBatch.End();
        }
    }
}
