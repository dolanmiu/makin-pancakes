﻿namespace _3D_Game_Recompile
{
    public enum GameState
    {
        /// <summary>
        /// Default value - means no state is set
        /// </summary>
        None,

        /// <summary>
        /// The Start Menu / Home page
        /// </summary>
        StartScreen,

        /// <summary>
        /// Select the level to play
        /// </summary>
        LevelSelect,

        /// <summary>
        /// The Start Menu/ Home page
        /// </summary>
        InGame,

        /// <summary>
        /// Logo Screen is being displayed
        /// </summary>
        LogoSplash,

        /// <summary>
        /// The shop
        /// </summary>
        Shop,

        /// <summary>
        /// How to play the game. A tutorial.
        /// </summary>
        HowToPlay,

        /// <summary>
        /// Settings of Game
        /// </summary>
        Settings,

        /// <summary>
        /// Design levels with this
        /// </summary>
        LevelDesigner,

        /// <summary>
        /// Credits of who made it and who contributed
        /// </summary>
        Credits,
    }

    public enum LightingType
    {
        /// <summary>
        /// Use the lighting parameters setup for in game
        /// </summary>
        InGame,

        /// <summary>
        /// Use the lighting parameters setup for menus
        /// </summary>
        Menu,
    }

    public enum InGameState
    {
        /// <summary>
        /// Fade into the game
        /// </summary>
        FadeIn,

        /// <summary>
        /// Set up a message before stage starts
        /// </summary>
        PreStage,

        /// <summary>
        /// Wave is destroyed, a message comes up
        /// </summary>
        WaveClear,

        /// <summary>
        /// Judges if a row should be added
        /// </summary>
        RowJudgement,

        /// <summary>
        /// Level is cleared
        /// </summary>
        LevelClear,

        /// <summary>
        /// Game is being played
        /// </summary>
        StageInProgress,

        /// <summary>
        /// Game is paused
        /// </summary>
        Paused,

        /// <summary>
        /// Player is falling, going to die
        /// </summary>
        Falling
    }
    public enum TestState { DesignMode, TestMode }
}
