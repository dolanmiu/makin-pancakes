﻿using _3D_Game_Recompile.Screens;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace _3D_Game_Recompile.HUD
{
    public sealed partial class SaveCustomLevelControl : UserControl
    {
        private LevelDesignerScreen screen;
        public SaveCustomLevelControl()
        {
            this.InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            screen.SaveLevel(levelText.Text);
            this.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        #region Properties
        public LevelDesignerScreen Screen
        {
            get { return screen; }
            set { screen = value; }
        }
        #endregion

        private void levelText_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
