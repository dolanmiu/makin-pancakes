﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MonoGame.Framework;
using _3D_Game_Recompile.HUD;


namespace _3D_Game_Recompile
{
    /// <summary>
    /// The root page used to display the game.
    /// </summary>
    public sealed partial class GamePage : SwapChainBackgroundPanel
    {
        readonly Game1 _game;
        public static SaveCustomLevelControl saveLevel;

        public GamePage(string launchArguments)
        {
            this.InitializeComponent();
            saveLevel = new SaveCustomLevelControl();
            SaveLevelStack.Children.Add(saveLevel);
            saveLevel.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            // Create the game.
            _game = XamlGame<Game1>.Create(launchArguments, Window.Current.CoreWindow, this);
        }

        public static void ClearUI()
        {
            //BlocksStack.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        public static void ShowInGameUI()
        {
        
        }
    }
}
